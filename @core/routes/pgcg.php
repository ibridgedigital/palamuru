<?php

use Illuminate\Support\Facades\Route;


Route::get('/login/admin', 'Auth\LoginController@showPgcgAdminLoginForm')->name('pgcg.admin.login');
Route::post('/logout/admin', 'PGCG\AdminDashboardController@adminLogout')->name('pgcg.admin.logout');
Route::post('/login/admin', 'Auth\LoginController@pgcgAdminLogin');

Route::group(['middleware' => ['setlang:frontend', 'globalVariable', 'maintains_mode', 'HtmlMinifier']], function() {
	// Route::post('/logout/admin', 'PGCG\AdminDashboardController@adminLogout')->name('pgcg.admin.logout');
	Route::get('/', 'PGCG\FrontendController@index')->name('pgcg.homepage');
	Route::get('/p/{slug}', 'PGCG\FrontendController@dynamic_single_page')->name('pgcg.frontend.dynamic.page');
	$announcements_page_slug = !empty(get_static_option('announcements_page_slug')) ? get_static_option('announcements_page_slug') : 'announcements';
	Route::get($announcements_page_slug, 'PGCG\FrontendController@announcements')->name('frontend.announcements');
	Route::get($announcements_page_slug.'/{slug}', 'PGCG\FrontendController@announcements_single')->name('pgcg.frontend.announcements.single');
});

// Route::get('/', 'PGCG\FrontendController@index')->name('pgcg.homepage');

Route::prefix('admin-home')->group(function () {
	Route::get('/', 'PGCG\AdminDashboardController@adminIndex')->name('pgcg.admin.home');
	Route::prefix('page')->group(function () {
        Route::get('/', 'PGCG\PagesController@index')->name('pgcg.admin.page');
        Route::get('/new', 'PGCG\PagesController@new_page')->name('pgcg.admin.page.new');
        Route::post('/new', 'PGCG\PagesController@store_new_page');
        Route::get('/edit/{id}', 'PGCG\PagesController@edit_page')->name('pgcg.admin.page.edit');
        Route::post('/update/{id}', 'PGCG\PagesController@update_page')->name('pgcg.admin.page.update');
        Route::post('/delete/{id}', 'PGCG\PagesController@delete_page')->name('pgcg.admin.page.delete');
        Route::get('/profile-update', 'PGCG\AdminDashboardController@admin_profile')->name('pgcg.admin.profile.update');
        Route::post('/profile-update', 'PGCG\AdminDashboardController@admin_profile_update');
        Route::post('/bulk-action', 'PGCG\PagesController@bulk_action')->name('pgcg.admin.page.bulk.action');
        
    });
    // Route::post('/logout/admin', 'PGCG\AdminDashboardController@adminLogout')->name('pgcg.admin.logout');
    Route::group(['prefix' => 'page-builder','namespace' => 'PGCG\Admin','middleware' 	=> 'auth:pgcg_admin'],function () {
    	/*-------------------------
            HOME PAGE BUILDER
        -------------------------*/
    	Route::get('/home-page', 'PageBuilderController@homepage_builder')->name('pgcg.admin.home.page.builder');
        Route::post('/home-page', 'PageBuilderController@update_homepage_builder');

		/*-------------------------
		   DYNAMIC PAGE BUILDER
		-------------------------*/
		Route::get('/dynamic-page/{type}/{id}', 'PageBuilderController@dynamicpage_builder')->name('pgcg.admin.dynamic.page.builder');
		Route::post('/dynamic-page', 'PageBuilderController@update_dynamicpage_builder')->name('admin.dynamic.page.builder.store');
	});
	Route::prefix('general-settings')->group(function () {
		/*----------------------------------------------------
		      SITE IDENTITY
		----------------------------------------------------*/
		Route::get('/site-identity', 'PGCG\GeneralSettingsController@site_identity')->name('pgcg.admin.general.site.identity');
		Route::post('/site-identity', 'PGCG\GeneralSettingsController@update_site_identity');
        /*----------------------------------------------------
            BASIC SETTINGS
        ----------------------------------------------------*/
        Route::get('/basic-settings', 'PGCG\GeneralSettingsController@basic_settings')->name('pgcg.admin.general.basic.settings');
        Route::post('/basic-settings', 'PGCG\GeneralSettingsController@update_basic_settings');
        /*----------------------------------------------------
            COLOR SETTINGS
      ----------------------------------------------------*/
        Route::get('/color-settings', 'PGCG\GeneralSettingsController@color_settings')->name('pgcg.admin.general.color.settings');
        Route::post('/color-settings', 'PGCG\GeneralSettingsController@update_color_settings');
        /*----------------------------------------------------
          SEO SETTINGS
        ----------------------------------------------------*/
        Route::get('/seo-settings', 'PGCG\GeneralSettingsController@seo_settings')->name('pgcg.admin.general.seo.settings');
        Route::post('/seo-settings', 'PGCG\GeneralSettingsController@update_seo_settings');
        /*----------------------------------------------------
          CUSTOM SCRIPT SETTINGS
         ----------------------------------------------------*/
        Route::get('/scripts', 'PGCG\GeneralSettingsController@scripts_settings')->name('pgcg.admin.general.scripts.settings');
        Route::post('/scripts', 'PGCG\GeneralSettingsController@update_scripts_settings');
        /*----------------------------------------------------
          EMAIL TEMPLATE SETTINGS
        ----------------------------------------------------*/
        Route::get('/email-template', 'PGCG\GeneralSettingsController@email_template_settings')->name('pgcg.admin.general.email.template');
        Route::post('/email-template', 'PGCG\GeneralSettingsController@update_email_template_settings');
        /*----------------------------------------------------
          EMAIL  SETTINGS
         ----------------------------------------------------*/
        Route::get('/email-settings', 'PGCG\GeneralSettingsController@email_settings')->name('pgcg.admin.general.email.settings');
        Route::post('/email-settings', 'PGCG\GeneralSettingsController@update_email_settings');
        /*----------------------------------------------------
          TYPOGRAPHY SETTINGS
        ----------------------------------------------------*/
        Route::get('/typography-settings', 'PGCG\GeneralSettingsController@typography_settings')->name('pgcg.admin.general.typography.settings');
        Route::post('/typography-settings', 'PGCG\GeneralSettingsController@update_typography_settings');
        Route::post('/typography-settings/single', 'PGCG\GeneralSettingsController@get_single_font_variant')->name('pgcg.admin.general.typography.single');
        /*----------------------------------------------------
          CACHE SETTINGS
         ----------------------------------------------------*/
        Route::get('/cache-settings', 'PGCG\GeneralSettingsController@cache_settings')->name('pgcg.admin.general.cache.settings');
        Route::post('/cache-settings', 'PGCG\GeneralSettingsController@update_cache_settings');
        /*----------------------------------------------------
         PAGE SETTINGS
        ----------------------------------------------------*/
        Route::get('/page-settings', 'PGCG\GeneralSettingsController@page_settings')->name('pgcg.admin.general.page.settings');
        Route::post('/page-settings', 'PGCG\GeneralSettingsController@update_page_settings');
        /*----------------------------------------------------
         UPDATE SYSTEM SETTINGS
        ----------------------------------------------------*/
        Route::get('/update-system', 'GeneralSettingsController@update_system')->name('admin.general.update.system');
        Route::post('/update-system', 'GeneralSettingsController@update_system_version');

		/*----------------------------------------------------
         CUSTOM CSS SETTINGS
        ----------------------------------------------------*/
        Route::get('/custom-css', 'PGCG\GeneralSettingsController@custom_css_settings')->name('pgcg.admin.general.custom.css');
        Route::post('/custom-css', 'PGCG\GeneralSettingsController@update_custom_css_settings');
        /*----------------------------------------------------
          CUSTOM JAVASCRIPT SETTINGS
         ----------------------------------------------------*/
        Route::get('/custom-js', 'PGCG\GeneralSettingsController@custom_js_settings')->name('pgcg.admin.general.custom.js');
        Route::post('/custom-js', 'PGCG\GeneralSettingsController@update_custom_js_settings');
        /*----------------------------------------------------
          SMTP SETTINGS
         ----------------------------------------------------*/
        Route::get('/smtp-settings', 'PGCG\GeneralSettingsController@smtp_settings')->name('pgcg.admin.general.smtp.settings');
        Route::post('/smtp-settings', 'PGCG\GeneralSettingsController@update_smtp_settings');
        Route::post('/smtp-settings/test', 'PGCG\GeneralSettingsController@test_smtp_settings')->name('pgcg.admin.general.smtp.settings.test');
        /*----------------------------------------------------
         GDPR SETTINGS
        ----------------------------------------------------*/
        Route::get('/gdpr-settings', 'PGCG\GeneralSettingsController@gdpr_settings')->name('pgcg.admin.general.gdpr.settings');
        Route::post('/gdpr-settings', 'PGCG\GeneralSettingsController@update_gdpr_cookie_settings');
        /*----------------------------------------------------
         PRELOADER SETTINGS
        ----------------------------------------------------*/
        Route::get('/preloader-settings', 'PGCG\GeneralSettingsController@preloader_settings')->name('pgcg.admin.general.preloader.settings');
        Route::post('/preloader-settings', 'PGCG\GeneralSettingsController@update_preloader_settings');
        /*----------------------------------------------------
         POPUP SETTINGS
        ----------------------------------------------------*/
        Route::get('/popup-settings', 'PGCG\GeneralSettingsController@popup_settings')->name('pgcg.admin.general.popup.settings');
        Route::post('/popup-settings', 'PGCG\GeneralSettingsController@update_popup_settings');
        /*----------------------------------------------------
          SITEMAP SETTINGS
         ----------------------------------------------------*/
        Route::get('/sitemap-settings', 'PGCG\GeneralSettingsController@sitemap_settings')->name('pgcg.admin.general.sitemap.settings');
        Route::post('/sitemap-settings', 'PGCG\GeneralSettingsController@update_sitemap_settings');
        Route::post('/sitemap-settings/delete', 'PGCG\GeneralSettingsController@delete_sitemap_settings')->name('pgcg.admin.general.sitemap.settings.delete');
        /*----------------------------------------------------
          RSS SETTINGS
         ----------------------------------------------------*/
        Route::get('/rss-settings', 'PGCG\GeneralSettingsController@rss_feed_settings')->name('pgcg.admin.general.rss.feed.settings');
        Route::post('/rss-settings', 'PGCG\GeneralSettingsController@update_rss_feed_settings');
        /*----------------------------------------------------
          MODULE SETTINGS
         ----------------------------------------------------*/
        Route::get('/module-settings', 'PGCG\GeneralSettingsController@module_settings')->name('pgcg.admin.general.module.settings');
        Route::post('/module-settings', 'PGCG\GeneralSettingsController@store_module_settings');
        /*----------------------------------------------------
            DATABASE UPGRADE
        ----------------------------------------------------*/
        Route::get('/database-upgrade', 'PGCG\GeneralSettingsController@database_upgrade')->name('pgcg.admin.general.database.upgrade');
        Route::post('/database-upgrade', 'PGCG\GeneralSettingsController@database_upgrade_post');
        /*----------------------------------------------------
            LICENSE SETTINGS
        ----------------------------------------------------*/
        Route::get('/license-setting', 'PGCG\GeneralSettingsController@license_settings')->name('pgcg.admin.general.license.settings');
        Route::post('/license-setting', 'PGCG\GeneralSettingsController@update_license_settings');

        /*----------------------------------------------------
         REGENERATE IMAGE SETTINGS
        ----------------------------------------------------*/
        Route::get('/regenerate-image', 'PGCG\GeneralSettingsController@regenerate_image_settings')->name('pgcg.admin.general.regenerate.thumbnail');
        Route::post('/regenerate-image', 'PGCG\GeneralSettingsController@update_regenerate_image_settings');
	});
	Route::prefix('announcements')->group(function() {
		/*----------------------------------------
            ANNOUNCEMENTS MODULE: ROUTEs
        ----------------------------------------*/
        Route::get('/all', 'PGCG\AnnouncementsController@all_announcements')->name('pgcg.admin.announcements.all');
        Route::get('/new', 'PGCG\AnnouncementsController@new_announcement')->name('pgcg.admin.announcements.new');
        Route::post('/new', 'PGCG\AnnouncementsController@store_announcement');
        Route::get('/edit/{id}', 'PGCG\AnnouncementsController@edit_announcement')->name('pgcg.admin.announcements.edit');
        Route::post('/update', 'PGCG\AnnouncementsController@update_announcement')->name('pgcg.admin.announcements.update');
        Route::post('/delete/{id}', 'PGCG\AnnouncementsController@delete_announcement')->name('pgcg.admin.announcements.delete');
        Route::post('/clone', 'PGCG\AnnouncementsController@clone_announcement')->name('pgcg.admin.announcements.clone');
        Route::post('/bulk-action', 'PGCG\AnnouncementsController@bulk_action')->name('pgcg.admin.announcements.bulk.action');

        /*----------------------------------------
        ANNOUNCEMENTS MODULE: CATEGORY ROUTES
         ----------------------------------------*/
        Route::group(['prefix' => 'category'],function (){
            //announcement category
            Route::get('/', 'PGCG\AnnouncementsCategoryController@all_announcements_category')->name('pgcg.admin.announcements.category.all');
            Route::post('/new', 'PGCG\AnnouncementsCategoryController@store_announcements_category')->name('pgcg.admin.announcements.category.new');
            Route::post('/update', 'PGCG\AnnouncementsCategoryController@update_announcements_category')->name('pgcg.admin.announcements.category.update');
            Route::post('/delete/{id}', 'PGCG\AnnouncementsCategoryController@delete_announcements_category')->name('pgcg.admin.announcements.category.delete');
            
            Route::post('/bulk-action', 'PGCG\AnnouncementsCategoryController@bulk_action')->name('pgcg.admin.announcements.category.bulk.action');
        });
	});
	/*==============================================
          COUNTERUP ROUTES
    ==============================================*/
	Route::prefix('counterup')->group(function () {
        Route::get('/', 'PGCG\CounterUpController@index')->name('pgcg.admin.counterup');
        Route::post('/', 'PGCG\CounterUpController@store');
        Route::post('/update', 'PGCG\CounterUpController@update')->name('pgcg.admin.counterup.update');
        Route::post('/delete/{id}', 'PGCG\CounterUpController@delete')->name('pgcg.admin.counterup.delete');
        Route::post('/bulk-action', 'PGCG\CounterUpController@bulk_action')->name('pgcg.admin.counterup.bulk.action');
    });
    /*==============================================
        TESTIMONIAL ROUTES
     ==============================================*/
    Route::prefix('testimonial')->group(function () {
        Route::get('/', 'PGCG\TestimonialController@index')->name('pgcg.admin.testimonial');
        Route::post('/', 'PGCG\TestimonialController@store');
        Route::post('/clone', 'PGCG\TestimonialController@clone')->name('pgcg.admin.testimonial.clone');
        Route::post('/update', 'PGCG\TestimonialController@update')->name('pgcg.admin.testimonial.update');
        Route::post('/delete/{id}', 'PGCG\TestimonialController@delete')->name('pgcg.admin.testimonial.delete');
        Route::post('/bulk-action', 'PGCG\TestimonialController@bulk_action')->name('pgcg.admin.testimonial.bulk.action');
    });
    /*==============================================
             WIDGETS MODULE ROUTES
    ==============================================*/
    Route::prefix('menu')->group(function () {
        Route::get('/', 'PGCG\MenuController@index')->name('pgcg.admin.menu');
        Route::post('/new', 'PGCG\MenuController@store_new_menu')->name('pgcg.admin.menu.new');
        Route::get('/edit/{id}', 'PGCG\MenuController@edit_menu')->name('pgcg.admin.menu.edit');
        Route::post('/update/{id}', 'PGCG\MenuController@update_menu')->name('pgcg.admin.menu.update');
        Route::post('/delete/{id}', 'PGCG\MenuController@delete_menu')->name('pgcg.admin.menu.delete');
        Route::post('/default/{id}', 'PGCG\MenuController@set_default_menu')->name('pgcg.admin.menu.default');
        Route::post('/mega-menu', 'PGCG\MenuController@mega_menu_item_select_markup')->name('pgcg.admin.mega.menu.item.select.markup');
    });
    Route::prefix('media-upload')->group(function () {
        Route::post('/delete', 'MediaUploadController@delete_upload_media_file')->name('pgcg.admin.upload.media.file.delete');
        Route::get('/page', 'PGCG\MediaUploadController@all_upload_media_images_for_page')->name('pgcg.admin.upload.media.images.page');
        Route::post('/alt', 'MediaUploadController@alt_change_upload_media_file')->name('pgcg.admin.upload.media.file.alt.change');
    });
    /*--------------------------
        PAGE BUILDER
    --------------------------*/
    Route::post('/update', 'PGCG\Admin\PageBuilderController@update_addon_content')->name('pgcg.admin.page.builder.update');
    Route::post('/new', 'PGCG\Admin\PageBuilderController@store_new_addon_content')->name('pgcg.admin.page.builder.new');
    Route::post('/delete', 'PGCG\Admin\PageBuilderController@delete')->name('pgcg.admin.page.builder.delete');
    Route::post('/update-order', 'PGCG\Admin\PageBuilderController@update_addon_order')->name('pgcg.admin.page.builder.update.addon.order');
    Route::post('/get-admin-markup', 'PGCG\Admin\PageBuilderController@get_admin_panel_addon_markup')->name('pgcg.admin.page.builder.get.addon.markup');
    /*==============================================
         IMAGE GALLERY ROUTES
    ==============================================*/
    Route::prefix('gallery-page')->group(function () {
        Route::get('/', 'PGCG\ImageGalleryPageController@index')->name('pgcg.admin.gallery.all');
        Route::post('/new', 'PGCG\ImageGalleryPageController@store')->name('pgcg.admin.gallery.new');
        Route::post('/update', 'PGCG\ImageGalleryPageController@update')->name('pgcg.admin.gallery.update');
        Route::post('/delete/{id}', 'PGCG\ImageGalleryPageController@delete')->name('pgcg.admin.gallery.delete');
        Route::post('/bulk-action', 'PGCG\ImageGalleryPageController@bulk_action')->name('pgcg.admin.gallery.bulk.action');
        Route::get('/page-settings', 'PGCG\ImageGalleryPageController@page_settings')->name('pgcg.admin.gallery.page.settings');
        Route::post('/page-settings', 'PGCG\ImageGalleryPageController@update_page_settings');
        /*------------------------
            IMAGE CATEGORY
        -------------------------*/
        Route::group(['prefix' => 'category'],function (){
            Route::get('/', 'PGCG\ImageGalleryPageController@category_index')->name('pgcg.admin.gallery.category');
            Route::post('/new', 'PGCG\ImageGalleryPageController@category_store')->name('pgcg.admin.gallery.category.new');
            Route::post('/update', 'PGCG\ImageGalleryPageController@category_update')->name('pgcg.admin.gallery.category.update');
            Route::post('/delete/{id}', 'PGCG\ImageGalleryPageController@category_delete')->name('pgcg.admin.gallery.category.delete');
            Route::post('/bulk-action', 'PGCG\ImageGalleryPageController@category_bulk_action')->name('pgcg.admin.gallery.category.bulk.action');
        });
        Route::post('/category-by-slug', 'PGCG\ImageGalleryPageController@category_by_slug')->name('pgcg.admin.gallery.category.by.lang');

    });
    /*==============================================
             WIDGETS MODULE ROUTES
    ==============================================*/
    Route::prefix('widgets')->group(function () {

        Route::get('/', 'PGCG\WidgetsController@index')->name('pgcg.admin.widgets');
        Route::post('/create', 'PGCG\WidgetsController@new_widget')->name('pgcg.admin.widgets.new');
        Route::post('/update', 'PGCG\WidgetsController@update_widget')->name('pgcg.admin.widgets.update');
        Route::post('/markup', 'PGCG\WidgetsController@widget_markup')->name('pgcg.admin.widgets.markup');
        Route::post('/update/order', 'PGCG\WidgetsController@update_order_widget')->name('pgcg.admin.widgets.update.order');
        Route::post('/delete', 'PGCG\WidgetsController@delete_widget')->name('pgcg.admin.widgets.delete');
    });
});


