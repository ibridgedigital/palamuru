<?php

use Illuminate\Support\Facades\Route;


Route::get('/login/admin', 'Auth\LoginController@showUpgcAdminLoginForm')->name('upgc.admin.login');
Route::post('/logout/admin', 'UPGC\AdminDashboardController@adminLogout')->name('upgc.admin.logout');
Route::post('/login/admin', 'Auth\LoginController@upgcAdminLogin');

Route::group(['middleware' => ['setlang:frontend', 'globalVariable', 'maintains_mode', 'HtmlMinifier']], function() {

	Route::get('/', 'UPGC\FrontendController@index')->name('upgc.homepage');
	Route::get('/p/{slug}', 'UPGC\FrontendController@dynamic_single_page')->name('upgc.frontend.dynamic.page');
	$announcements_page_slug = !empty(get_static_option('announcements_page_slug')) ? get_static_option('announcements_page_slug') : 'announcements';
	Route::get($announcements_page_slug, 'UPGC\FrontendController@announcements')->name('upgc.frontend.announcements');
	Route::get($announcements_page_slug.'/{slug}', 'UPGC\FrontendController@announcements_single')->name('upgc.frontend.announcements.single');
});

// Route::get('/', 'UPGC\FrontendController@index')->name('upgc.homepage');

Route::prefix('admin-home')->group(function () {
	Route::get('/', 'UPGC\AdminDashboardController@adminIndex')->name('upgc.admin.home');
	Route::prefix('page')->group(function () {
        Route::get('/', 'UPGC\PagesController@index')->name('upgc.admin.page');
        Route::get('/new', 'UPGC\PagesController@new_page')->name('upgc.admin.page.new');
        Route::post('/new', 'UPGC\PagesController@store_new_page');
        Route::get('/edit/{id}', 'UPGC\PagesController@edit_page')->name('upgc.admin.page.edit');
        Route::post('/update/{id}', 'UPGC\PagesController@update_page')->name('upgc.admin.page.update');
        Route::post('/delete/{id}', 'UPGC\PagesController@delete_page')->name('upgc.admin.page.delete');
        Route::get('/profile-update', 'UPGC\AdminDashboardController@admin_profile')->name('upgc.admin.profile.update');
        Route::post('/profile-update', 'UPGC\AdminDashboardController@admin_profile_update');
        Route::post('/bulk-action', 'UPGC\PagesController@bulk_action')->name('upgc.admin.page.bulk.action');
        
    });
    // Route::post('/logout/admin', 'UPGC\AdminDashboardController@adminLogout')->name('upgc.admin.logout');
    Route::group(['prefix' => 'page-builder','namespace' => 'UPGC\Admin','middleware' 	=> 'auth:upgc_admin'],function () {
    	/*-------------------------
            HOME PAGE BUILDER
        -------------------------*/
    	Route::get('/home-page', 'PageBuilderController@homepage_builder')->name('upgc.admin.home.page.builder');
        Route::post('/home-page', 'PageBuilderController@update_homepage_builder');

		/*-------------------------
		   DYNAMIC PAGE BUILDER
		-------------------------*/
		Route::get('/dynamic-page/{type}/{id}', 'PageBuilderController@dynamicpage_builder')->name('upgc.admin.dynamic.page.builder');
		Route::post('/dynamic-page', 'PageBuilderController@update_dynamicpage_builder')->name('upgc.admin.dynamic.page.builder.store');
	});
	Route::prefix('general-settings')->group(function () {
		/*----------------------------------------------------
		      SITE IDENTITY
		----------------------------------------------------*/
		Route::get('/site-identity', 'UPGC\GeneralSettingsController@site_identity')->name('upgc.admin.general.site.identity');
		Route::post('/site-identity', 'UPGC\GeneralSettingsController@update_site_identity');
        /*----------------------------------------------------
            BASIC SETTINGS
        ----------------------------------------------------*/
        Route::get('/basic-settings', 'UPGC\GeneralSettingsController@basic_settings')->name('upgc.admin.general.basic.settings');
        Route::post('/basic-settings', 'UPGC\GeneralSettingsController@update_basic_settings');
        /*----------------------------------------------------
            COLOR SETTINGS
      ----------------------------------------------------*/
        Route::get('/color-settings', 'UPGC\GeneralSettingsController@color_settings')->name('upgc.admin.general.color.settings');
        Route::post('/color-settings', 'UPGC\GeneralSettingsController@update_color_settings');
        /*----------------------------------------------------
          SEO SETTINGS
        ----------------------------------------------------*/
        Route::get('/seo-settings', 'UPGC\GeneralSettingsController@seo_settings')->name('upgc.admin.general.seo.settings');
        Route::post('/seo-settings', 'UPGC\GeneralSettingsController@update_seo_settings');
        /*----------------------------------------------------
          CUSTOM SCRIPT SETTINGS
         ----------------------------------------------------*/
        Route::get('/scripts', 'UPGC\GeneralSettingsController@scripts_settings')->name('upgc.admin.general.scripts.settings');
        Route::post('/scripts', 'UPGC\GeneralSettingsController@update_scripts_settings');
        /*----------------------------------------------------
          EMAIL TEMPLATE SETTINGS
        ----------------------------------------------------*/
        Route::get('/email-template', 'UPGC\GeneralSettingsController@email_template_settings')->name('upgc.admin.general.email.template');
        Route::post('/email-template', 'UPGC\GeneralSettingsController@update_email_template_settings');
        /*----------------------------------------------------
          EMAIL  SETTINGS
         ----------------------------------------------------*/
        Route::get('/email-settings', 'UPGC\GeneralSettingsController@email_settings')->name('upgc.admin.general.email.settings');
        Route::post('/email-settings', 'UPGC\GeneralSettingsController@update_email_settings');
        /*----------------------------------------------------
          TYPOGRAPHY SETTINGS
        ----------------------------------------------------*/
        Route::get('/typography-settings', 'UPGC\GeneralSettingsController@typography_settings')->name('upgc.admin.general.typography.settings');
        Route::post('/typography-settings', 'UPGC\GeneralSettingsController@update_typography_settings');
        Route::post('/typography-settings/single', 'UPGC\GeneralSettingsController@get_single_font_variant')->name('upgc.admin.general.typography.single');
        /*----------------------------------------------------
          CACHE SETTINGS
         ----------------------------------------------------*/
        Route::get('/cache-settings', 'UPGC\GeneralSettingsController@cache_settings')->name('upgc.admin.general.cache.settings');
        Route::post('/cache-settings', 'UPGC\GeneralSettingsController@update_cache_settings');
        /*----------------------------------------------------
         PAGE SETTINGS
        ----------------------------------------------------*/
        Route::get('/page-settings', 'UPGC\GeneralSettingsController@page_settings')->name('upgc.admin.general.page.settings');
        Route::post('/page-settings', 'UPGC\GeneralSettingsController@update_page_settings');
        /*----------------------------------------------------
         UPDATE SYSTEM SETTINGS
        ----------------------------------------------------*/
        Route::get('/update-system', 'GeneralSettingsController@update_system')->name('admin.general.update.system');
        Route::post('/update-system', 'GeneralSettingsController@update_system_version');

		/*----------------------------------------------------
         CUSTOM CSS SETTINGS
        ----------------------------------------------------*/
        Route::get('/custom-css', 'UPGC\GeneralSettingsController@custom_css_settings')->name('upgc.admin.general.custom.css');
        Route::post('/custom-css', 'UPGC\GeneralSettingsController@update_custom_css_settings');
        /*----------------------------------------------------
          CUSTOM JAVASCRIPT SETTINGS
         ----------------------------------------------------*/
        Route::get('/custom-js', 'UPGC\GeneralSettingsController@custom_js_settings')->name('upgc.admin.general.custom.js');
        Route::post('/custom-js', 'UPGC\GeneralSettingsController@update_custom_js_settings');
        /*----------------------------------------------------
          SMTP SETTINGS
         ----------------------------------------------------*/
        Route::get('/smtp-settings', 'UPGC\GeneralSettingsController@smtp_settings')->name('upgc.admin.general.smtp.settings');
        Route::post('/smtp-settings', 'UPGC\GeneralSettingsController@update_smtp_settings');
        Route::post('/smtp-settings/test', 'UPGC\GeneralSettingsController@test_smtp_settings')->name('upgc.admin.general.smtp.settings.test');
        /*----------------------------------------------------
         GDPR SETTINGS
        ----------------------------------------------------*/
        Route::get('/gdpr-settings', 'UPGC\GeneralSettingsController@gdpr_settings')->name('upgc.admin.general.gdpr.settings');
        Route::post('/gdpr-settings', 'UPGC\GeneralSettingsController@update_gdpr_cookie_settings');
        /*----------------------------------------------------
         PRELOADER SETTINGS
        ----------------------------------------------------*/
        Route::get('/preloader-settings', 'UPGC\GeneralSettingsController@preloader_settings')->name('upgc.admin.general.preloader.settings');
        Route::post('/preloader-settings', 'UPGC\GeneralSettingsController@update_preloader_settings');
        /*----------------------------------------------------
         POPUP SETTINGS
        ----------------------------------------------------*/
        Route::get('/popup-settings', 'UPGC\GeneralSettingsController@popup_settings')->name('upgc.admin.general.popup.settings');
        Route::post('/popup-settings', 'UPGC\GeneralSettingsController@update_popup_settings');
        /*----------------------------------------------------
          SITEMAP SETTINGS
         ----------------------------------------------------*/
        Route::get('/sitemap-settings', 'UPGC\GeneralSettingsController@sitemap_settings')->name('upgc.admin.general.sitemap.settings');
        Route::post('/sitemap-settings', 'UPGC\GeneralSettingsController@update_sitemap_settings');
        Route::post('/sitemap-settings/delete', 'UPGC\GeneralSettingsController@delete_sitemap_settings')->name('upgc.admin.general.sitemap.settings.delete');
        /*----------------------------------------------------
          RSS SETTINGS
         ----------------------------------------------------*/
        Route::get('/rss-settings', 'UPGC\GeneralSettingsController@rss_feed_settings')->name('upgc.admin.general.rss.feed.settings');
        Route::post('/rss-settings', 'UPGC\GeneralSettingsController@update_rss_feed_settings');
        /*----------------------------------------------------
          MODULE SETTINGS
         ----------------------------------------------------*/
        Route::get('/module-settings', 'UPGC\GeneralSettingsController@module_settings')->name('upgc.admin.general.module.settings');
        Route::post('/module-settings', 'UPGC\GeneralSettingsController@store_module_settings');
        /*----------------------------------------------------
            DATABASE UPGRADE
        ----------------------------------------------------*/
        Route::get('/database-upgrade', 'UPGC\GeneralSettingsController@database_upgrade')->name('upgc.admin.general.database.upgrade');
        Route::post('/database-upgrade', 'UPGC\GeneralSettingsController@database_upgrade_post');
        /*----------------------------------------------------
            LICENSE SETTINGS
        ----------------------------------------------------*/
        Route::get('/license-setting', 'UPGC\GeneralSettingsController@license_settings')->name('upgc.admin.general.license.settings');
        Route::post('/license-setting', 'UPGC\GeneralSettingsController@update_license_settings');

        /*----------------------------------------------------
         REGENERATE IMAGE SETTINGS
        ----------------------------------------------------*/
        Route::get('/regenerate-image', 'UPGC\GeneralSettingsController@regenerate_image_settings')->name('upgc.admin.general.regenerate.thumbnail');
        Route::post('/regenerate-image', 'UPGC\GeneralSettingsController@update_regenerate_image_settings');
	});
	Route::prefix('announcements')->group(function() {
		/*----------------------------------------
            ANNOUNCEMENTS MODULE: ROUTEs
        ----------------------------------------*/
        Route::get('/all', 'UPGC\AnnouncementsController@all_announcements')->name('upgc.admin.announcements.all');
        Route::get('/new', 'UPGC\AnnouncementsController@new_announcement')->name('upgc.admin.announcements.new');
        Route::post('/new', 'UPGC\AnnouncementsController@store_announcement');
        Route::get('/edit/{id}', 'UPGC\AnnouncementsController@edit_announcement')->name('upgc.admin.announcements.edit');
        Route::post('/update', 'UPGC\AnnouncementsController@update_announcement')->name('upgc.admin.announcements.update');
        Route::post('/delete/{id}', 'UPGC\AnnouncementsController@delete_announcement')->name('upgc.admin.announcements.delete');
        Route::post('/clone', 'UPGC\AnnouncementsController@clone_announcement')->name('upgc.admin.announcements.clone');
        Route::post('/bulk-action', 'UPGC\AnnouncementsController@bulk_action')->name('upgc.admin.announcements.bulk.action');

        /*----------------------------------------
        ANNOUNCEMENTS MODULE: CATEGORY ROUTES
         ----------------------------------------*/
        Route::group(['prefix' => 'category'],function (){
            //announcement category
            Route::get('/', 'UPGC\AnnouncementsCategoryController@all_announcements_category')->name('upgc.admin.announcements.category.all');
            Route::post('/new', 'UPGC\AnnouncementsCategoryController@store_announcements_category')->name('upgc.admin.announcements.category.new');
            Route::post('/update', 'UPGC\AnnouncementsCategoryController@update_announcements_category')->name('upgc.admin.announcements.category.update');
            Route::post('/delete/{id}', 'UPGC\AnnouncementsCategoryController@delete_announcements_category')->name('upgc.admin.announcements.category.delete');
            
            Route::post('/bulk-action', 'UPGC\AnnouncementsCategoryController@bulk_action')->name('upgc.admin.announcements.category.bulk.action');
        });
	});
	/*==============================================
          COUNTERUP ROUTES
    ==============================================*/
	Route::prefix('counterup')->group(function () {
        Route::get('/', 'UPGC\CounterUpController@index')->name('upgc.admin.counterup');
        Route::post('/', 'UPGC\CounterUpController@store');
        Route::post('/update', 'UPGC\CounterUpController@update')->name('upgc.admin.counterup.update');
        Route::post('/delete/{id}', 'UPGC\CounterUpController@delete')->name('upgc.admin.counterup.delete');
        Route::post('/bulk-action', 'UPGC\CounterUpController@bulk_action')->name('upgc.admin.counterup.bulk.action');
    });
    /*==============================================
        TESTIMONIAL ROUTES
     ==============================================*/
    Route::prefix('testimonial')->group(function () {
        Route::get('/', 'UPGC\TestimonialController@index')->name('upgc.admin.testimonial');
        Route::post('/', 'UPGC\TestimonialController@store');
        Route::post('/clone', 'UPGC\TestimonialController@clone')->name('upgc.admin.testimonial.clone');
        Route::post('/update', 'UPGC\TestimonialController@update')->name('upgc.admin.testimonial.update');
        Route::post('/delete/{id}', 'UPGC\TestimonialController@delete')->name('upgc.admin.testimonial.delete');
        Route::post('/bulk-action', 'UPGC\TestimonialController@bulk_action')->name('upgc.admin.testimonial.bulk.action');
    });
    /*==============================================
             WIDGETS MODULE ROUTES
    ==============================================*/
    Route::prefix('menu')->group(function () {
        Route::get('/', 'UPGC\MenuController@index')->name('upgc.admin.menu');
        Route::post('/new', 'UPGC\MenuController@store_new_menu')->name('upgc.admin.menu.new');
        Route::get('/edit/{id}', 'UPGC\MenuController@edit_menu')->name('upgc.admin.menu.edit');
        Route::post('/update/{id}', 'UPGC\MenuController@update_menu')->name('upgc.admin.menu.update');
        Route::post('/delete/{id}', 'UPGC\MenuController@delete_menu')->name('upgc.admin.menu.delete');
        Route::post('/default/{id}', 'UPGC\MenuController@set_default_menu')->name('upgc.admin.menu.default');
        Route::post('/mega-menu', 'UPGC\MenuController@mega_menu_item_select_markup')->name('upgc.admin.mega.menu.item.select.markup');
    });
    Route::prefix('media-upload')->group(function () {
        Route::post('/delete', 'MediaUploadController@delete_upload_media_file')->name('upgc.admin.upload.media.file.delete');
        Route::get('/page', 'UPGC\MediaUploadController@all_upload_media_images_for_page')->name('upgc.admin.upload.media.images.page');
        Route::post('/alt', 'MediaUploadController@alt_change_upload_media_file')->name('upgc.admin.upload.media.file.alt.change');
    });
    /*--------------------------
        PAGE BUILDER
    --------------------------*/
    Route::post('/update', 'UPGC\Admin\PageBuilderController@update_addon_content')->name('upgc.admin.page.builder.update');
    Route::post('/new', 'UPGC\Admin\PageBuilderController@store_new_addon_content')->name('upgc.admin.page.builder.new');
    Route::post('/delete', 'UPGC\Admin\PageBuilderController@delete')->name('upgc.admin.page.builder.delete');
    Route::post('/update-order', 'UPGC\Admin\PageBuilderController@update_addon_order')->name('upgc.admin.page.builder.update.addon.order');
    Route::post('/get-admin-markup', 'UPGC\Admin\PageBuilderController@get_admin_panel_addon_markup')->name('upgc.admin.page.builder.get.addon.markup');
    /*==============================================
         IMAGE GALLERY ROUTES
    ==============================================*/
    Route::prefix('gallery-page')->group(function () {
        Route::get('/', 'UPGC\ImageGalleryPageController@index')->name('upgc.admin.gallery.all');
        Route::post('/new', 'UPGC\ImageGalleryPageController@store')->name('upgc.admin.gallery.new');
        Route::post('/update', 'UPGC\ImageGalleryPageController@update')->name('upgc.admin.gallery.update');
        Route::post('/delete/{id}', 'UPGC\ImageGalleryPageController@delete')->name('upgc.admin.gallery.delete');
        Route::post('/bulk-action', 'UPGC\ImageGalleryPageController@bulk_action')->name('upgc.admin.gallery.bulk.action');
        Route::get('/page-settings', 'UPGC\ImageGalleryPageController@page_settings')->name('upgc.admin.gallery.page.settings');
        Route::post('/page-settings', 'UPGC\ImageGalleryPageController@update_page_settings');
        /*------------------------
            IMAGE CATEGORY
        -------------------------*/
        Route::group(['prefix' => 'category'],function (){
            Route::get('/', 'UPGC\ImageGalleryPageController@category_index')->name('upgc.admin.gallery.category');
            Route::post('/new', 'UPGC\ImageGalleryPageController@category_store')->name('upgc.admin.gallery.category.new');
            Route::post('/update', 'UPGC\ImageGalleryPageController@category_update')->name('upgc.admin.gallery.category.update');
            Route::post('/delete/{id}', 'UPGC\ImageGalleryPageController@category_delete')->name('upgc.admin.gallery.category.delete');
            Route::post('/bulk-action', 'UPGC\ImageGalleryPageController@category_bulk_action')->name('upgc.admin.gallery.category.bulk.action');
        });
        Route::post('/category-by-slug', 'UPGC\ImageGalleryPageController@category_by_slug')->name('upgc.admin.gallery.category.by.lang');

    });
    /*==============================================
             WIDGETS MODULE ROUTES
    ==============================================*/
    Route::prefix('widgets')->group(function () {

        Route::get('/', 'UPGC\WidgetsController@index')->name('upgc.admin.widgets');
        Route::post('/create', 'UPGC\WidgetsController@new_widget')->name('upgc.admin.widgets.new');
        Route::post('/update', 'UPGC\WidgetsController@update_widget')->name('upgc.admin.widgets.update');
        Route::post('/markup', 'UPGC\WidgetsController@widget_markup')->name('upgc.admin.widgets.markup');
        Route::post('/update/order', 'UPGC\WidgetsController@update_order_widget')->name('upgc.admin.widgets.update.order');
        Route::post('/delete', 'UPGC\WidgetsController@delete_widget')->name('upgc.admin.widgets.delete');
    });
});


