<?php

use Illuminate\Support\Facades\Route;


Route::get('/login/admin', 'Auth\LoginController@showPgcwAdminLoginForm')->name('pgcw.admin.login');
Route::post('/logout/admin', 'PGCW\AdminDashboardController@adminLogout')->name('pgcw.admin.logout');
Route::post('/login/admin', 'Auth\LoginController@pgcwAdminLogin');

Route::group(['middleware' => ['setlang:frontend', 'globalVariable', 'maintains_mode', 'HtmlMinifier']], function() {

	Route::get('/', 'PGCW\FrontendController@index')->name('pgcw.homepage');
	Route::get('/p/{slug}', 'PGCW\FrontendController@dynamic_single_page')->name('pgcw.frontend.dynamic.page');
	$announcements_page_slug = !empty(get_static_option('announcements_page_slug')) ? get_static_option('announcements_page_slug') : 'announcements';
	Route::get($announcements_page_slug, 'PGCW\FrontendController@announcements')->name('pgcw.frontend.announcements');
	Route::get($announcements_page_slug.'/{slug}', 'PGCW\FrontendController@announcements_single')->name('pgcw.frontend.announcements.single');
});

// Route::get('/', 'PGCW\FrontendController@index')->name('pgcw.homepage');

Route::prefix('admin-home')->group(function () {
	Route::get('/', 'PGCW\AdminDashboardController@adminIndex')->name('pgcw.admin.home');
	Route::prefix('page')->group(function () {
        Route::get('/', 'PGCW\PagesController@index')->name('pgcw.admin.page');
        Route::get('/new', 'PGCW\PagesController@new_page')->name('pgcw.admin.page.new');
        Route::post('/new', 'PGCW\PagesController@store_new_page');
        Route::get('/edit/{id}', 'PGCW\PagesController@edit_page')->name('pgcw.admin.page.edit');
        Route::post('/update/{id}', 'PGCW\PagesController@update_page')->name('pgcw.admin.page.update');
        Route::post('/delete/{id}', 'PGCW\PagesController@delete_page')->name('pgcw.admin.page.delete');
        Route::get('/profile-update', 'PGCW\AdminDashboardController@admin_profile')->name('pgcw.admin.profile.update');
        Route::post('/profile-update', 'PGCW\AdminDashboardController@admin_profile_update');
        Route::post('/bulk-action', 'PGCW\PagesController@bulk_action')->name('pgcw.admin.page.bulk.action');
        
    });
    // Route::post('/logout/admin', 'PGCW\AdminDashboardController@adminLogout')->name('pgcw.admin.logout');
    Route::group(['prefix' => 'page-builder','namespace' => 'PGCW\Admin','middleware' 	=> 'auth:pgcw_admin'],function () {
    	/*-------------------------
            HOME PAGE BUILDER
        -------------------------*/
    	Route::get('/home-page', 'PageBuilderController@homepage_builder')->name('pgcw.admin.home.page.builder');
        Route::post('/home-page', 'PageBuilderController@update_homepage_builder');

		/*-------------------------
		   DYNAMIC PAGE BUILDER
		-------------------------*/
		Route::get('/dynamic-page/{type}/{id}', 'PageBuilderController@dynamicpage_builder')->name('pgcw.admin.dynamic.page.builder');
		Route::post('/dynamic-page', 'PageBuilderController@update_dynamicpage_builder')->name('pgcw.admin.dynamic.page.builder.store');
	});
	Route::prefix('general-settings')->group(function () {
		/*----------------------------------------------------
		      SITE IDENTITY
		----------------------------------------------------*/
		Route::get('/site-identity', 'PGCW\GeneralSettingsController@site_identity')->name('pgcw.admin.general.site.identity');
		Route::post('/site-identity', 'PGCW\GeneralSettingsController@update_site_identity');
        /*----------------------------------------------------
            BASIC SETTINGS
        ----------------------------------------------------*/
        Route::get('/basic-settings', 'PGCW\GeneralSettingsController@basic_settings')->name('pgcw.admin.general.basic.settings');
        Route::post('/basic-settings', 'PGCW\GeneralSettingsController@update_basic_settings');
        /*----------------------------------------------------
            COLOR SETTINGS
      ----------------------------------------------------*/
        Route::get('/color-settings', 'PGCW\GeneralSettingsController@color_settings')->name('pgcw.admin.general.color.settings');
        Route::post('/color-settings', 'PGCW\GeneralSettingsController@update_color_settings');
        /*----------------------------------------------------
          SEO SETTINGS
        ----------------------------------------------------*/
        Route::get('/seo-settings', 'PGCW\GeneralSettingsController@seo_settings')->name('pgcw.admin.general.seo.settings');
        Route::post('/seo-settings', 'PGCW\GeneralSettingsController@update_seo_settings');
        /*----------------------------------------------------
          CUSTOM SCRIPT SETTINGS
         ----------------------------------------------------*/
        Route::get('/scripts', 'PGCW\GeneralSettingsController@scripts_settings')->name('pgcw.admin.general.scripts.settings');
        Route::post('/scripts', 'PGCW\GeneralSettingsController@update_scripts_settings');
        /*----------------------------------------------------
          EMAIL TEMPLATE SETTINGS
        ----------------------------------------------------*/
        Route::get('/email-template', 'PGCW\GeneralSettingsController@email_template_settings')->name('pgcw.admin.general.email.template');
        Route::post('/email-template', 'PGCW\GeneralSettingsController@update_email_template_settings');
        /*----------------------------------------------------
          EMAIL  SETTINGS
         ----------------------------------------------------*/
        Route::get('/email-settings', 'PGCW\GeneralSettingsController@email_settings')->name('pgcw.admin.general.email.settings');
        Route::post('/email-settings', 'PGCW\GeneralSettingsController@update_email_settings');
        /*----------------------------------------------------
          TYPOGRAPHY SETTINGS
        ----------------------------------------------------*/
        Route::get('/typography-settings', 'PGCW\GeneralSettingsController@typography_settings')->name('pgcw.admin.general.typography.settings');
        Route::post('/typography-settings', 'PGCW\GeneralSettingsController@update_typography_settings');
        Route::post('/typography-settings/single', 'PGCW\GeneralSettingsController@get_single_font_variant')->name('pgcw.admin.general.typography.single');
        /*----------------------------------------------------
          CACHE SETTINGS
         ----------------------------------------------------*/
        Route::get('/cache-settings', 'PGCW\GeneralSettingsController@cache_settings')->name('pgcw.admin.general.cache.settings');
        Route::post('/cache-settings', 'PGCW\GeneralSettingsController@update_cache_settings');
        /*----------------------------------------------------
         PAGE SETTINGS
        ----------------------------------------------------*/
        Route::get('/page-settings', 'PGCW\GeneralSettingsController@page_settings')->name('pgcw.admin.general.page.settings');
        Route::post('/page-settings', 'PGCW\GeneralSettingsController@update_page_settings');
        /*----------------------------------------------------
         UPDATE SYSTEM SETTINGS
        ----------------------------------------------------*/
        Route::get('/update-system', 'GeneralSettingsController@update_system')->name('admin.general.update.system');
        Route::post('/update-system', 'GeneralSettingsController@update_system_version');

		/*----------------------------------------------------
         CUSTOM CSS SETTINGS
        ----------------------------------------------------*/
        Route::get('/custom-css', 'PGCW\GeneralSettingsController@custom_css_settings')->name('pgcw.admin.general.custom.css');
        Route::post('/custom-css', 'PGCW\GeneralSettingsController@update_custom_css_settings');
        /*----------------------------------------------------
          CUSTOM JAVASCRIPT SETTINGS
         ----------------------------------------------------*/
        Route::get('/custom-js', 'PGCW\GeneralSettingsController@custom_js_settings')->name('pgcw.admin.general.custom.js');
        Route::post('/custom-js', 'PGCW\GeneralSettingsController@update_custom_js_settings');
        /*----------------------------------------------------
          SMTP SETTINGS
         ----------------------------------------------------*/
        Route::get('/smtp-settings', 'PGCW\GeneralSettingsController@smtp_settings')->name('pgcw.admin.general.smtp.settings');
        Route::post('/smtp-settings', 'PGCW\GeneralSettingsController@update_smtp_settings');
        Route::post('/smtp-settings/test', 'PGCW\GeneralSettingsController@test_smtp_settings')->name('pgcw.admin.general.smtp.settings.test');
        /*----------------------------------------------------
         GDPR SETTINGS
        ----------------------------------------------------*/
        Route::get('/gdpr-settings', 'PGCW\GeneralSettingsController@gdpr_settings')->name('pgcw.admin.general.gdpr.settings');
        Route::post('/gdpr-settings', 'PGCW\GeneralSettingsController@update_gdpr_cookie_settings');
        /*----------------------------------------------------
         PRELOADER SETTINGS
        ----------------------------------------------------*/
        Route::get('/preloader-settings', 'PGCW\GeneralSettingsController@preloader_settings')->name('pgcw.admin.general.preloader.settings');
        Route::post('/preloader-settings', 'PGCW\GeneralSettingsController@update_preloader_settings');
        /*----------------------------------------------------
         POPUP SETTINGS
        ----------------------------------------------------*/
        Route::get('/popup-settings', 'PGCW\GeneralSettingsController@popup_settings')->name('pgcw.admin.general.popup.settings');
        Route::post('/popup-settings', 'PGCW\GeneralSettingsController@update_popup_settings');
        /*----------------------------------------------------
          SITEMAP SETTINGS
         ----------------------------------------------------*/
        Route::get('/sitemap-settings', 'PGCW\GeneralSettingsController@sitemap_settings')->name('pgcw.admin.general.sitemap.settings');
        Route::post('/sitemap-settings', 'PGCW\GeneralSettingsController@update_sitemap_settings');
        Route::post('/sitemap-settings/delete', 'PGCW\GeneralSettingsController@delete_sitemap_settings')->name('pgcw.admin.general.sitemap.settings.delete');
        /*----------------------------------------------------
          RSS SETTINGS
         ----------------------------------------------------*/
        Route::get('/rss-settings', 'PGCW\GeneralSettingsController@rss_feed_settings')->name('pgcw.admin.general.rss.feed.settings');
        Route::post('/rss-settings', 'PGCW\GeneralSettingsController@update_rss_feed_settings');
        /*----------------------------------------------------
          MODULE SETTINGS
         ----------------------------------------------------*/
        Route::get('/module-settings', 'PGCW\GeneralSettingsController@module_settings')->name('pgcw.admin.general.module.settings');
        Route::post('/module-settings', 'PGCW\GeneralSettingsController@store_module_settings');
        /*----------------------------------------------------
            DATABASE UPGRADE
        ----------------------------------------------------*/
        Route::get('/database-upgrade', 'PGCW\GeneralSettingsController@database_upgrade')->name('pgcw.admin.general.database.upgrade');
        Route::post('/database-upgrade', 'PGCW\GeneralSettingsController@database_upgrade_post');
        /*----------------------------------------------------
            LICENSE SETTINGS
        ----------------------------------------------------*/
        Route::get('/license-setting', 'PGCW\GeneralSettingsController@license_settings')->name('pgcw.admin.general.license.settings');
        Route::post('/license-setting', 'PGCW\GeneralSettingsController@update_license_settings');

        /*----------------------------------------------------
         REGENERATE IMAGE SETTINGS
        ----------------------------------------------------*/
        Route::get('/regenerate-image', 'PGCW\GeneralSettingsController@regenerate_image_settings')->name('pgcw.admin.general.regenerate.thumbnail');
        Route::post('/regenerate-image', 'PGCW\GeneralSettingsController@update_regenerate_image_settings');
	});
	Route::prefix('announcements')->group(function() {
		/*----------------------------------------
            ANNOUNCEMENTS MODULE: ROUTEs
        ----------------------------------------*/
        Route::get('/all', 'PGCW\AnnouncementsController@all_announcements')->name('pgcw.admin.announcements.all');
        Route::get('/new', 'PGCW\AnnouncementsController@new_announcement')->name('pgcw.admin.announcements.new');
        Route::post('/new', 'PGCW\AnnouncementsController@store_announcement');
        Route::get('/edit/{id}', 'PGCW\AnnouncementsController@edit_announcement')->name('pgcw.admin.announcements.edit');
        Route::post('/update', 'PGCW\AnnouncementsController@update_announcement')->name('pgcw.admin.announcements.update');
        Route::post('/delete/{id}', 'PGCW\AnnouncementsController@delete_announcement')->name('pgcw.admin.announcements.delete');
        Route::post('/clone', 'PGCW\AnnouncementsController@clone_announcement')->name('pgcw.admin.announcements.clone');
        Route::post('/bulk-action', 'PGCW\AnnouncementsController@bulk_action')->name('pgcw.admin.announcements.bulk.action');

        /*----------------------------------------
        ANNOUNCEMENTS MODULE: CATEGORY ROUTES
         ----------------------------------------*/
        Route::group(['prefix' => 'category'],function (){
            //announcement category
            Route::get('/', 'PGCW\AnnouncementsCategoryController@all_announcements_category')->name('pgcw.admin.announcements.category.all');
            Route::post('/new', 'PGCW\AnnouncementsCategoryController@store_announcements_category')->name('pgcw.admin.announcements.category.new');
            Route::post('/update', 'PGCW\AnnouncementsCategoryController@update_announcements_category')->name('pgcw.admin.announcements.category.update');
            Route::post('/delete/{id}', 'PGCW\AnnouncementsCategoryController@delete_announcements_category')->name('pgcw.admin.announcements.category.delete');
            
            Route::post('/bulk-action', 'PGCW\AnnouncementsCategoryController@bulk_action')->name('pgcw.admin.announcements.category.bulk.action');
        });
	});
	/*==============================================
          COUNTERUP ROUTES
    ==============================================*/
	Route::prefix('counterup')->group(function () {
        Route::get('/', 'PGCW\CounterUpController@index')->name('pgcw.admin.counterup');
        Route::post('/', 'PGCW\CounterUpController@store');
        Route::post('/update', 'PGCW\CounterUpController@update')->name('pgcw.admin.counterup.update');
        Route::post('/delete/{id}', 'PGCW\CounterUpController@delete')->name('pgcw.admin.counterup.delete');
        Route::post('/bulk-action', 'PGCW\CounterUpController@bulk_action')->name('pgcw.admin.counterup.bulk.action');
    });
    /*==============================================
        TESTIMONIAL ROUTES
     ==============================================*/
    Route::prefix('testimonial')->group(function () {
        Route::get('/', 'PGCW\TestimonialController@index')->name('pgcw.admin.testimonial');
        Route::post('/', 'PGCW\TestimonialController@store');
        Route::post('/clone', 'PGCW\TestimonialController@clone')->name('pgcw.admin.testimonial.clone');
        Route::post('/update', 'PGCW\TestimonialController@update')->name('pgcw.admin.testimonial.update');
        Route::post('/delete/{id}', 'PGCW\TestimonialController@delete')->name('pgcw.admin.testimonial.delete');
        Route::post('/bulk-action', 'PGCW\TestimonialController@bulk_action')->name('pgcw.admin.testimonial.bulk.action');
    });
    /*==============================================
             WIDGETS MODULE ROUTES
    ==============================================*/
    Route::prefix('menu')->group(function () {
        Route::get('/', 'PGCW\MenuController@index')->name('pgcw.admin.menu');
        Route::post('/new', 'PGCW\MenuController@store_new_menu')->name('pgcw.admin.menu.new');
        Route::get('/edit/{id}', 'PGCW\MenuController@edit_menu')->name('pgcw.admin.menu.edit');
        Route::post('/update/{id}', 'PGCW\MenuController@update_menu')->name('pgcw.admin.menu.update');
        Route::post('/delete/{id}', 'PGCW\MenuController@delete_menu')->name('pgcw.admin.menu.delete');
        Route::post('/default/{id}', 'PGCW\MenuController@set_default_menu')->name('pgcw.admin.menu.default');
        Route::post('/mega-menu', 'PGCW\MenuController@mega_menu_item_select_markup')->name('pgcw.admin.mega.menu.item.select.markup');
    });
    Route::prefix('media-upload')->group(function () {
        Route::post('/delete', 'MediaUploadController@delete_upload_media_file')->name('pgcw.admin.upload.media.file.delete');
        Route::get('/page', 'PGCW\MediaUploadController@all_upload_media_images_for_page')->name('pgcw.admin.upload.media.images.page');
        Route::post('/alt', 'MediaUploadController@alt_change_upload_media_file')->name('pgcw.admin.upload.media.file.alt.change');
    });
    /*--------------------------
        PAGE BUILDER
    --------------------------*/
    Route::post('/update', 'PGCW\Admin\PageBuilderController@update_addon_content')->name('pgcw.admin.page.builder.update');
    Route::post('/new', 'PGCW\Admin\PageBuilderController@store_new_addon_content')->name('pgcw.admin.page.builder.new');
    Route::post('/delete', 'PGCW\Admin\PageBuilderController@delete')->name('pgcw.admin.page.builder.delete');
    Route::post('/update-order', 'PGCW\Admin\PageBuilderController@update_addon_order')->name('pgcw.admin.page.builder.update.addon.order');
    Route::post('/get-admin-markup', 'PGCW\Admin\PageBuilderController@get_admin_panel_addon_markup')->name('pgcw.admin.page.builder.get.addon.markup');
    /*==============================================
         IMAGE GALLERY ROUTES
    ==============================================*/
    Route::prefix('gallery-page')->group(function () {
        Route::get('/', 'PGCW\ImageGalleryPageController@index')->name('pgcw.admin.gallery.all');
        Route::post('/new', 'PGCW\ImageGalleryPageController@store')->name('pgcw.admin.gallery.new');
        Route::post('/update', 'PGCW\ImageGalleryPageController@update')->name('pgcw.admin.gallery.update');
        Route::post('/delete/{id}', 'PGCW\ImageGalleryPageController@delete')->name('pgcw.admin.gallery.delete');
        Route::post('/bulk-action', 'PGCW\ImageGalleryPageController@bulk_action')->name('pgcw.admin.gallery.bulk.action');
        Route::get('/page-settings', 'PGCW\ImageGalleryPageController@page_settings')->name('pgcw.admin.gallery.page.settings');
        Route::post('/page-settings', 'PGCW\ImageGalleryPageController@update_page_settings');
        /*------------------------
            IMAGE CATEGORY
        -------------------------*/
        Route::group(['prefix' => 'category'],function (){
            Route::get('/', 'PGCW\ImageGalleryPageController@category_index')->name('pgcw.admin.gallery.category');
            Route::post('/new', 'PGCW\ImageGalleryPageController@category_store')->name('pgcw.admin.gallery.category.new');
            Route::post('/update', 'PGCW\ImageGalleryPageController@category_update')->name('pgcw.admin.gallery.category.update');
            Route::post('/delete/{id}', 'PGCW\ImageGalleryPageController@category_delete')->name('pgcw.admin.gallery.category.delete');
            Route::post('/bulk-action', 'PGCW\ImageGalleryPageController@category_bulk_action')->name('pgcw.admin.gallery.category.bulk.action');
        });
        Route::post('/category-by-slug', 'PGCW\ImageGalleryPageController@category_by_slug')->name('pgcw.admin.gallery.category.by.lang');

    });
    /*==============================================
             WIDGETS MODULE ROUTES
    ==============================================*/
    Route::prefix('widgets')->group(function () {

        Route::get('/', 'PGCW\WidgetsController@index')->name('pgcw.admin.widgets');
        Route::post('/create', 'PGCW\WidgetsController@new_widget')->name('pgcw.admin.widgets.new');
        Route::post('/update', 'PGCW\WidgetsController@update_widget')->name('pgcw.admin.widgets.update');
        Route::post('/markup', 'PGCW\WidgetsController@widget_markup')->name('pgcw.admin.widgets.markup');
        Route::post('/update/order', 'PGCW\WidgetsController@update_order_widget')->name('pgcw.admin.widgets.update.order');
        Route::post('/delete', 'PGCW\WidgetsController@delete_widget')->name('pgcw.admin.widgets.delete');
    });
});


