<?php

use Illuminate\Support\Facades\Route;




Route::get('/login/admin', 'Auth\LoginController@showUceAdminLoginForm')->name('uce.admin.login');
Route::post('/logout/admin', 'UCE\AdminDashboardController@adminLogout')->name('uce.admin.logout');
Route::post('/login/admin', 'Auth\LoginController@uceAdminLogin');

Route::group(['middleware' => ['setlang:frontend', 'globalVariable', 'maintains_mode', 'HtmlMinifier']], function() {

	Route::get('/', 'UCE\FrontendController@index')->name('uce.homepage');
	Route::get('/p/{slug}', 'UCE\FrontendController@dynamic_single_page')->name('uce.frontend.dynamic.page');
	$announcements_page_slug = !empty(get_static_option('announcements_page_slug')) ? get_static_option('announcements_page_slug') : 'announcements';
	Route::get($announcements_page_slug, 'UCE\FrontendController@announcements')->name('frontend.announcements');
	Route::get($announcements_page_slug.'/{slug}', 'UCE\FrontendController@announcements_single')->name('uce.frontend.announcements.single');
});

// Route::get('/', 'UCE\FrontendController@index')->name('uce.homepage');

Route::prefix('admin-home')->group(function () {
	Route::get('/', 'UCE\AdminDashboardController@adminIndex')->name('uce.admin.home');
	Route::prefix('page')->group(function () {
        Route::get('/', 'UCE\PagesController@index')->name('uce.admin.page');
        Route::get('/new', 'UCE\PagesController@new_page')->name('uce.admin.page.new');
        Route::post('/new', 'UCE\PagesController@store_new_page');
        Route::get('/edit/{id}', 'UCE\PagesController@edit_page')->name('uce.admin.page.edit');
        Route::post('/update/{id}', 'UCE\PagesController@update_page')->name('uce.admin.page.update');
        Route::post('/delete/{id}', 'UCE\PagesController@delete_page')->name('uce.admin.page.delete');
        // Route::post('/bulk-action', 'UCE\PagesController@bulk_action')->name('admin.page.bulk.action');
        
    });
    Route::group(['prefix' => 'page-builder','namespace' => 'UCE\Admin','middleware' 	=> 'auth:uce_admin'],function () {
    	/*-------------------------
            HOME PAGE BUILDER
        -------------------------*/
    	Route::get('/home-page', 'PageBuilderController@homepage_builder')->name('uce.admin.home.page.builder');
        Route::post('/home-page', 'PageBuilderController@update_homepage_builder');

		/*-------------------------
		   DYNAMIC PAGE BUILDER
		-------------------------*/
		Route::get('/dynamic-page/{type}/{id}', 'PageBuilderController@dynamicpage_builder')->name('uce.admin.dynamic.page.builder');
		// Route::post('/dynamic-page', 'PageBuilderController@update_dynamicpage_builder')->name('admin.dynamic.page.builder.store');
	});
	Route::prefix('general-settings')->group(function () {
		/*----------------------------------------------------
		      SITE IDENTITY
		----------------------------------------------------*/
		Route::get('/site-identity', 'UCE\GeneralSettingsController@site_identity')->name('uce.admin.general.site.identity');
		Route::post('/site-identity', 'UCE\GeneralSettingsController@update_site_identity');

		/*----------------------------------------------------
         CUSTOM CSS SETTINGS
        ----------------------------------------------------*/
        Route::get('/custom-css', 'UCE\GeneralSettingsController@custom_css_settings')->name('uce.admin.general.custom.css');
        Route::post('/custom-css', 'UCE\GeneralSettingsController@update_custom_css_settings');
	});
	Route::prefix('announcements')->group(function() {
		/*----------------------------------------
            ANNOUNCEMENTS MODULE: ROUTEs
        ----------------------------------------*/
        Route::get('/all', 'UCE\AnnouncementsController@all_announcements')->name('uce.admin.announcements.all');
        Route::get('/new', 'UCE\AnnouncementsController@new_announcement')->name('uce.admin.announcements.new');
        Route::post('/new', 'UCE\AnnouncementsController@store_announcement');
        Route::get('/edit/{id}', 'UCE\AnnouncementsController@edit_announcement')->name('uce.admin.announcements.edit');
        Route::post('/update', 'UCE\AnnouncementsController@update_announcement')->name('uce.admin.announcements.update');
        Route::post('/delete/{id}', 'UCE\AnnouncementsController@delete_announcement')->name('uce.admin.announcements.delete');
        Route::post('/clone', 'UCE\AnnouncementsController@clone_announcement')->name('uce.admin.announcements.clone');
        Route::post('/bulk-action', 'UCE\AnnouncementsController@bulk_action')->name('uce.admin.announcements.bulk.action');

        /*----------------------------------------
        ANNOUNCEMENTS MODULE: CATEGORY ROUTES
         ----------------------------------------*/
        Route::group(['prefix' => 'category'],function (){
            //announcement category
            Route::get('/', 'UCE\AnnouncementsCategoryController@all_announcements_category')->name('uce.admin.announcements.category.all');
            Route::post('/new', 'UCE\AnnouncementsCategoryController@store_announcements_category')->name('uce.admin.announcements.category.new');
            Route::post('/update', 'UCE\AnnouncementsCategoryController@update_announcements_category')->name('uce.admin.announcements.category.update');
            Route::post('/delete/{id}', 'UCE\AnnouncementsCategoryController@delete_announcements_category')->name('uce.admin.announcements.category.delete');
            
            Route::post('/bulk-action', 'UCE\AnnouncementsCategoryController@bulk_action')->name('uce.admin.announcements.category.bulk.action');
        });
	});
	/*==============================================
          COUNTERUP ROUTES
    ==============================================*/
	Route::prefix('counterup')->group(function () {
        Route::get('/', 'UCE\CounterUpController@index')->name('uce.admin.counterup');
        Route::post('/', 'UCE\CounterUpController@store');
        Route::post('/update', 'UCE\CounterUpController@update')->name('uce.admin.counterup.update');
        Route::post('/delete/{id}', 'UCE\CounterUpController@delete')->name('uce.admin.counterup.delete');
        Route::post('/bulk-action', 'UCE\CounterUpController@bulk_action')->name('uce.admin.counterup.bulk.action');
    });
    /*==============================================
        TESTIMONIAL ROUTES
     ==============================================*/
    Route::prefix('testimonial')->group(function () {
        Route::get('/', 'UCE\TestimonialController@index')->name('uce.admin.testimonial');
        Route::post('/', 'UCE\TestimonialController@store');
        Route::post('/clone', 'UCE\TestimonialController@clone')->name('uce.admin.testimonial.clone');
        Route::post('/update', 'UCE\TestimonialController@update')->name('uce.admin.testimonial.update');
        Route::post('/delete/{id}', 'UCE\TestimonialController@delete')->name('uce.admin.testimonial.delete');
        Route::post('/bulk-action', 'UCE\TestimonialController@bulk_action')->name('uce.admin.testimonial.bulk.action');
    });
    /*==============================================
             WIDGETS MODULE ROUTES
    ==============================================*/
    Route::prefix('menu')->group(function () {
        Route::get('/', 'UCE\MenuController@index')->name('uce.admin.menu');
        Route::post('/new', 'UCE\MenuController@store_new_menu')->name('uce.admin.menu.new');
        Route::get('/edit/{id}', 'UCE\MenuController@edit_menu')->name('uce.admin.menu.edit');
        Route::post('/update/{id}', 'UCE\MenuController@update_menu')->name('uce.admin.menu.update');
        Route::post('/delete/{id}', 'UCE\MenuController@delete_menu')->name('uce.admin.menu.delete');
        Route::post('/default/{id}', 'UCE\MenuController@set_default_menu')->name('uce.admin.menu.default');
        Route::post('/mega-menu', 'UCE\MenuController@mega_menu_item_select_markup')->name('uce.admin.mega.menu.item.select.markup');
    });
	Route::prefix('media-upload')->group(function () {
        Route::post('/delete', 'MediaUploadController@delete_upload_media_file')->name('admin.upload.media.file.delete');
        Route::get('/page', 'UCE\MediaUploadController@all_upload_media_images_for_page')->name('uce.admin.upload.media.images.page');
        Route::post('/alt', 'MediaUploadController@alt_change_upload_media_file')->name('admin.upload.media.file.alt.change');
    });
	/*--------------------------
        PAGE BUILDER
    --------------------------*/
    Route::post('/update', 'UCE\Admin\PageBuilderController@update_addon_content')->name('uce.admin.page.builder.update');
    Route::post('/new', 'UCE\Admin\PageBuilderController@store_new_addon_content')->name('uce.admin.page.builder.new');
    Route::post('/delete', 'UCE\Admin\PageBuilderController@delete')->name('uce.admin.page.builder.delete');
    Route::post('/update-order', 'UCE\Admin\PageBuilderController@update_addon_order')->name('uce.admin.page.builder.update.addon.order');
    Route::post('/get-admin-markup', 'UCE\Admin\PageBuilderController@get_admin_panel_addon_markup')->name('uce.admin.page.builder.get.addon.markup');
	
	/*==============================================
         IMAGE GALLERY ROUTES
    ==============================================*/
    Route::prefix('gallery-page')->group(function () {
        Route::get('/', 'UCE\ImageGalleryPageController@index')->name('uce.admin.gallery.all');
        Route::post('/new', 'UCE\ImageGalleryPageController@store')->name('uce.admin.gallery.new');
        Route::post('/update', 'UCE\ImageGalleryPageController@update')->name('uce.admin.gallery.update');
        Route::post('/delete/{id}', 'UCE\ImageGalleryPageController@delete')->name('uce.admin.gallery.delete');
        Route::post('/bulk-action', 'UCE\ImageGalleryPageController@bulk_action')->name('uce.admin.gallery.bulk.action');
        Route::get('/page-settings', 'UCE\ImageGalleryPageController@page_settings')->name('uce.admin.gallery.page.settings');
        Route::post('/page-settings', 'UCE\ImageGalleryPageController@update_page_settings');
        /*------------------------
            IMAGE CATEGORY
        -------------------------*/
        Route::group(['prefix' => 'category'],function (){
            Route::get('/', 'UCE\ImageGalleryPageController@category_index')->name('uce.admin.gallery.category');
            Route::post('/new', 'UCE\ImageGalleryPageController@category_store')->name('uce.admin.gallery.category.new');
            Route::post('/update', 'UCE\ImageGalleryPageController@category_update')->name('uce.admin.gallery.category.update');
            Route::post('/delete/{id}', 'UCE\ImageGalleryPageController@category_delete')->name('uce.admin.gallery.category.delete');
            Route::post('/bulk-action', 'UCE\ImageGalleryPageController@category_bulk_action')->name('uce.admin.gallery.category.bulk.action');
        });
        Route::post('/category-by-slug', 'UCE\ImageGalleryPageController@category_by_slug')->name('uce.admin.gallery.category.by.lang');

    });
	/*==============================================
             WIDGETS MODULE ROUTES
    ==============================================*/
    Route::prefix('widgets')->group(function () {

        Route::get('/', 'UCE\WidgetsController@index')->name('uce.admin.widgets');
        Route::post('/create', 'UCE\WidgetsController@new_widget')->name('uce.admin.widgets.new');
        Route::post('/update', 'UCE\WidgetsController@update_widget')->name('uce.admin.widgets.update');
        Route::post('/markup', 'UCE\WidgetsController@widget_markup')->name('uce.admin.widgets.markup');
        Route::post('/update/order', 'UCE\WidgetsController@update_order_widget')->name('uce.admin.widgets.update.order');
        Route::post('/delete', 'UCE\WidgetsController@delete_widget')->name('uce.admin.widgets.delete');
    });
});


