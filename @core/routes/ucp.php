<?php

use Illuminate\Support\Facades\Route;


Route::get('/login/admin', 'Auth\LoginController@showUcpAdminLoginForm')->name('ucp.admin.login');
Route::post('/logout/admin', 'UCP\AdminDashboardController@adminLogout')->name('ucp.admin.logout');
Route::post('/login/admin', 'Auth\LoginController@ucpAdminLogin');

Route::group(['middleware' => ['setlang:frontend', 'globalVariable', 'maintains_mode', 'HtmlMinifier']], function() {

	Route::get('/', 'UCP\FrontendController@index')->name('ucp.homepage');
	Route::get('/p/{slug}', 'UCP\FrontendController@dynamic_single_page')->name('ucp.frontend.dynamic.page');
	$announcements_page_slug = !empty(get_static_option('announcements_page_slug')) ? get_static_option('announcements_page_slug') : 'announcements';
	Route::get($announcements_page_slug, 'UCP\FrontendController@announcements')->name('ucp.frontend.announcements');
	Route::get($announcements_page_slug.'/{slug}', 'UCP\FrontendController@announcements_single')->name('ucp.frontend.announcements.single');
});

// Route::get('/', 'UCP\FrontendController@index')->name('ucp.homepage');

Route::prefix('admin-home')->group(function () {
	Route::get('/', 'UCP\AdminDashboardController@adminIndex')->name('ucp.admin.home');
	Route::prefix('page')->group(function () {
        Route::get('/', 'UCP\PagesController@index')->name('ucp.admin.page');
        Route::get('/new', 'UCP\PagesController@new_page')->name('ucp.admin.page.new');
        Route::post('/new', 'UCP\PagesController@store_new_page');
        Route::get('/edit/{id}', 'UCP\PagesController@edit_page')->name('ucp.admin.page.edit');
        Route::post('/update/{id}', 'UCP\PagesController@update_page')->name('ucp.admin.page.update');
        Route::post('/delete/{id}', 'UCP\PagesController@delete_page')->name('ucp.admin.page.delete');
        Route::get('/profile-update', 'UCP\AdminDashboardController@admin_profile')->name('ucp.admin.profile.update');
        Route::post('/profile-update', 'UCP\AdminDashboardController@admin_profile_update');
        Route::post('/bulk-action', 'UCP\PagesController@bulk_action')->name('ucp.admin.page.bulk.action');
        
    });
    // Route::post('/logout/admin', 'UCP\AdminDashboardController@adminLogout')->name('ucp.admin.logout');
    Route::group(['prefix' => 'page-builder','namespace' => 'UCP\Admin','middleware' 	=> 'auth:ucp_admin'],function () {
    	/*-------------------------
            HOME PAGE BUILDER
        -------------------------*/
    	Route::get('/home-page', 'PageBuilderController@homepage_builder')->name('ucp.admin.home.page.builder');
        Route::post('/home-page', 'PageBuilderController@update_homepage_builder');

		/*-------------------------
		   DYNAMIC PAGE BUILDER
		-------------------------*/
		Route::get('/dynamic-page/{type}/{id}', 'PageBuilderController@dynamicpage_builder')->name('ucp.admin.dynamic.page.builder');
		Route::post('/dynamic-page', 'PageBuilderController@update_dynamicpage_builder')->name('ucp.admin.dynamic.page.builder.store');
	});
	Route::prefix('general-settings')->group(function () {
		/*----------------------------------------------------
		      SITE IDENTITY
		----------------------------------------------------*/
		Route::get('/site-identity', 'UCP\GeneralSettingsController@site_identity')->name('ucp.admin.general.site.identity');
		Route::post('/site-identity', 'UCP\GeneralSettingsController@update_site_identity');
        /*----------------------------------------------------
            BASIC SETTINGS
        ----------------------------------------------------*/
        Route::get('/basic-settings', 'UCP\GeneralSettingsController@basic_settings')->name('ucp.admin.general.basic.settings');
        Route::post('/basic-settings', 'UCP\GeneralSettingsController@update_basic_settings');
        /*----------------------------------------------------
            COLOR SETTINGS
      ----------------------------------------------------*/
        Route::get('/color-settings', 'UCP\GeneralSettingsController@color_settings')->name('ucp.admin.general.color.settings');
        Route::post('/color-settings', 'UCP\GeneralSettingsController@update_color_settings');
        /*----------------------------------------------------
          SEO SETTINGS
        ----------------------------------------------------*/
        Route::get('/seo-settings', 'UCP\GeneralSettingsController@seo_settings')->name('ucp.admin.general.seo.settings');
        Route::post('/seo-settings', 'UCP\GeneralSettingsController@update_seo_settings');
        /*----------------------------------------------------
          CUSTOM SCRIPT SETTINGS
         ----------------------------------------------------*/
        Route::get('/scripts', 'UCP\GeneralSettingsController@scripts_settings')->name('ucp.admin.general.scripts.settings');
        Route::post('/scripts', 'UCP\GeneralSettingsController@update_scripts_settings');
        /*----------------------------------------------------
          EMAIL TEMPLATE SETTINGS
        ----------------------------------------------------*/
        Route::get('/email-template', 'UCP\GeneralSettingsController@email_template_settings')->name('ucp.admin.general.email.template');
        Route::post('/email-template', 'UCP\GeneralSettingsController@update_email_template_settings');
        /*----------------------------------------------------
          EMAIL  SETTINGS
         ----------------------------------------------------*/
        Route::get('/email-settings', 'UCP\GeneralSettingsController@email_settings')->name('ucp.admin.general.email.settings');
        Route::post('/email-settings', 'UCP\GeneralSettingsController@update_email_settings');
        /*----------------------------------------------------
          TYPOGRAPHY SETTINGS
        ----------------------------------------------------*/
        Route::get('/typography-settings', 'UCP\GeneralSettingsController@typography_settings')->name('ucp.admin.general.typography.settings');
        Route::post('/typography-settings', 'UCP\GeneralSettingsController@update_typography_settings');
        Route::post('/typography-settings/single', 'UCP\GeneralSettingsController@get_single_font_variant')->name('ucp.admin.general.typography.single');
        /*----------------------------------------------------
          CACHE SETTINGS
         ----------------------------------------------------*/
        Route::get('/cache-settings', 'UCP\GeneralSettingsController@cache_settings')->name('ucp.admin.general.cache.settings');
        Route::post('/cache-settings', 'UCP\GeneralSettingsController@update_cache_settings');
        /*----------------------------------------------------
         PAGE SETTINGS
        ----------------------------------------------------*/
        Route::get('/page-settings', 'UCP\GeneralSettingsController@page_settings')->name('ucp.admin.general.page.settings');
        Route::post('/page-settings', 'UCP\GeneralSettingsController@update_page_settings');
        /*----------------------------------------------------
         UPDATE SYSTEM SETTINGS
        ----------------------------------------------------*/
        Route::get('/update-system', 'GeneralSettingsController@update_system')->name('admin.general.update.system');
        Route::post('/update-system', 'GeneralSettingsController@update_system_version');

		/*----------------------------------------------------
         CUSTOM CSS SETTINGS
        ----------------------------------------------------*/
        Route::get('/custom-css', 'UCP\GeneralSettingsController@custom_css_settings')->name('ucp.admin.general.custom.css');
        Route::post('/custom-css', 'UCP\GeneralSettingsController@update_custom_css_settings');
        /*----------------------------------------------------
          CUSTOM JAVASCRIPT SETTINGS
         ----------------------------------------------------*/
        Route::get('/custom-js', 'UCP\GeneralSettingsController@custom_js_settings')->name('ucp.admin.general.custom.js');
        Route::post('/custom-js', 'UCP\GeneralSettingsController@update_custom_js_settings');
        /*----------------------------------------------------
          SMTP SETTINGS
         ----------------------------------------------------*/
        Route::get('/smtp-settings', 'UCP\GeneralSettingsController@smtp_settings')->name('ucp.admin.general.smtp.settings');
        Route::post('/smtp-settings', 'UCP\GeneralSettingsController@update_smtp_settings');
        Route::post('/smtp-settings/test', 'UCP\GeneralSettingsController@test_smtp_settings')->name('ucp.admin.general.smtp.settings.test');
        /*----------------------------------------------------
         GDPR SETTINGS
        ----------------------------------------------------*/
        Route::get('/gdpr-settings', 'UCP\GeneralSettingsController@gdpr_settings')->name('ucp.admin.general.gdpr.settings');
        Route::post('/gdpr-settings', 'UCP\GeneralSettingsController@update_gdpr_cookie_settings');
        /*----------------------------------------------------
         PRELOADER SETTINGS
        ----------------------------------------------------*/
        Route::get('/preloader-settings', 'UCP\GeneralSettingsController@preloader_settings')->name('ucp.admin.general.preloader.settings');
        Route::post('/preloader-settings', 'UCP\GeneralSettingsController@update_preloader_settings');
        /*----------------------------------------------------
         POPUP SETTINGS
        ----------------------------------------------------*/
        Route::get('/popup-settings', 'UCP\GeneralSettingsController@popup_settings')->name('ucp.admin.general.popup.settings');
        Route::post('/popup-settings', 'UCP\GeneralSettingsController@update_popup_settings');
        /*----------------------------------------------------
          SITEMAP SETTINGS
         ----------------------------------------------------*/
        Route::get('/sitemap-settings', 'UCP\GeneralSettingsController@sitemap_settings')->name('ucp.admin.general.sitemap.settings');
        Route::post('/sitemap-settings', 'UCP\GeneralSettingsController@update_sitemap_settings');
        Route::post('/sitemap-settings/delete', 'UCP\GeneralSettingsController@delete_sitemap_settings')->name('ucp.admin.general.sitemap.settings.delete');
        /*----------------------------------------------------
          RSS SETTINGS
         ----------------------------------------------------*/
        Route::get('/rss-settings', 'UCP\GeneralSettingsController@rss_feed_settings')->name('ucp.admin.general.rss.feed.settings');
        Route::post('/rss-settings', 'UCP\GeneralSettingsController@update_rss_feed_settings');
        /*----------------------------------------------------
          MODULE SETTINGS
         ----------------------------------------------------*/
        Route::get('/module-settings', 'UCP\GeneralSettingsController@module_settings')->name('ucp.admin.general.module.settings');
        Route::post('/module-settings', 'UCP\GeneralSettingsController@store_module_settings');
        /*----------------------------------------------------
            DATABASE UPGRADE
        ----------------------------------------------------*/
        Route::get('/database-upgrade', 'UCP\GeneralSettingsController@database_upgrade')->name('ucp.admin.general.database.upgrade');
        Route::post('/database-upgrade', 'UCP\GeneralSettingsController@database_upgrade_post');
        /*----------------------------------------------------
            LICENSE SETTINGS
        ----------------------------------------------------*/
        Route::get('/license-setting', 'UCP\GeneralSettingsController@license_settings')->name('ucp.admin.general.license.settings');
        Route::post('/license-setting', 'UCP\GeneralSettingsController@update_license_settings');

        /*----------------------------------------------------
         REGENERATE IMAGE SETTINGS
        ----------------------------------------------------*/
        Route::get('/regenerate-image', 'UCP\GeneralSettingsController@regenerate_image_settings')->name('ucp.admin.general.regenerate.thumbnail');
        Route::post('/regenerate-image', 'UCP\GeneralSettingsController@update_regenerate_image_settings');
	});
	Route::prefix('announcements')->group(function() {
		/*----------------------------------------
            ANNOUNCEMENTS MODULE: ROUTEs
        ----------------------------------------*/
        Route::get('/all', 'UCP\AnnouncementsController@all_announcements')->name('ucp.admin.announcements.all');
        Route::get('/new', 'UCP\AnnouncementsController@new_announcement')->name('ucp.admin.announcements.new');
        Route::post('/new', 'UCP\AnnouncementsController@store_announcement');
        Route::get('/edit/{id}', 'UCP\AnnouncementsController@edit_announcement')->name('ucp.admin.announcements.edit');
        Route::post('/update', 'UCP\AnnouncementsController@update_announcement')->name('ucp.admin.announcements.update');
        Route::post('/delete/{id}', 'UCP\AnnouncementsController@delete_announcement')->name('ucp.admin.announcements.delete');
        Route::post('/clone', 'UCP\AnnouncementsController@clone_announcement')->name('ucp.admin.announcements.clone');
        Route::post('/bulk-action', 'UCP\AnnouncementsController@bulk_action')->name('ucp.admin.announcements.bulk.action');

        /*----------------------------------------
        ANNOUNCEMENTS MODULE: CATEGORY ROUTES
         ----------------------------------------*/
        Route::group(['prefix' => 'category'],function (){
            //announcement category
            Route::get('/', 'UCP\AnnouncementsCategoryController@all_announcements_category')->name('ucp.admin.announcements.category.all');
            Route::post('/new', 'UCP\AnnouncementsCategoryController@store_announcements_category')->name('ucp.admin.announcements.category.new');
            Route::post('/update', 'UCP\AnnouncementsCategoryController@update_announcements_category')->name('ucp.admin.announcements.category.update');
            Route::post('/delete/{id}', 'UCP\AnnouncementsCategoryController@delete_announcements_category')->name('ucp.admin.announcements.category.delete');
            
            Route::post('/bulk-action', 'UCP\AnnouncementsCategoryController@bulk_action')->name('ucp.admin.announcements.category.bulk.action');
        });
	});
	/*==============================================
          COUNTERUP ROUTES
    ==============================================*/
	Route::prefix('counterup')->group(function () {
        Route::get('/', 'UCP\CounterUpController@index')->name('ucp.admin.counterup');
        Route::post('/', 'UCP\CounterUpController@store');
        Route::post('/update', 'UCP\CounterUpController@update')->name('ucp.admin.counterup.update');
        Route::post('/delete/{id}', 'UCP\CounterUpController@delete')->name('ucp.admin.counterup.delete');
        Route::post('/bulk-action', 'UCP\CounterUpController@bulk_action')->name('ucp.admin.counterup.bulk.action');
    });
    /*==============================================
        TESTIMONIAL ROUTES
     ==============================================*/
    Route::prefix('testimonial')->group(function () {
        Route::get('/', 'UCP\TestimonialController@index')->name('ucp.admin.testimonial');
        Route::post('/', 'UCP\TestimonialController@store');
        Route::post('/clone', 'UCP\TestimonialController@clone')->name('ucp.admin.testimonial.clone');
        Route::post('/update', 'UCP\TestimonialController@update')->name('ucp.admin.testimonial.update');
        Route::post('/delete/{id}', 'UCP\TestimonialController@delete')->name('ucp.admin.testimonial.delete');
        Route::post('/bulk-action', 'UCP\TestimonialController@bulk_action')->name('ucp.admin.testimonial.bulk.action');
    });
    /*==============================================
             WIDGETS MODULE ROUTES
    ==============================================*/
    Route::prefix('menu')->group(function () {
        Route::get('/', 'UCP\MenuController@index')->name('ucp.admin.menu');
        Route::post('/new', 'UCP\MenuController@store_new_menu')->name('ucp.admin.menu.new');
        Route::get('/edit/{id}', 'UCP\MenuController@edit_menu')->name('ucp.admin.menu.edit');
        Route::post('/update/{id}', 'UCP\MenuController@update_menu')->name('ucp.admin.menu.update');
        Route::post('/delete/{id}', 'UCP\MenuController@delete_menu')->name('ucp.admin.menu.delete');
        Route::post('/default/{id}', 'UCP\MenuController@set_default_menu')->name('ucp.admin.menu.default');
        Route::post('/mega-menu', 'UCP\MenuController@mega_menu_item_select_markup')->name('ucp.admin.mega.menu.item.select.markup');
    });
    Route::prefix('media-upload')->group(function () {
        Route::post('/delete', 'MediaUploadController@delete_upload_media_file')->name('ucp.admin.upload.media.file.delete');
        Route::get('/page', 'UCP\MediaUploadController@all_upload_media_images_for_page')->name('ucp.admin.upload.media.images.page');
        Route::post('/alt', 'MediaUploadController@alt_change_upload_media_file')->name('ucp.admin.upload.media.file.alt.change');
    });
    /*--------------------------
        PAGE BUILDER
    --------------------------*/
    Route::post('/update', 'UCP\Admin\PageBuilderController@update_addon_content')->name('ucp.admin.page.builder.update');
    Route::post('/new', 'UCP\Admin\PageBuilderController@store_new_addon_content')->name('ucp.admin.page.builder.new');
    Route::post('/delete', 'UCP\Admin\PageBuilderController@delete')->name('ucp.admin.page.builder.delete');
    Route::post('/update-order', 'UCP\Admin\PageBuilderController@update_addon_order')->name('ucp.admin.page.builder.update.addon.order');
    Route::post('/get-admin-markup', 'UCP\Admin\PageBuilderController@get_admin_panel_addon_markup')->name('ucp.admin.page.builder.get.addon.markup');
    /*==============================================
         IMAGE GALLERY ROUTES
    ==============================================*/
    Route::prefix('gallery-page')->group(function () {
        Route::get('/', 'UCP\ImageGalleryPageController@index')->name('ucp.admin.gallery.all');
        Route::post('/new', 'UCP\ImageGalleryPageController@store')->name('ucp.admin.gallery.new');
        Route::post('/update', 'UCP\ImageGalleryPageController@update')->name('ucp.admin.gallery.update');
        Route::post('/delete/{id}', 'UCP\ImageGalleryPageController@delete')->name('ucp.admin.gallery.delete');
        Route::post('/bulk-action', 'UCP\ImageGalleryPageController@bulk_action')->name('ucp.admin.gallery.bulk.action');
        Route::get('/page-settings', 'UCP\ImageGalleryPageController@page_settings')->name('ucp.admin.gallery.page.settings');
        Route::post('/page-settings', 'UCP\ImageGalleryPageController@update_page_settings');
        /*------------------------
            IMAGE CATEGORY
        -------------------------*/
        Route::group(['prefix' => 'category'],function (){
            Route::get('/', 'UCP\ImageGalleryPageController@category_index')->name('ucp.admin.gallery.category');
            Route::post('/new', 'UCP\ImageGalleryPageController@category_store')->name('ucp.admin.gallery.category.new');
            Route::post('/update', 'UCP\ImageGalleryPageController@category_update')->name('ucp.admin.gallery.category.update');
            Route::post('/delete/{id}', 'UCP\ImageGalleryPageController@category_delete')->name('ucp.admin.gallery.category.delete');
            Route::post('/bulk-action', 'UCP\ImageGalleryPageController@category_bulk_action')->name('ucp.admin.gallery.category.bulk.action');
        });
        Route::post('/category-by-slug', 'UCP\ImageGalleryPageController@category_by_slug')->name('ucp.admin.gallery.category.by.lang');

    });
    /*==============================================
             WIDGETS MODULE ROUTES
    ==============================================*/
    Route::prefix('widgets')->group(function () {

        Route::get('/', 'UCP\WidgetsController@index')->name('ucp.admin.widgets');
        Route::post('/create', 'UCP\WidgetsController@new_widget')->name('ucp.admin.widgets.new');
        Route::post('/update', 'UCP\WidgetsController@update_widget')->name('ucp.admin.widgets.update');
        Route::post('/markup', 'UCP\WidgetsController@widget_markup')->name('ucp.admin.widgets.markup');
        Route::post('/update/order', 'UCP\WidgetsController@update_order_widget')->name('ucp.admin.widgets.update.order');
        Route::post('/delete', 'UCP\WidgetsController@delete_widget')->name('ucp.admin.widgets.delete');
    });
});


