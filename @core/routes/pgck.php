<?php

use Illuminate\Support\Facades\Route;


Route::get('/login/admin', 'Auth\LoginController@showPgckAdminLoginForm')->name('pgck.admin.login');
Route::post('/logout/admin', 'PGCK\AdminDashboardController@adminLogout')->name('pgck.admin.logout');
Route::post('/login/admin', 'Auth\LoginController@pgckAdminLogin');

Route::group(['middleware' => ['setlang:frontend', 'globalVariable', 'maintains_mode', 'HtmlMinifier']], function() {

  Route::get('/', 'PGCK\FrontendController@index')->name('pgck.homepage');
  Route::get('/p/{slug}', 'PGCK\FrontendController@dynamic_single_page')->name('pgck.frontend.dynamic.page');
  $announcements_page_slug = !empty(get_static_option('announcements_page_slug')) ? get_static_option('announcements_page_slug') : 'announcements';
  Route::get($announcements_page_slug, 'PGCK\FrontendController@announcements')->name('pgck.frontend.announcements');
  Route::get($announcements_page_slug.'/{slug}', 'PGCK\FrontendController@announcements_single')->name('pgck.frontend.announcements.single');
});

// Route::get('/', 'PGCK\FrontendController@index')->name('pgck.homepage');

Route::prefix('admin-home')->group(function () {
  Route::get('/', 'PGCK\AdminDashboardController@adminIndex')->name('pgck.admin.home');
  Route::prefix('page')->group(function () {
        Route::get('/', 'PGCK\PagesController@index')->name('pgck.admin.page');
        Route::get('/new', 'PGCK\PagesController@new_page')->name('pgck.admin.page.new');
        Route::post('/new', 'PGCK\PagesController@store_new_page');
        Route::get('/edit/{id}', 'PGCK\PagesController@edit_page')->name('pgck.admin.page.edit');
        Route::post('/update/{id}', 'PGCK\PagesController@update_page')->name('pgck.admin.page.update');
        Route::post('/delete/{id}', 'PGCK\PagesController@delete_page')->name('pgck.admin.page.delete');
        Route::get('/profile-update', 'PGCK\AdminDashboardController@admin_profile')->name('pgck.admin.profile.update');
        Route::post('/profile-update', 'PGCK\AdminDashboardController@admin_profile_update');
        Route::post('/bulk-action', 'PGCK\PagesController@bulk_action')->name('pgck.admin.page.bulk.action');
        
    });
    // Route::post('/logout/admin', 'PGCK\AdminDashboardController@adminLogout')->name('pgck.admin.logout');
    Route::group(['prefix' => 'page-builder','namespace' => 'PGCK\Admin','middleware'   => 'auth:pgck_admin'],function () {
      /*-------------------------
            HOME PAGE BUILDER
        -------------------------*/
      Route::get('/home-page', 'PageBuilderController@homepage_builder')->name('pgck.admin.home.page.builder');
        Route::post('/home-page', 'PageBuilderController@update_homepage_builder');

    /*-------------------------
       DYNAMIC PAGE BUILDER
    -------------------------*/
    Route::get('/dynamic-page/{type}/{id}', 'PageBuilderController@dynamicpage_builder')->name('pgck.admin.dynamic.page.builder');
    Route::post('/dynamic-page', 'PageBuilderController@update_dynamicpage_builder')->name('pgck.admin.dynamic.page.builder.store');
  });
  Route::prefix('general-settings')->group(function () {
    /*----------------------------------------------------
          SITE IDENTITY
    ----------------------------------------------------*/
    Route::get('/site-identity', 'PGCK\GeneralSettingsController@site_identity')->name('pgck.admin.general.site.identity');
    Route::post('/site-identity', 'PGCK\GeneralSettingsController@update_site_identity');
        /*----------------------------------------------------
            BASIC SETTINGS
        ----------------------------------------------------*/
        Route::get('/basic-settings', 'PGCK\GeneralSettingsController@basic_settings')->name('pgck.admin.general.basic.settings');
        Route::post('/basic-settings', 'PGCK\GeneralSettingsController@update_basic_settings');
        /*----------------------------------------------------
            COLOR SETTINGS
      ----------------------------------------------------*/
        Route::get('/color-settings', 'PGCK\GeneralSettingsController@color_settings')->name('pgck.admin.general.color.settings');
        Route::post('/color-settings', 'PGCK\GeneralSettingsController@update_color_settings');
        /*----------------------------------------------------
          SEO SETTINGS
        ----------------------------------------------------*/
        Route::get('/seo-settings', 'PGCK\GeneralSettingsController@seo_settings')->name('pgck.admin.general.seo.settings');
        Route::post('/seo-settings', 'PGCK\GeneralSettingsController@update_seo_settings');
        /*----------------------------------------------------
          CUSTOM SCRIPT SETTINGS
         ----------------------------------------------------*/
        Route::get('/scripts', 'PGCK\GeneralSettingsController@scripts_settings')->name('pgck.admin.general.scripts.settings');
        Route::post('/scripts', 'PGCK\GeneralSettingsController@update_scripts_settings');
        /*----------------------------------------------------
          EMAIL TEMPLATE SETTINGS
        ----------------------------------------------------*/
        Route::get('/email-template', 'PGCK\GeneralSettingsController@email_template_settings')->name('pgck.admin.general.email.template');
        Route::post('/email-template', 'PGCK\GeneralSettingsController@update_email_template_settings');
        /*----------------------------------------------------
          EMAIL  SETTINGS
         ----------------------------------------------------*/
        Route::get('/email-settings', 'PGCK\GeneralSettingsController@email_settings')->name('pgck.admin.general.email.settings');
        Route::post('/email-settings', 'PGCK\GeneralSettingsController@update_email_settings');
        /*----------------------------------------------------
          TYPOGRAPHY SETTINGS
        ----------------------------------------------------*/
        Route::get('/typography-settings', 'PGCK\GeneralSettingsController@typography_settings')->name('pgck.admin.general.typography.settings');
        Route::post('/typography-settings', 'PGCK\GeneralSettingsController@update_typography_settings');
        Route::post('/typography-settings/single', 'PGCK\GeneralSettingsController@get_single_font_variant')->name('pgck.admin.general.typography.single');
        /*----------------------------------------------------
          CACHE SETTINGS
         ----------------------------------------------------*/
        Route::get('/cache-settings', 'PGCK\GeneralSettingsController@cache_settings')->name('pgck.admin.general.cache.settings');
        Route::post('/cache-settings', 'PGCK\GeneralSettingsController@update_cache_settings');
        /*----------------------------------------------------
         PAGE SETTINGS
        ----------------------------------------------------*/
        Route::get('/page-settings', 'PGCK\GeneralSettingsController@page_settings')->name('pgck.admin.general.page.settings');
        Route::post('/page-settings', 'PGCK\GeneralSettingsController@update_page_settings');
        /*----------------------------------------------------
         UPDATE SYSTEM SETTINGS
        ----------------------------------------------------*/
        Route::get('/update-system', 'GeneralSettingsController@update_system')->name('admin.general.update.system');
        Route::post('/update-system', 'GeneralSettingsController@update_system_version');

    /*----------------------------------------------------
         CUSTOM CSS SETTINGS
        ----------------------------------------------------*/
        Route::get('/custom-css', 'PGCK\GeneralSettingsController@custom_css_settings')->name('pgck.admin.general.custom.css');
        Route::post('/custom-css', 'PGCK\GeneralSettingsController@update_custom_css_settings');
        /*----------------------------------------------------
          CUSTOM JAVASCRIPT SETTINGS
         ----------------------------------------------------*/
        Route::get('/custom-js', 'PGCK\GeneralSettingsController@custom_js_settings')->name('pgck.admin.general.custom.js');
        Route::post('/custom-js', 'PGCK\GeneralSettingsController@update_custom_js_settings');
        /*----------------------------------------------------
          SMTP SETTINGS
         ----------------------------------------------------*/
        Route::get('/smtp-settings', 'PGCK\GeneralSettingsController@smtp_settings')->name('pgck.admin.general.smtp.settings');
        Route::post('/smtp-settings', 'PGCK\GeneralSettingsController@update_smtp_settings');
        Route::post('/smtp-settings/test', 'PGCK\GeneralSettingsController@test_smtp_settings')->name('pgck.admin.general.smtp.settings.test');
        /*----------------------------------------------------
         GDPR SETTINGS
        ----------------------------------------------------*/
        Route::get('/gdpr-settings', 'PGCK\GeneralSettingsController@gdpr_settings')->name('pgck.admin.general.gdpr.settings');
        Route::post('/gdpr-settings', 'PGCK\GeneralSettingsController@update_gdpr_cookie_settings');
        /*----------------------------------------------------
         PRELOADER SETTINGS
        ----------------------------------------------------*/
        Route::get('/preloader-settings', 'PGCK\GeneralSettingsController@preloader_settings')->name('pgck.admin.general.preloader.settings');
        Route::post('/preloader-settings', 'PGCK\GeneralSettingsController@update_preloader_settings');
        /*----------------------------------------------------
         POPUP SETTINGS
        ----------------------------------------------------*/
        Route::get('/popup-settings', 'PGCK\GeneralSettingsController@popup_settings')->name('pgck.admin.general.popup.settings');
        Route::post('/popup-settings', 'PGCK\GeneralSettingsController@update_popup_settings');
        /*----------------------------------------------------
          SITEMAP SETTINGS
         ----------------------------------------------------*/
        Route::get('/sitemap-settings', 'PGCK\GeneralSettingsController@sitemap_settings')->name('pgck.admin.general.sitemap.settings');
        Route::post('/sitemap-settings', 'PGCK\GeneralSettingsController@update_sitemap_settings');
        Route::post('/sitemap-settings/delete', 'PGCK\GeneralSettingsController@delete_sitemap_settings')->name('pgck.admin.general.sitemap.settings.delete');
        /*----------------------------------------------------
          RSS SETTINGS
         ----------------------------------------------------*/
        Route::get('/rss-settings', 'PGCK\GeneralSettingsController@rss_feed_settings')->name('pgck.admin.general.rss.feed.settings');
        Route::post('/rss-settings', 'PGCK\GeneralSettingsController@update_rss_feed_settings');
        /*----------------------------------------------------
          MODULE SETTINGS
         ----------------------------------------------------*/
        Route::get('/module-settings', 'PGCK\GeneralSettingsController@module_settings')->name('pgck.admin.general.module.settings');
        Route::post('/module-settings', 'PGCK\GeneralSettingsController@store_module_settings');
        /*----------------------------------------------------
            DATABASE UPGRADE
        ----------------------------------------------------*/
        Route::get('/database-upgrade', 'PGCK\GeneralSettingsController@database_upgrade')->name('pgck.admin.general.database.upgrade');
        Route::post('/database-upgrade', 'PGCK\GeneralSettingsController@database_upgrade_post');
        /*----------------------------------------------------
            LICENSE SETTINGS
        ----------------------------------------------------*/
        Route::get('/license-setting', 'PGCK\GeneralSettingsController@license_settings')->name('pgck.admin.general.license.settings');
        Route::post('/license-setting', 'PGCK\GeneralSettingsController@update_license_settings');

        /*----------------------------------------------------
         REGENERATE IMAGE SETTINGS
        ----------------------------------------------------*/
        Route::get('/regenerate-image', 'PGCK\GeneralSettingsController@regenerate_image_settings')->name('pgck.admin.general.regenerate.thumbnail');
        Route::post('/regenerate-image', 'PGCK\GeneralSettingsController@update_regenerate_image_settings');
  });
  Route::prefix('announcements')->group(function() {
    /*----------------------------------------
            ANNOUNCEMENTS MODULE: ROUTEs
        ----------------------------------------*/
        Route::get('/all', 'PGCK\AnnouncementsController@all_announcements')->name('pgck.admin.announcements.all');
        Route::get('/new', 'PGCK\AnnouncementsController@new_announcement')->name('pgck.admin.announcements.new');
        Route::post('/new', 'PGCK\AnnouncementsController@store_announcement');
        Route::get('/edit/{id}', 'PGCK\AnnouncementsController@edit_announcement')->name('pgck.admin.announcements.edit');
        Route::post('/update', 'PGCK\AnnouncementsController@update_announcement')->name('pgck.admin.announcements.update');
        Route::post('/delete/{id}', 'PGCK\AnnouncementsController@delete_announcement')->name('pgck.admin.announcements.delete');
        Route::post('/clone', 'PGCK\AnnouncementsController@clone_announcement')->name('pgck.admin.announcements.clone');
        Route::post('/bulk-action', 'PGCK\AnnouncementsController@bulk_action')->name('pgck.admin.announcements.bulk.action');

        /*----------------------------------------
        ANNOUNCEMENTS MODULE: CATEGORY ROUTES
         ----------------------------------------*/
        Route::group(['prefix' => 'category'],function (){
            //announcement category
            Route::get('/', 'PGCK\AnnouncementsCategoryController@all_announcements_category')->name('pgck.admin.announcements.category.all');
            Route::post('/new', 'PGCK\AnnouncementsCategoryController@store_announcements_category')->name('pgck.admin.announcements.category.new');
            Route::post('/update', 'PGCK\AnnouncementsCategoryController@update_announcements_category')->name('pgck.admin.announcements.category.update');
            Route::post('/delete/{id}', 'PGCK\AnnouncementsCategoryController@delete_announcements_category')->name('pgck.admin.announcements.category.delete');
            
            Route::post('/bulk-action', 'PGCK\AnnouncementsCategoryController@bulk_action')->name('pgck.admin.announcements.category.bulk.action');
        });
  });
  /*==============================================
          COUNTERUP ROUTES
    ==============================================*/
  Route::prefix('counterup')->group(function () {
        Route::get('/', 'PGCK\CounterUpController@index')->name('pgck.admin.counterup');
        Route::post('/', 'PGCK\CounterUpController@store');
        Route::post('/update', 'PGCK\CounterUpController@update')->name('pgck.admin.counterup.update');
        Route::post('/delete/{id}', 'PGCK\CounterUpController@delete')->name('pgck.admin.counterup.delete');
        Route::post('/bulk-action', 'PGCK\CounterUpController@bulk_action')->name('pgck.admin.counterup.bulk.action');
    });
    /*==============================================
        TESTIMONIAL ROUTES
     ==============================================*/
    Route::prefix('testimonial')->group(function () {
        Route::get('/', 'PGCK\TestimonialController@index')->name('pgck.admin.testimonial');
        Route::post('/', 'PGCK\TestimonialController@store');
        Route::post('/clone', 'PGCK\TestimonialController@clone')->name('pgck.admin.testimonial.clone');
        Route::post('/update', 'PGCK\TestimonialController@update')->name('pgck.admin.testimonial.update');
        Route::post('/delete/{id}', 'PGCK\TestimonialController@delete')->name('pgck.admin.testimonial.delete');
        Route::post('/bulk-action', 'PGCK\TestimonialController@bulk_action')->name('pgck.admin.testimonial.bulk.action');
    });
    /*==============================================
             WIDGETS MODULE ROUTES
    ==============================================*/
    Route::prefix('menu')->group(function () {
        Route::get('/', 'PGCK\MenuController@index')->name('pgck.admin.menu');
        Route::post('/new', 'PGCK\MenuController@store_new_menu')->name('pgck.admin.menu.new');
        Route::get('/edit/{id}', 'PGCK\MenuController@edit_menu')->name('pgck.admin.menu.edit');
        Route::post('/update/{id}', 'PGCK\MenuController@update_menu')->name('pgck.admin.menu.update');
        Route::post('/delete/{id}', 'PGCK\MenuController@delete_menu')->name('pgck.admin.menu.delete');
        Route::post('/default/{id}', 'PGCK\MenuController@set_default_menu')->name('pgck.admin.menu.default');
        Route::post('/mega-menu', 'PGCK\MenuController@mega_menu_item_select_markup')->name('pgck.admin.mega.menu.item.select.markup');
    });
    Route::prefix('media-upload')->group(function () {
        Route::post('/delete', 'MediaUploadController@delete_upload_media_file')->name('pgck.admin.upload.media.file.delete');
        Route::get('/page', 'PGCK\MediaUploadController@all_upload_media_images_for_page')->name('pgck.admin.upload.media.images.page');
        Route::post('/alt', 'MediaUploadController@alt_change_upload_media_file')->name('pgck.admin.upload.media.file.alt.change');
    });
    /*--------------------------
        PAGE BUILDER
    --------------------------*/
    Route::post('/update', 'PGCK\Admin\PageBuilderController@update_addon_content')->name('pgck.admin.page.builder.update');
    Route::post('/new', 'PGCK\Admin\PageBuilderController@store_new_addon_content')->name('pgck.admin.page.builder.new');
    Route::post('/delete', 'PGCK\Admin\PageBuilderController@delete')->name('pgck.admin.page.builder.delete');
    Route::post('/update-order', 'PGCK\Admin\PageBuilderController@update_addon_order')->name('pgck.admin.page.builder.update.addon.order');
    Route::post('/get-admin-markup', 'PGCK\Admin\PageBuilderController@get_admin_panel_addon_markup')->name('pgck.admin.page.builder.get.addon.markup');
    /*==============================================
         IMAGE GALLERY ROUTES
    ==============================================*/
    Route::prefix('gallery-page')->group(function () {
        Route::get('/', 'PGCK\ImageGalleryPageController@index')->name('pgck.admin.gallery.all');
        Route::post('/new', 'PGCK\ImageGalleryPageController@store')->name('pgck.admin.gallery.new');
        Route::post('/update', 'PGCK\ImageGalleryPageController@update')->name('pgck.admin.gallery.update');
        Route::post('/delete/{id}', 'PGCK\ImageGalleryPageController@delete')->name('pgck.admin.gallery.delete');
        Route::post('/bulk-action', 'PGCK\ImageGalleryPageController@bulk_action')->name('pgck.admin.gallery.bulk.action');
        Route::get('/page-settings', 'PGCK\ImageGalleryPageController@page_settings')->name('pgck.admin.gallery.page.settings');
        Route::post('/page-settings', 'PGCK\ImageGalleryPageController@update_page_settings');
        /*------------------------
            IMAGE CATEGORY
        -------------------------*/
        Route::group(['prefix' => 'category'],function (){
            Route::get('/', 'PGCK\ImageGalleryPageController@category_index')->name('pgck.admin.gallery.category');
            Route::post('/new', 'PGCK\ImageGalleryPageController@category_store')->name('pgck.admin.gallery.category.new');
            Route::post('/update', 'PGCK\ImageGalleryPageController@category_update')->name('pgck.admin.gallery.category.update');
            Route::post('/delete/{id}', 'PGCK\ImageGalleryPageController@category_delete')->name('pgck.admin.gallery.category.delete');
            Route::post('/bulk-action', 'PGCK\ImageGalleryPageController@category_bulk_action')->name('pgck.admin.gallery.category.bulk.action');
        });
        Route::post('/category-by-slug', 'PGCK\ImageGalleryPageController@category_by_slug')->name('pgck.admin.gallery.category.by.lang');

    });
    /*==============================================
             WIDGETS MODULE ROUTES
    ==============================================*/
    Route::prefix('widgets')->group(function () {

        Route::get('/', 'PGCK\WidgetsController@index')->name('pgck.admin.widgets');
        Route::post('/create', 'PGCK\WidgetsController@new_widget')->name('pgck.admin.widgets.new');
        Route::post('/update', 'PGCK\WidgetsController@update_widget')->name('pgck.admin.widgets.update');
        Route::post('/markup', 'PGCK\WidgetsController@widget_markup')->name('pgck.admin.widgets.markup');
        Route::post('/update/order', 'PGCK\WidgetsController@update_order_widget')->name('pgck.admin.widgets.update.order');
        Route::post('/delete', 'PGCK\WidgetsController@delete_widget')->name('pgck.admin.widgets.delete');
    });
});


