<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpgcCounterup extends Model
{
    protected $table = 'upgc_counterups';
    protected $fillable = ['icon','number','title','extra_text','lang'];
}
