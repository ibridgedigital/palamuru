<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgckImageGallery extends Model
{
    protected $table = 'pgck_image_galleries';
    protected $fillable = ['image','title','lang','cat_id'];
}
