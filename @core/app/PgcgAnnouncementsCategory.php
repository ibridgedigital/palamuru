<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcgAnnouncementsCategory extends Model
{
    protected $table = 'pgcg_announcements_categories';
    protected $fillable = ['title','status','lang'];
}
