<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcgImageGalleryCategory extends Model
{
    protected $table = 'pgcg_image_gallery_categories';
    protected $fillable = ['title','status','lang'];
}
