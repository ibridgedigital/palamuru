<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpgcImageGallery extends Model
{
    protected $table = 'upgc_image_galleries';
    protected $fillable = ['image','title','lang','cat_id'];
}
