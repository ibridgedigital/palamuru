<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UcpImageGalleryCategory extends Model
{
    protected $table = 'ucp_image_gallery_categories';
    protected $fillable = ['title','status','lang'];
}
