<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UcpMediaUpload extends Model
{
    protected $table = "ucp_media_uploads";
    protected $fillable = ['title','alt','size','path','dimensions','type'];
}
