<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcwCounterup extends Model
{
    protected $table = 'pgcw_counterups';
    protected $fillable = ['icon','number','title','extra_text','lang'];
}
