<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcgAnnouncements extends Model
{
    protected $table = 'pgcg_announcements';
    protected $fillable = ['title','status','lang','date','image','time','cost','available_tickets','organizer','organizer_email','organizer_phone','slug','pdf_url','organizer_website','venue','venue_location','venue_phone','content','category_id','meta_description','meta_tags'];

    public function category(){
        return $this->hasOne('App\PgcgAnnouncementsCategory','id','category_id');
    }
}
