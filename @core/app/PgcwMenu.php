<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcwMenu extends Model
{
    protected $table = 'pgcw_menus';
    protected $fillable = ['title','lang','content','status'];
}
