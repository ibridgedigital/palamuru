<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpgcImageGalleryCategory extends Model
{
    protected $table = 'upgc_image_gallery_categories';
    protected $fillable = ['title','status','lang'];
}
