<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpgcWidgets extends Model
{
    protected $table = 'upgc_widgets';
    protected $fillable = ['widget_area','widget_order','widget_name','widget_content','widget_location','frontend_render_function','admin_render_function'];
}
