<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpgcAdminRole extends Model
{
    protected $table = 'upgc_admin_roles';
    protected $fillable = ['name' ,'permission'];

}
