<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'departments';
    protected $fillable = ['dept_name', 'hod_id', 'hod_name'];


    public function user(){
        return $this->belongsTo(User::class,'hod_id');
    }
}
