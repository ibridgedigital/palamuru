<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcgPage extends Model
{
    protected $table = 'pgcg_pages';
    protected $fillable = ['title','lang','slug','meta_description','meta_tags','content','status','visibility','page_builder_status'];
}
