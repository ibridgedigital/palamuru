<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UceAnnouncementsCategory extends Model
{
    protected $table = 'uce_announcements_categories';
    protected $fillable = ['title','status','lang'];
}
