<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcwTestimonial extends Model
{
    protected $table ='pgcw_testimonials';
    protected $fillable = ['name','image','lang','status','description','designation'];
}
