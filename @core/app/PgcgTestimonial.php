<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcgTestimonial extends Model
{
    protected $table ='pgcg_testimonials';
    protected $fillable = ['name','image','lang','status','description','designation'];
}
