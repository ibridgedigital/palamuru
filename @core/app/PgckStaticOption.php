<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgckStaticOption extends Model
{
    protected $table = 'pgck_static_options';
    protected $fillable = ['option_name','option_value'];
}
