<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcgMenu extends Model
{
    protected $table = 'pgcg_menus';
    protected $fillable = ['title','lang','content','status'];
}
