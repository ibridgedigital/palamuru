<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcwAnnouncementsCategory extends Model
{
    protected $table = 'pgcw_announcements_categories';
    protected $fillable = ['title','status','lang'];
}
