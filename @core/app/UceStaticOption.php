<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UceStaticOption extends Model
{
    protected $table = 'uce_static_options';
    protected $fillable = ['option_name','option_value'];
}
