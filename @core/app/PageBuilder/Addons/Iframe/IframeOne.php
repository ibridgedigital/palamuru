<?php

namespace App\PageBuilder\Addons\Iframe;
use App\Helpers\LanguageHelper;
use App\Helpers\SanitizeInput;
use App\PageBuilder\Fields\Image;
use App\PageBuilder\Fields\Number;
use App\PageBuilder\Fields\Select;
use App\PageBuilder\Fields\Slider;
use App\PageBuilder\Fields\Text;
use App\PageBuilder\Fields\Textarea;
use App\PageBuilder\Fields\Htmlcode;
use App\PageBuilder\PageBuilderBase;
use App\ProductCategory;

class IframeOne extends PageBuilderBase
{
    /**
     * @inheritDoc
     */
    public function preview_image()
    {
       return 'container/style-01.png';
    }

    /**
     * @inheritDoc
     */
    public function admin_render()
    {
        $output = $this->admin_form_before();
        $output .= $this->admin_form_start();
        $output .= $this->default_fields();
        $widget_saved_values = $this->get_settings();


        $output .= $this->admin_language_tab(); //have to start language tab from here on
        $output .= $this->admin_language_tab_start();

        $all_languages = LanguageHelper::all_languages();
        foreach ($all_languages as $key => $lang) {
            $output .= $this->admin_language_tab_content_start([
                'class' => $key == 0 ? 'tab-pane fade show active' : 'tab-pane fade',
                'id' => "nav-home-" . $lang->slug
            ]);
            $output .= $this->admin_language_tab_content_end();
        }

        $output .= $this->admin_language_tab_end(); //have to end language tab

        $output .= Text::get([
            'name' => 'src_url',
            'label' => __('Source Url'),
            'value' => $widget_saved_values['src_url'] ?? null,
            'max' => 200,
        ]);

        $output .= Slider::get([
            'name' => 'padding_top',
            'label' => __('Padding Top'),
            'value' => $widget_saved_values['padding_top'] ?? 110,
            'max' => 200,
        ]);
        $output .= Slider::get([
            'name' => 'padding_bottom',
            'label' => __('Padding Bottom'),
            'value' => $widget_saved_values['padding_bottom'] ?? 110,
            'max' => 200,
        ]);
        $output .= Text::get([
            'name' => 'class_name',
            'label' => __('Class Name'),
            'value' => $widget_saved_values['class_name'] ?? null,
            'max' => 200,
        ]);
        $output .= Text::get([
            'name' => 'id_name',
            'label' => __('Id Name'),
            'value' => $widget_saved_values['id_name'] ?? null,
            'max' => 200,
        ]);
        $output .= Text::get([
            'name' => 'parent_id_name',
            'label' => __('Parent Id Name'),
            'value' => $widget_saved_values['parent_id_name'] ?? null,
            'max' => 200,
        ]);

        // add padding option

        $output .= $this->admin_form_submit_button();
        $output .= $this->admin_form_end();
        $output .= $this->admin_form_after();

        return $output;
    }

    /**
     * @inheritDoc
     */
    public function frontend_render()
    {
        $settings = $this->get_settings();
        $current_lang = LanguageHelper::user_lang_slug();
        $padding_top = SanitizeInput::esc_html($settings['padding_top']);
        $padding_bottom = SanitizeInput::esc_html($settings['padding_bottom']);
        
        $class_name = SanitizeInput::esc_html($settings['class_name']);
        $id_name = SanitizeInput::esc_html($settings['id_name']);
        $parent_id_name = SanitizeInput::esc_html($settings['parent_id_name']);
        $src_url = SanitizeInput::esc_html($settings['src_url']);
    	$base_url = url('/').'/assets/uploads/media-uploader/pdf/';
        $url = $base_url.$src_url;
        
        return <<<HTML
<div class="{$class_name}" data-padding-top="{$padding_top}" data-padding-bottom="{$padding_bottom}" id="{$id_name}">
    <div class="text-center">
        <iframe src="{$url}" title="description" width="900" height="600"></iframe>
    </div>
</div>
HTML;

    }

    /**
     * @inheritDoc
     */
    public function addon_title()
    {
        return __('IFrame');
    }
}