<?php

namespace App\PageBuilder\Addons\Marquee;
use App\Helpers\LanguageHelper;
use App\Helpers\SanitizeInput;
use App\PageBuilder\Fields\Image;
use App\PageBuilder\Fields\Number;
use App\PageBuilder\Fields\Select;
use App\PageBuilder\Fields\Slider;
use App\PageBuilder\Fields\Text;
use App\PageBuilder\Fields\Textarea;
use App\PageBuilder\Fields\Htmlcode;
use App\PageBuilder\PageBuilderBase;
use App\ProductCategory;

class Marquee extends PageBuilderBase
{
    /**
     * @inheritDoc
     */
    public function preview_image()
    {
       return 'marquee/style-01.png';
    }

    /**
     * @inheritDoc
     */
    public function admin_render()
    {
        $output = $this->admin_form_before();
        $output .= $this->admin_form_start();
        $output .= $this->default_fields();
        $widget_saved_values = $this->get_settings();


        $output .= $this->admin_language_tab(); //have to start language tab from here on
        $output .= $this->admin_language_tab_start();

        $all_languages = LanguageHelper::all_languages();
        foreach ($all_languages as $key => $lang) {
            $output .= $this->admin_language_tab_content_start([
                'class' => $key == 0 ? 'tab-pane fade show active' : 'tab-pane fade',
                'id' => "nav-home-" . $lang->slug
            ]);
            $output .= $this->admin_language_tab_content_end();
        }

        $output .= $this->admin_language_tab_end(); //have to end language tab

        $output .= Text::get([
            'name' => 'content',
            'label' => __('Marquee Content'),
            'value' => $widget_saved_values['content'] ?? null,
            'max' => 200,
        ]);

        $output .= Select::get([
            'name' => 'direction',
            'label' => __('Order'),
            'options' => [
                'left' => __('Left'),
                'right' => __('Right'),
                'up' => __('Up'),
                'down' => __('Down'),
            ],
            'value' => $widget_saved_values['direction'] ?? null,
            'info' => __('set direction')
        ]);
        $output .= Text::get([
            'name' => 'speed',
            'label' => __('Speed'),
            'value' => $widget_saved_values['speed'] ?? null,
            'max' => 200,
        ]);
        $output .= Text::get([
            'name' => 'height',
            'label' => __('Height in PX'),
            'value' => $widget_saved_values['height'] ?? null,
            'max' => 200,
        ]);

        $output .= Text::get([
            'name' => 'width',
            'label' => __('Width in %'),
            'value' => $widget_saved_values['width'] ?? null,
            'max' => 200,
        ]);

        $output .= Slider::get([
            'name' => 'padding_top',
            'label' => __('Padding Top'),
            'value' => $widget_saved_values['padding_top'] ?? 110,
            'max' => 200,
        ]);
        $output .= Slider::get([
            'name' => 'padding_bottom',
            'label' => __('Padding Bottom'),
            'value' => $widget_saved_values['padding_bottom'] ?? 110,
            'max' => 200,
        ]);
        $output .= Text::get([
            'name' => 'class_name',
            'label' => __('Class Name'),
            'value' => $widget_saved_values['class_name'] ?? null,
            'max' => 200,
        ]);
        $output .= Text::get([
            'name' => 'id_name',
            'label' => __('Id Name'),
            'value' => $widget_saved_values['id_name'] ?? null,
            'max' => 200,
        ]);
        $output .= Text::get([
            'name' => 'parent_id_name',
            'label' => __('Parent Id Name'),
            'value' => $widget_saved_values['parent_id_name'] ?? null,
            'max' => 200,
        ]);

        // add padding option

        $output .= $this->admin_form_submit_button();
        $output .= $this->admin_form_end();
        $output .= $this->admin_form_after();

        return $output;
    }

    /**
     * @inheritDoc
     */
    public function frontend_render()
    {
        $settings = $this->get_settings();
        $current_lang = LanguageHelper::user_lang_slug();
        $padding_top = SanitizeInput::esc_html($settings['padding_top']);
        $padding_bottom = SanitizeInput::esc_html($settings['padding_bottom']);
        
        $class_name = SanitizeInput::esc_html($settings['class_name']);
        $id_name = SanitizeInput::esc_html($settings['id_name']);
        $parent_id_name = SanitizeInput::esc_html($settings['parent_id_name']);
        $direction = SanitizeInput::esc_html($settings['direction']);
        $speed = SanitizeInput::esc_html($settings['speed']);
        $content = SanitizeInput::esc_html($settings['content']);
        $height = SanitizeInput::esc_html($settings['height']);
        $width = SanitizeInput::esc_html($settings['width']);
        
        return <<<HTML
            <marquee behavior="scroll" direction="{$direction}" scrollamount="{$speed}" height="{$height}px" width="{$width}%">
                <div class="{$class_name}" data-padding-top="{$padding_top}" data-padding-bottom="{$padding_bottom}" id="{$id_name}">{$content}</div>
            </marquee>
        HTML;

    }

    /**
     * @inheritDoc
     */
    public function addon_title()
    {
        return __('Marquee');
    }
}