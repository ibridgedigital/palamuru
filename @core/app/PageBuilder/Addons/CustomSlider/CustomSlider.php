<?php

namespace App\PageBuilder\Addons\CustomSlider;
use App\Helpers\LanguageHelper;
use App\Helpers\SanitizeInput;
use App\PageBuilder\Fields\Image;
use App\PageBuilder\Fields\Number;
use App\PageBuilder\Fields\Select;
use App\PageBuilder\Fields\Slider;
use App\PageBuilder\Fields\Text;
/*use App\PageBuilder\Fields\Textarea;
use App\PageBuilder\Fields\Htmlcode;*/
use App\PageBuilder\PageBuilderBase;
use App\ProductCategory;

class CustomSlider extends PageBuilderBase
{
    /**
     * @inheritDoc
     */
    public function preview_image()
    {
       return 'container/style-01.png';
    }

    /**
     * @inheritDoc
     */
    public function admin_render()
    {
        $output = $this->admin_form_before();
        $output .= $this->admin_form_start();
        $output .= $this->default_fields();
        $widget_saved_values = $this->get_settings();


        $output .= $this->admin_language_tab(); //have to start language tab from here on
        $output .= $this->admin_language_tab_start();

        $all_languages = LanguageHelper::all_languages();
        foreach ($all_languages as $key => $lang) {
            $output .= $this->admin_language_tab_content_start([
                'class' => $key == 0 ? 'tab-pane fade show active' : 'tab-pane fade',
                'id' => "nav-home-" . $lang->slug
            ]);
            $output .= $this->admin_language_tab_content_end();
        }

        $output .= $this->admin_language_tab_end(); //have to end language tab

        $output .= Slider::get([
            'name' => 'padding_top',
            'label' => __('Padding Top'),
            'value' => $widget_saved_values['padding_top'] ?? 110,
            'max' => 200,
        ]);
        $output .= Slider::get([
            'name' => 'padding_bottom',
            'label' => __('Padding Bottom'),
            'value' => $widget_saved_values['padding_bottom'] ?? 110,
            'max' => 200,
        ]);
        $output .= Text::get([
            'name' => 'class_name',
            'label' => __('Class Name'),
            'value' => $widget_saved_values['class_name'] ?? null,
            'max' => 200,
        ]);
        $output .= Text::get([
            'name' => 'id_name',
            'label' => __('Id Name'),
            'value' => $widget_saved_values['id_name'] ?? null,
            'max' => 200,
        ]);
        $output .= Text::get([
            'name' => 'parent_id_name',
            'label' => __('Parent Id Name'),
            'value' => $widget_saved_values['parent_id_name'] ?? null,
            'max' => 200,
        ]);

        // add padding option

        $output .= $this->admin_form_submit_button();
        $output .= $this->admin_form_end();
        $output .= $this->admin_form_after();

        return $output;
    }

    /**
     * @inheritDoc
     */
    public function frontend_render()
    {
        $settings = $this->get_settings();
        $current_lang = LanguageHelper::user_lang_slug();
        $padding_top = SanitizeInput::esc_html($settings['padding_top']);
        $padding_bottom = SanitizeInput::esc_html($settings['padding_bottom']);
        
        $class_name = SanitizeInput::esc_html($settings['class_name']);
        $id_name = SanitizeInput::esc_html($settings['id_name']);
        $parent_id_name = SanitizeInput::esc_html($settings['parent_id_name']);
        
        return <<<HTML
        <div id="myCarousel" class="carousel slide" data-padding-top="{$padding_top}" data-padding-bottom="{$padding_bottom}">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for Slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <!-- Set the first background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('http://www.marchettidesign.net/demo/optimized-bootstrap/code.jpg');"></div>
                    <div class="carousel-caption bs-caption">
                        <div class="bs-left">
                            <h4 class="animated fadeInRight">Caption Animation</h4>
                            <h4 class="animated fadeInRight">Lorem ipsum dolor sit amet consectetur adipisicing elit</h4>
                        </div>
                        <div class="bs-right">
                            <img src="http://www.marchettidesign.net/demo/optimized-bootstrap/conference.jpg" alt="..." width="500">
                        </div>
                    </div>
                </div>
                <div class="item">
                    <!-- Set the second background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('http://www.marchettidesign.net/demo/optimized-bootstrap/conference.jpg');"></div>
                    <div class="carousel-caption">
                         <h2 class="animated fadeInDown">Caption Animation</h2>
                         <p class="animated fadeInUp">Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
                         <p class="animated fadeInUp"><a href="#" class="btn btn-transparent btn-rounded btn-large">Learn More</a></p>
                    </div>
                </div>
                <div class="item">
                    <!-- Set the third background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('http://www.marchettidesign.net/demo/optimized-bootstrap/campus.jpg');"></div>
                    <div class="carousel-caption">
                         <h2 class="animated fadeInRight">Caption Animation</h2>
                         <p class="animated fadeInRight">Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
                         <p class="animated fadeInRight"><a href="#" class="btn btn-transparent btn-rounded btn-large">Learn More</a></p>
                    </div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="icon-prev"></span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="icon-next"></span>
            </a>
        </div>
HTML;

    }

    /**
     * @inheritDoc
     */
    public function addon_title()
    {
        return __('Custom Slider');
    }
}