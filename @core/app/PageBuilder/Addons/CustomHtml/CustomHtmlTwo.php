<?php


namespace App\PageBuilder\Addons\CustomHtml;
use App\Helpers\LanguageHelper;
use App\Helpers\SanitizeInput;
use App\PageBuilder\Fields\Image;
use App\PageBuilder\Fields\Number;
use App\PageBuilder\Fields\Select;
use App\PageBuilder\Fields\Slider;
use App\PageBuilder\Fields\Text;
use App\PageBuilder\Fields\Textarea;
use App\PageBuilder\Fields\Htmlcode;
use App\PageBuilder\PageBuilderBase;
use App\ProductCategory;

class CustomHtmlTwo extends PageBuilderBase
{
    /**
     * @inheritDoc
     */
    public function preview_image()
    {
       return 'customhtml/style-01.png';
    }

    /**
     * @inheritDoc
     */
    public function admin_render()
    {
        $output = $this->admin_form_before();
        $output .= $this->admin_form_start();
        $output .= $this->default_fields();
        $widget_saved_values = $this->get_settings();


        $output .= $this->admin_language_tab(); //have to start language tab from here on
        $output .= $this->admin_language_tab_start();

        $all_languages = LanguageHelper::all_languages();
        foreach ($all_languages as $key => $lang) {
            $output .= $this->admin_language_tab_content_start([
                'class' => $key == 0 ? 'tab-pane fade show active' : 'tab-pane fade',
                'id' => "nav-home-" . $lang->slug
            ]);
            $output .= Text::get([
                'name' => 'section_subtitle_'.$lang->slug,
                'label' => __('Section Subtitle'),
                'value' => $widget_saved_values['section_subtitle_' . $lang->slug] ?? null,
            ]);
            $output .= Text::get([
                'name' => 'section_title_'.$lang->slug,
                'label' => __('Section Title'),
                'value' => $widget_saved_values['section_title_' . $lang->slug] ?? null,
            ]);
            $output .= $this->admin_language_tab_content_end();
        }

        $output .= $this->admin_language_tab_end(); //have to end language tab

        $output .= Htmlcode::get([
            'name' => 'html_code',
            'label' => __('Html Code'),
            'value' => $widget_saved_values['html_code'] ?? null,
            'info' => __('enter custom html code you want to show in frontend'),
        ]);
        
        $output .= Slider::get([
            'name' => 'padding_top',
            'label' => __('Padding Top'),
            'value' => $widget_saved_values['padding_top'] ?? 110,
            'max' => 200,
        ]);
        $output .= Slider::get([
            'name' => 'padding_bottom',
            'label' => __('Padding Bottom'),
            'value' => $widget_saved_values['padding_bottom'] ?? 110,
            'max' => 200,
        ]);
        $output .= Text::get([
            'name' => 'class_name',
            'label' => __('Class Name'),
            'value' => $widget_saved_values['class_name'] ?? null,
            'max' => 200,
        ]);
    	$output .= Text::get([
            'name' => 'id_name',
            'label' => __('Id Name'),
            'value' => $widget_saved_values['id_name'] ?? null,
            'max' => 200,
        ]);
        $output .= Text::get([
            'name' => 'parent_id_name',
            'label' => __('Parent Id Name'),
            'value' => $widget_saved_values['parent_id_name'] ?? null,
            'max' => 200,
        ]);

        // add padding option

        $output .= $this->admin_form_submit_button();
        $output .= $this->admin_form_end();
        $output .= $this->admin_form_after();

        return $output;
    }

    /**
     * @inheritDoc
     */
    public function frontend_render()
    {
        $settings = $this->get_settings();
        $current_lang = LanguageHelper::user_lang_slug();
        $section_title = SanitizeInput::esc_html($settings['section_title_'.$current_lang]);
        $section_subtitle = SanitizeInput::esc_html($settings['section_subtitle_'.$current_lang]);
        $padding_top = SanitizeInput::esc_html($settings['padding_top']);
        $padding_bottom = SanitizeInput::esc_html($settings['padding_bottom']);
        $html_code = $settings['html_code'];
        $class_name = SanitizeInput::esc_html($settings['class_name']);
    	$id_name = SanitizeInput::esc_html($settings['id_name']);
        $parent_id_name = SanitizeInput::esc_html($settings['parent_id_name']);
        
        $subtitle_markup = '';
        
        $section_title_markup = '';
        if (!empty($section_title)){
            $section_title_markup .= <<<HTML
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title desktop-center margin-bottom-60 grocery-home">
                         {$subtitle_markup}
                        <h1 class="subtitle">{$section_subtitle}</h1>
                    </div>
                </div>
            </div>
            HTML;
        }

        return <<<HTML
      

    <div class="container-fluid {$class_name}" id="{$id_name}" data-padding-top="{$padding_top}" data-padding-bottom="{$padding_bottom}">
        {$section_title_markup}
        <h2 class="title">{$section_title}</h2>
        <div class="row">
            <div class="col-lg-12">
                {$html_code}
            </div>
        </div>
    </div>
HTML;

    }

    /**
     * @inheritDoc
     */
    public function addon_title()
    {
        return __('Custom Html: 02');
    }
}