<?php


namespace App\PageBuilder\Addons\Profile;
use App\Appointment;
use App\AppointmentCategory;
use App\Helpers\LanguageHelper;
use App\Helpers\SanitizeInput;
use App\PageBuilder\Fields\ColorPicker;
use App\PageBuilder\Fields\IconPicker;
use App\PageBuilder\Fields\Image;
use App\PageBuilder\Fields\NiceSelect;
use App\PageBuilder\Fields\Notice;
use App\PageBuilder\Fields\Number;
use App\PageBuilder\Fields\Select;
use App\PageBuilder\Fields\Slider;
use App\PageBuilder\Fields\Switcher;
use App\PageBuilder\Fields\Text;
use App\PageBuilder\Fields\Textarea;
use App\PageBuilder\PageBuilderBase;
use App\ProductCategory;
use App\ProductRatings;
use App\Products;
use App\User;
use App\UserProfile;
use App\UserCategory;
use App\Department;

use Illuminate\Support\Str;

class ProfileSingleStyleOne extends PageBuilderBase
{


    public function enable() : bool
    {
        return (boolean) get_static_option('appointment_module_status');
    }

    /**
     * @inheritDoc
     */
    public function preview_image()
    {
       return 'appointment/slider-01.png';
    }

    /**
     * @inheritDoc
     */
    public function admin_render()
    {
        $output = $this->admin_form_before();
        $output .= $this->admin_form_start();
        $output .= $this->default_fields();
        $widget_saved_values = $this->get_settings();


        $output .= $this->admin_language_tab(); //have to start language tab from here on
        $output .= $this->admin_language_tab_start();

        $all_languages = LanguageHelper::all_languages();
        foreach ($all_languages as $key => $lang) {
            $output .= $this->admin_language_tab_content_start([
                'class' => $key == 0 ? 'tab-pane fade show active' : 'tab-pane fade',
                'id' => "nav-home-" . $lang->slug
            ]);
            
            $output .= $this->admin_language_tab_content_end();
        }

        $output .= $this->admin_language_tab_end(); //have to end language tab
        $users = User::get()->pluck('name', 'id')->toArray();
        
        $output .= Select::get([
            'name' => 'user',
            'label' => __('User'),
            'options' => $users,
            'value' => $widget_saved_values['user'] ?? null,
            'info' => __('select user ')
        ]);
        $output .= Slider::get([
            'name' => 'padding_top',
            'label' => __('Padding Top'),
            'value' => $widget_saved_values['padding_top'] ?? 110,
            'max' => 200,
        ]);
        $output .= Slider::get([
            'name' => 'padding_bottom',
            'label' => __('Padding Bottom'),
            'value' => $widget_saved_values['padding_bottom'] ?? 110,
            'max' => 200,
        ]);

        $output .= Text::get([
            'name' => 'class_name',
            'label' => __('Class Name'),
            'value' => $widget_saved_values['class_name'] ?? null,
            'max' => 200,
        ]);
        $output .= Text::get([
            'name' => 'id_name',
            'label' => __('Id Name'),
            'value' => $widget_saved_values['id_name'] ?? null,
            'max' => 200,
        ]);
        $output .= Text::get([
            'name' => 'parent_id_name',
            'label' => __('Parent Id Name'),
            'value' => $widget_saved_values['parent_id_name'] ?? null,
            'max' => 200,
        ]);

        // add padding option

        $output .= $this->admin_form_submit_button();
        $output .= $this->admin_form_end();
        $output .= $this->admin_form_after();

        return $output;
    }

    /**
     * @inheritDoc
     */
    public function frontend_render()
    {
        $settings = $this->get_settings();
        $user = SanitizeInput::esc_html($settings['user']);
        $padding_top = SanitizeInput::esc_html($settings['padding_top']);
        $padding_bottom = SanitizeInput::esc_html($settings['padding_bottom']);
        $class_name = SanitizeInput::esc_html($settings['class_name']);
        $id_name = SanitizeInput::esc_html($settings['id_name']);
        $parent_id_name = SanitizeInput::esc_html($settings['parent_id_name']);
        $item = UserProfile::where('user_id',$user)->first();
        $image_markup = render_image_markup_by_attachment_id($item->image);
        $facebook_markup = '';
        $twitter_markup = '';
        $linkedin_markup = '';
        $pinterest_markup = '';
        if(!empty($item->facebook))
        {
            $facebook_markup = <<<HTML
                <li>
                    <a class="facebook" href="{$item->facebook}"><i class="fab fa-facebook-f"></i></a>
               </li>
            HTML;
        }                 
        if(!empty($item->twitter))
        {
            $twitter_markup = <<<HTML
                <li>
                    <a class="facebook" href="{$item->twitter}"><i class="fab fa-twitter"></i></a>
               </li>
            HTML;
        }  
        if(!empty($item->linkedin))
        {
            $linkedin_markup = <<<HTML
                <li>
                    <a class="facebook" href="{$item->linkedin}"><i class="fab fa-linkedin-in"></i></a>
               </li>
            HTML;
        }  
        if(!empty($item->pinterest))
        {
            $pinterest_markup = <<<HTML
                <li>
                    <a class="facebook" href="{$item->pinterest}"><i class="fab fa-pinterest-p"></i></a>
               </li>
            HTML;
        }
        $experience_info_markup = '';  
        if(!empty($item->experience_info))
        {
            $experience_info = explode('|', $item->experience_info);
            $experience_info_markup .= <<<HTML
                <div class="education-info">
                <h3 class="title">Experience Info</h3>
                <ul class="circle-list">
                HTML;
            foreach($experience_info as $info)
            {
                $experience_info_markup .= <<<HTML
                    <li>$info</li>
                HTML;
            }
            $experience_info_markup .= <<<HTML
                    </ul>
                </div>
                HTML;
            
        }

  
        $additional_info_markup = '';  
        if(!empty($item->additional_info))
        {
            $additional_info = explode('|', $item->additional_info);
            $additional_info_markup .= <<<HTML
                <div class="additional-info">
                    <h3 class="title">Additional Info</h3>
                    <ul class="circle-list">
                HTML;
            foreach($additional_info as $info)
            {
                $additional_info_markup .= <<<HTML
                    <li>$info</li>
                HTML;
            }
            $additional_info_markup .= <<<HTML
                        </ul>
                    </div>
                HTML;
        }
        

        $specialized_info_markup = '';  
        if(!empty($item->specialized_info))
        {
            $specialized_info = explode('|', $item->specialized_info);
            $specialized_info_markup .= <<<HTML
                <div class="specialised-info">
                    <h3 class="title">Specialize Info</h3>
                    <ul class="circle-list">
                HTML;
            foreach($specialized_info as $info)
            {
                $specialized_info_markup .= <<<HTML
                    <li>$info</li>
                HTML;
            }
            $specialized_info_markup .= <<<HTML
                    </ul>
                </div>
                HTML;
        }

        $publications_markup = '';  
        if(!empty($item->publications))
        {
            $publications = explode('|', $item->publications);
            $publications_markup .= <<<HTML
                <div class="additional-info">
                    <h3 class="title">Publications</h3>
                    <ul class="circle-list">
                HTML;
            foreach($publications as $info)
            {
                $publications_markup .= <<<HTML
                    <li>$info</li>
                HTML;
            }
            $publications_markup .= <<<HTML
                    </ul>
                </div>
                HTML;
        }
    
    	$achievements_markup = '';
        if(!empty($item->achievements))
        {
            $achievements = explode('|', $item->achievements);
            $achievements_markup .= <<<HTML
                <div class="additional-info">
                    <h3 class="title">Achievements</h3>
                    <ul class="circle-list">
                HTML;
            foreach($achievements as $info)
            {
                $achievements_markup .= <<<HTML
                    <li>$info</li>
                HTML;
            }
            $achievements_markup .= <<<HTML
                    </ul>
                </div>
                HTML;
        }


        return <<<HTML
<section class="blog-details-content-area {$class_name}" id="{$id_name}" data-padding-top="{$padding_top}" data-padding-bottom="{$padding_bottom}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="appointment-details-item">
                       
                        <div class="top-part">
                           <div class="thumb">
                               {$image_markup}
                           </div>
                           <div class="content">
                               <span class="designation">{$item->designation}</span>
                               <h2 class="title">{$item->name}</h2>
                               <div class="short-description">{$item->short_description}</div>
                               
                               <div class="social-share-wrap">
                                   <ul class="social-share">
                                        {$facebook_markup}
                                        {$twitter_markup}
                                        {$linkedin_markup}
                                        {$pinterest_markup}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="bottom-part">
                            <x-error-msg/>
                            <x-flash-msg/>
                            <nav>
                                <div class="nav nav-tabs" role="tablist">
                                    <a class="nav-link "  data-toggle="tab" href="#nav-information" role="tab"  aria-selected="false">Information</a>
                                    <a class="nav-link active"  data-toggle="tab" href="#nav-booking" role="tab"  aria-selected="true">Publications</a>
                                    <a class="nav-link"  data-toggle="tab" href="#nav-feedback" role="tab"  aria-selected="false">Achievements</a>
                                </div>
                            </nav>

                            <div class="tab-content" >
                                <div class="tab-pane fade" id="nav-information" role="tabpanel" >
                                    <div class="information-area-wrap">
                                        <div class="description-wrap">
                                            {$item->description}
                                        </div>
                                        {$experience_info_markup}
                                        {$additional_info_markup}
                                        {$specialized_info_markup}
                                        
                                    </div>
                                </div>
                                <div class="tab-pane fade show active" id="nav-booking" role="tabpanel" >
                                    <div class="information-area-wrap">
                                        {$publications_markup}
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-feedback" role="tabpanel" >
                                    <div class="feedback-wrapper">
                                        {$achievements_markup}
                                    </div>

                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
HTML;

    }

    /**
     * @inheritDoc
     */
    public function addon_title()
    {
        return __('Profile single: 01');
    }
}