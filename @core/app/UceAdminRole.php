<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UceAdminRole extends Model
{
    protected $table = 'uce_admin_roles';
    protected $fillable = ['name' ,'permission'];

}
