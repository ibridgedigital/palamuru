<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UcpImageGallery extends Model
{
    protected $table = 'ucp_image_galleries';
    protected $fillable = ['image','title','lang','cat_id'];
}
