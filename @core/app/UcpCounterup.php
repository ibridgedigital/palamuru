<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UcpCounterup extends Model
{
    protected $table = 'ucp_counterups';
    protected $fillable = ['icon','number','title','extra_text','lang'];
}
