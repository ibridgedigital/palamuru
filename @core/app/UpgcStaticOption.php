<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpgcStaticOption extends Model
{
    protected $table = 'upgc_static_options';
    protected $fillable = ['option_name','option_value'];
}
