<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcwMediaUpload extends Model
{
    protected $table = "pgcw_media_uploads";
    protected $fillable = ['title','alt','size','path','dimensions','type'];
}
