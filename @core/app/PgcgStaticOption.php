<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcgStaticOption extends Model
{
    protected $table = 'pgcg_static_options';
    protected $fillable = ['option_name','option_value'];
}
