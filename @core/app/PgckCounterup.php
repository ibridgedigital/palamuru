<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgckCounterup extends Model
{
    protected $table = 'pgck_counterups';
    protected $fillable = ['icon','number','title','extra_text','lang'];
}
