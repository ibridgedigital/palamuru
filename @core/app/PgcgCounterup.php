<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcgCounterup extends Model
{
    protected $table = 'pgcg_counterups';
    protected $fillable = ['icon','number','title','extra_text','lang'];
}
