<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgckAdminRole extends Model
{
    protected $table = 'pgck_admin_roles';
    protected $fillable = ['name' ,'permission'];

}
