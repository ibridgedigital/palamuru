<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UcpAnnouncementsCategory extends Model
{
    protected $table = 'ucp_announcements_categories';
    protected $fillable = ['title','status','lang'];
}
