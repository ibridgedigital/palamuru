<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UcpPage extends Model
{
    protected $table = 'ucp_pages';
    protected $fillable = ['title','lang','slug','meta_description','meta_tags','content','status','visibility','page_builder_status'];
}
