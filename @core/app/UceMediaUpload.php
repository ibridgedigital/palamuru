<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UceMediaUpload extends Model
{
    protected $table = "uce_media_uploads";
    protected $fillable = ['title','alt','size','path','dimensions','type'];
}
