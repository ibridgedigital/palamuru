<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UceCounterup extends Model
{
    protected $table = 'uce_counterups';
    protected $fillable = ['icon','number','title','extra_text','lang'];
}
