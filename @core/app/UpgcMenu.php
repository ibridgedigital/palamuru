<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpgcMenu extends Model
{
    protected $table = 'upgc_menus';
    protected $fillable = ['title','lang','content','status'];
}
