<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnnouncementsCategory extends Model
{
    protected $table = 'announcements_categories';
    protected $fillable = ['title','status','lang'];
}
