<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcwStaticOption extends Model
{
    protected $table = 'pgcw_static_options';
    protected $fillable = ['option_name','option_value'];
}
