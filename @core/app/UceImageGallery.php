<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UceImageGallery extends Model
{
    protected $table = 'uce_image_galleries';
    protected $fillable = ['image','title','lang','cat_id'];
}
