<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UcpMenu extends Model
{
    protected $table = 'ucp_menus';
    protected $fillable = ['title','lang','content','status'];
}
