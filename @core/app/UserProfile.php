<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = 'user_profile';
    protected $fillable = ['user_id', 'dept_id','category_id', 'name', 'designation', 'description', 'location', 'short_description', 'additional_info', 'experience_info', 'specialized_info', 'meta_title', 'meta_description', 'meta_tags', 'image', 'facebook', 'twitter', 'linkedin', 'status', 'created_at'];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function category(){
        // return $this->hasOne(UserCategory::class,'id','category_id');
        return $this->belongsTo(UserCategory::class,'category_id');
    }
    public function department(){
        return $this->belongsTo(Department::class,'dept_id');
    }
}