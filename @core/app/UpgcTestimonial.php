<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpgcTestimonial extends Model
{
    protected $table ='upgc_testimonials';
    protected $fillable = ['name','image','lang','status','description','designation'];
}
