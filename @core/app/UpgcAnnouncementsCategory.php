<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpgcAnnouncementsCategory extends Model
{
    protected $table = 'upgc_announcements_categories';
    protected $fillable = ['title','status','lang'];
}
