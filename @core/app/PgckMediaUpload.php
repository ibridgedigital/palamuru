<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgckMediaUpload extends Model
{
    protected $table = "pgck_media_uploads";
    protected $fillable = ['title','alt','size','path','dimensions','type'];
}
