<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcgImageGallery extends Model
{
    protected $table = 'pgcg_image_galleries';
    protected $fillable = ['image','title','lang','cat_id'];
}
