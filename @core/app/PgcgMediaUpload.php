<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcgMediaUpload extends Model
{
    protected $table = "pgcg_media_uploads";
    protected $fillable = ['title','alt','size','path','dimensions','type'];
}
