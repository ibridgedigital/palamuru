<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcwImageGalleryCategory extends Model
{
    protected $table = 'pgcw_image_gallery_categories';
    protected $fillable = ['title','status','lang'];
}
