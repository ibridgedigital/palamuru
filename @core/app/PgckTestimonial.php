<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgckTestimonial extends Model
{
    protected $table ='pgck_testimonials';
    protected $fillable = ['name','image','lang','status','description','designation'];
}
