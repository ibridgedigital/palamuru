<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UceTestimonial extends Model
{
    protected $table ='uce_testimonials';
    protected $fillable = ['name','image','lang','status','description','designation'];
}
