<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcwWidgets extends Model
{
    protected $table = 'pgcw_widgets';
    protected $fillable = ['widget_area','widget_order','widget_name','widget_content','widget_location','frontend_render_function','admin_render_function'];
}
