<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcwAdminRole extends Model
{
    protected $table = 'pgcw_admin_roles';
    protected $fillable = ['name' ,'permission'];

}
