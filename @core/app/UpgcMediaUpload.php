<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpgcMediaUpload extends Model
{
    protected $table = "upgc_media_uploads";
    protected $fillable = ['title','alt','size','path','dimensions','type'];
}
