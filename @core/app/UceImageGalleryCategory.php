<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UceImageGalleryCategory extends Model
{
    protected $table = 'uce_image_gallery_categories';
    protected $fillable = ['title','status','lang'];
}
