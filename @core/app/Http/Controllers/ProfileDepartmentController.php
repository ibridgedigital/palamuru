<?php

namespace App\Http\Controllers;

use App\AppointmentCategoryLang;
use App\Helpers\IbridgeHelpers;
use App\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Department;

class ProfileDepartmentController extends Controller
{
    private $all_languages;
    public function __construct()
    {
        $this->all_languages = Language::all();
    }

    public function department_all(){
         $departments = Department::all();
         return view('frontend3.profile.profile-department')->with(['departments' => $departments,'all_languages' => $this->all_languages ]);
    }

    public function department_new(Request $request){
        $this->validate($request,[
            'title' => 'required',
        ]);
        DB::beginTransaction();

        try {
            $dept_id = Department::create(['dept_name' => $request->title])->id;
            
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            throwException($e->getMessage());
        }
        return back()->with(IbridgeHelpers::item_new());
    }

    public function department_delete(Request $request,$id){
        Department::findOrFail($id)->delete();
        return back()->with(IbridgeHelpers::item_delete());
    }

    public function bulk_action(Request $request){
        Department::whereIn('id',$request->ids)->delete();
        return response()->json(['status' => 'ok']);
    }

    public function department_update(Request $request){
        $this->validate($request,[
            'title' => 'required',
        ]);
        DB::beginTransaction();
        try {
            Department::find($request->id)->update(['dept_name' => $request->title]);
            
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            throwException($e->getMessage());
        }
        return back()->with(IbridgeHelpers::item_update());
    }
}
