<?php

namespace App\Http\Controllers\UCE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Admin;
use App\Brand;
use App\Donation;
use App\DonationLogs;
use App\EventAttendance;
use App\Events;
use App\Faq;
use App\Helpers\NexelitHelpers;
use App\Jobs;
use App\Language;
use App\Mail\ProductOrder;
use App\MediaUpload;
use App\Order;
use App\Products;
use App\Services;
use App\Blog;
use App\ContactInfoItem;
use App\Counterup;
use App\KeyFeatures;
use App\PricePlan;
use App\SocialIcons;
use App\TeamMember;
use App\Testimonial;
use App\Works;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Symfony\Component\Process\Process;

class TestController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:uce_admin');
    }
    
    public function testControlloer()
    {
        dd("tests");
    }
}
