<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\AppointmentBooking;
use App\AppointmentBookingTime;
use App\AppointmentCategory;
use App\AppointmentLang;
use App\Feedback;
use App\Helpers\NexelitHelpers;
use App\Language;
use App\Mail\FeedbackMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

use App\User;
use App\UserProfile;
use App\UserCategory;
use App\Department;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use App\Notifications\UserApprovedNotification;


class ProfileController extends Controller
{
    //appointment_all
    public $base_view_path = 'frontend3.profile.';
    // public $base_view_path = 'frontend3.appointment.';
    public $base_path = 'frontend3.pages.profile.';
    public $languages;

    public function __construct()
    {
        // $this->middleware(['auth:web','auth']);
        $this->middleware(['auth']);
        $this->languages = Language::all();
    }

    public function profile_all()
    {
        // dd(Auth::user()->id);
        $dept = UserProfile::where('user_id',Auth::user()->id)->first();
        $all_appointment = Appointment::with('lang')->get();
        if(Auth::user()->role == 3){

            $all_profile = UserProfile::where('dept_id',$dept->dept_id)->get();
        }
        elseif(Auth::user()->role == 1 || Auth::user()->role == 2){
            $all_profile = UserProfile::get();
        }
        else {

            $all_profile = UserProfile::where('user_id',Auth::user()->id)->get();
        }
        
        // dd($this->base_view_path);
        return view($this->base_view_path . 'profile-all')->with(['all_appointment' => $all_profile]);
    }

    public function profile_new()
    {
        $all_booking_time = AppointmentBookingTime::where('status' , 'publish')->get();
        $all_category = UserCategory::get();
        $departments = Department::get();
        return view($this->base_view_path . 'profile-new')->with([
            'all_languages' => $this->languages,
            'all_booking_time' => $all_booking_time,
            'all_category' => $all_category,
            'departments' => $departments,
        ]);
    }

    public function category_by_lang(Request $request){
        $data = AppointmentCategory::where(['status' => 'publish', 'lang' => $request->lang])->get();
        return response()->json($data);
    }

    public function user_profile_approve(Request $request)
    {
        $approved = $request->is_approved;
        if($request->is_approved == 0){
            User::where('id',$request->user_id)->update(['is_approved' => 1]);
            $user = User::where('id',$request->user_id)->first();
            Notification::send($user, new UserApprovedNotification());
        }
        else{
            $user = User::where('id',$request->user_id)->update(['is_approved' => 0]);
        }
        return response()->json($user);
    }
    public function profile_store(Request $request){
       
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ],[
            'password.required' => __('password is required'),
            'password.confirmed' => __('both password does not matched'),
        ]);

        DB::beginTransaction();
        try {
                $additional_info = implode('|', $request->additional_info);
                $experience_info = implode('|', $request->experience_info);
                $specialized_info = implode('|', $request->specialized_info);
                $publications = implode('|', $request->publications);
            	$user = User::create([
            	'name' => $request->name,
            	'email' => $request->email,
            	'country' => $request->country,
            	'image' => $request->image,
            	'username' => $request->username,
            	'password' => Hash::make($request->password),
        	]);

            $user_profile = UserProfile::create([
                'user_id' => $user->id,
                'category_id' => $request->category_id,
                'dept_id' => $request->department_id,
                'name' => $request->name,
                'slug' => $request->slug,
                'designation' => $request->designation,
                'description' => $request->description,
                'location' => $request->location,
                'short_description' => $request->short_description,
                'additional_info' => $additional_info,
                'experience_info' => $experience_info,
                'specialized_info' => $specialized_info,
                'publications' => $publications,
                'meta_title' => $request->meta_title,
                'meta_description' => $request->meta_description,
                'meta_tags' => $request->meta_tags,
                'image' => $request->image,
                'facebook' => $request->facebook,
                'twitter' => $request->twitter,
                'linkedin' => $request->linkedin,
                'pinterest' => $request->pinterest,
                'status' => $request->status
            ]);
            
             DB::commit();

        }catch (\Throwable $e){
            report($e);
        }

        return back()->with(NexelitHelpers::item_new());
    }

    public function profile_delete(Request $request,$id){
        $user_profile = UserProfile::where('id',$id)->first();
        UserProfile::findOrFail($id)->delete();
        User::findOrFail($user_profile->user_id)->delete();
        return back()->with(NexelitHelpers::item_delete());
    }
    public function single($slug,$id){
        $item = UserProfile::findOrFail( $id);
        return view('frontend3.user.dashboard.single')->with(['user_profile' =>$item]);
    }
    public function profile_edit($id){
    	// dd($id);
        $profile = UserProfile::findOrFail($id);
        $all_category = UserCategory::get();
        $departments = Department::get();
        // dd($profile);
        return view($this->base_view_path.'profile-edit')->with([
            'user_profile' => $profile,
            'all_category' => $all_category,
            'departments' => $departments,
        ]);
    }

    public function profile_update(Request $request){
        $this->validate($request,[
            'name' => 'required',
        ]);
        DB::beginTransaction();
		// dd($request);
        try {
                $additional_info = implode('|', $request->additional_info);
                $experience_info = implode('|', $request->experience_info);
                $specialized_info = implode('|', $request->specialized_info);
                $publications = implode('|', $request->publications);

// dd($request);

            $user_profile = UserProfile::where('id',$request->id)->update([
                'category_id' => $request->category_id,
                'dept_id' => $request->department_id,
                'name' => $request->name,
                'slug' => $request->slug,
                'designation' => $request->designation,
                'description' => $request->description,
                'location' => $request->location,
                'short_description' => $request->short_description,
                'additional_info' => $additional_info,
                'experience_info' => $experience_info,
                'specialized_info' => $specialized_info,
                'publications' => $publications,
                'meta_title' => $request->meta_title,
                'meta_description' => $request->meta_description,
                'meta_tags' => $request->meta_tags,
                'image' => $request->image,
                'facebook' => $request->facebook,
                'twitter' => $request->twitter,
                'linkedin' => $request->linkedin,
            ]);
            User::where('id',$request->user_id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'username' => $request->username,
                'image' => $request->image,
            ]);

            DB::commit();

        }catch (\Throwable $e){
            report($e);
        }

        return back()->with(NexelitHelpers::item_update());
    }

    public function appointment_clone(Request $request){

        $appointment = Appointment::findOrFail($request->id);


        DB::beginTransaction();
        try {
            DB::commit();
        }catch (\Throwable $e){
            report($e);
        }
        $appointment_id = Appointment::create([
            'categories_id' => $appointment->categories_id,
            'booking_time_ids' => implode(',',array_column($appointment->booking_time_ids,'id')),
            'status' => 'draft',
            'appointment_status' => $appointment->appointment_status,
            'image' => $appointment->image,
            'max_appointment' => $appointment->max_appointment,
            'price' => $appointment->price,
        ])->id;

        foreach ($this->languages as $lang){
            $appointment_lang = AppointmentLang::where(['appointment_id' => $request->id, 'lang' => $lang->slug])->first();
            AppointmentLang::create([
                'appointment_id' => $appointment_id,
                'description' => $appointment_lang->description ?? '',
                'additional_info' => $appointment_lang->additional_info ,
                'experience_info'=> $appointment_lang->experience_info,
                'specialized_info'=> $appointment_lang->specialized_info,
                'location' => $appointment_lang->location ?? '',
                'meta_description' => $appointment_lang->meta_description ?? '',
                'meta_title' => $appointment_lang->meta_title ?? '',
                'meta_tags' => $appointment_lang->meta_tags ?? '',
                'slug' => $appointment_lang->slug ?? '',
                'short_description' => $appointment_lang->short_description ?? '',
                'lang' => $lang->slug,
                'title' => $appointment_lang->title ?? '',
                'designation' => $appointment_lang->designation ?? '',
            ]);
        }

        return back()->with(NexelitHelpers::item_clone());
    }

    public function bulk_action(Request $request){
    	$userProfiles = UserProfile::whereIn('id',$request->ids)->get();
        UserProfile::whereIn('id',$request->ids)->delete();
    	foreach($userProfiles as $userProfile)
        {
        	User::where('id',$userProfile->user_id)->delete();
        }
    
        // AppointmentLang::whereIn('appointment_id',$request->ids)->delete();

        return response()->json(['status' => 'ok']);
    }

    public function form_builder(){
        return view($this->base_view_path.'appointment-booking-form');
    }
    public function form_builder_save(Request $request){
        $this->validate($request,[
            'field_name' => 'required|max:191',
            'field_placeholder' => 'required|max:191',
        ]);
        unset($request['_token']);
        $all_fields_name = [];
        $all_request_except_token = $request->all();
        foreach ($request->field_name as $fname){
            $all_fields_name[] = strtolower(Str::slug($fname));
        }
        $all_request_except_token['field_name'] = $all_fields_name;
        $json_encoded_data = json_encode($all_request_except_token);

        update_static_option('appointment_booking_page_form_fields',$json_encoded_data);
        return redirect()->back()->with(['msg' => __('Form Updated...'),'type' => 'success']);
    }

    public function settings(){
        return view($this->base_view_path.'appointment-settings')->with(['all_languages' => $this->languages]);
    }

    public function settings_save(Request $request){
        $this->validate($request,[
            'appointment_notify_mail' => 'required|email|max:191',
            'disable_guest_mode_for_appointment_module' => 'nullable|string',
        ]);
        foreach ($this->languages as $lang){
            $this->validate($request,[
                'appointment_single_'.$lang->slug.'_information_tab_title' => 'nullable|string',
                'appointment_single_'.$lang->slug.'_booking_tab_title' => 'nullable|string',
                'appointment_single_'.$lang->slug.'_feedback_tab_title' => 'nullable|string',
                'appointment_single_'.$lang->slug.'_appointment_booking_information_text' => 'nullable|string',
                'appointment_single_'.$lang->slug.'_appointment_booking_button_text' => 'nullable|string',
                'appointment_single_'.$lang->slug.'_appointment_booking_about_me_title' => 'nullable|string',
                'appointment_single_'.$lang->slug.'_appointment_booking_educational_info_title' => 'nullable|string',
                'appointment_single_'.$lang->slug.'_appointment_booking_additional_info_title' => 'nullable|string',
                'appointment_single_'.$lang->slug.'_appointment_booking_client_feedback_title' => 'nullable|string',
                'appointment_single_'.$lang->slug.'_appointment_booking_specialize_info_title' => 'nullable|string',
                'appointment_booking_'.$lang->slug.'_success_page_title' => 'nullable|string',
                'appointment_booking_'.$lang->slug.'_success_page_description' => 'nullable|string',
                'appointment_booking_'.$lang->slug.'_cancel_page_title' => 'nullable|string',
                'appointment_booking_'.$lang->slug.'_cancel_page_description' => 'nullable|string',
            ]);
            $fields_list = [
                'appointment_single_'.$lang->slug.'_information_tab_title',
                'appointment_single_'.$lang->slug.'_booking_tab_title',
                'appointment_single_'.$lang->slug.'_feedback_tab_title' ,
                'appointment_single_'.$lang->slug.'_appointment_booking_information_text',
                'appointment_single_'.$lang->slug.'_appointment_booking_button_text' ,
                'appointment_single_'.$lang->slug.'_appointment_booking_about_me_title' ,
                'appointment_single_'.$lang->slug.'_appointment_booking_educational_info_title',
                'appointment_single_'.$lang->slug.'_appointment_booking_additional_info_title',
                'appointment_single_'.$lang->slug.'_appointment_booking_specialize_info_title',
                'appointment_single_'.$lang->slug.'_appointment_booking_client_feedback_title',
                'appointment_booking_'.$lang->slug.'_success_page_title',
                'appointment_booking_'.$lang->slug.'_success_page_description',
                'appointment_booking_'.$lang->slug.'_cancel_page_title',
                'appointment_booking_'.$lang->slug.'_cancel_page_description',
                'appointment_page_'.$lang->slug.'_booking_button_text'
            ];

            foreach ($fields_list as $field){
                update_static_option($field,$request->$field);
            }
        }

        update_static_option('disable_guest_mode_for_appointment_module',$request->disable_guest_mode_for_appointment_module);
        update_static_option('appointment_notify_mail',$request->appointment_notify_mail);
        return back()->with([
            'msg' => __('Settings Updated'),
            'type' => 'success'
        ]);
    }
}
