<?php

namespace App\Http\Controllers\PGCG;

use App\Announcements;
use App\AnnouncementsCategory;
use App\PgcgAnnouncements;
use App\PgcgAnnouncementsCategory;
use App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AnnouncementsCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pgcg_admin');
    }

    public function all_announcements_category(){
        $all_category = PgcgAnnouncementsCategory::all()->groupBy('lang');
        $all_languages = Language::all();
        return view('pgcg.backend.announcements.all-announcements-category')->with(['all_category' => $all_category,'all_languages' => $all_languages] );
    }

    public function store_announcements_category(Request $request){
        $this->validate($request,[
            'title' => 'required|string|max:191|unique:events_categories',
            'lang' => 'required|string|max:191',
            'status' => 'required|string|max:191'
        ]);
        PgcgAnnouncementsCategory::create($request->all());

        return redirect()->back()->with([
            'msg' => __('New Category Added...'),
            'type' => 'success'
        ]);
    }

    public function update_announcements_category(Request $request){
        $this->validate($request,[
            'title' => 'required|string|max:191',
            'lang' => 'required|string|max:191',
            'status' => 'required|string|max:191'
        ]);

        PgcgAnnouncementsCategory::find($request->id)->update([
            'title' => $request->title,
            'status' => $request->status,
            'lang' => $request->lang,
        ]);

        return redirect()->back()->with([
            'msg' => __( 'Category Update Success...'),
            'type' => 'success'
        ]);
    }

    public function delete_announcements_category(Request $request,$id){
        if (PgcgAnnouncements::where('category_id',$id)->first()){
            return redirect()->back()->with([
                'msg' => __('You Can Not Delete This Category, It Already Associated With An Announcement...'),
                'type' => 'danger'
            ]);
        }
        PgcgAnnouncementsCategory::find($id)->delete();
        return redirect()->back()->with([
            'msg' => __('Category Delete Success...'),
            'type' => 'danger'
        ]);
    }


    public function bulk_action(Request $request){
        if (PgcgAnnouncements::whereIn('category_id',$request->ids)->count() > 0){
            return response()->json(['status' => 'no']);
        }
        PgcgAnnouncementsCategory::whereIn('id',$request->ids)->delete();
        return response()->json(['status' => 'ok']);
    }
}
