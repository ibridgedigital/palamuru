<?php

namespace App\Http\Controllers\UCP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Admin;
use App\UcpAdmin;
use App\Brand;
use App\Donation;
use App\DonationLogs;
use App\EventAttendance;
use App\Events;
use App\Faq;
use App\Helpers\NexelitHelpers;
use App\Jobs;
use App\Language;
use App\Mail\ProductOrder;
use App\MediaUpload;
use App\Order;
use App\Products;
use App\Services;
use App\Blog;
use App\ContactInfoItem;
use App\Counterup;
use App\KeyFeatures;
use App\PricePlan;
use App\SocialIcons;
use App\TeamMember;
use App\Testimonial;
use App\Works;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Symfony\Component\Process\Process;

class AdminDashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:ucp_admin');
    }
    public function adminIndex()
    {
        // dd("test admin dashboard controller");
        // dd(Auth::user()->name);
        $default_lang = get_default_language();

        $all_blogs = Blog::where('lang', $default_lang)->count();
        $total_admin = UcpAdmin::count();
        $total_testimonial = Testimonial::where('lang', $default_lang)->count();
        $total_team_member = TeamMember::where('lang', $default_lang)->count();
        $total_counterup = Counterup::where('lang', $default_lang)->count();
        $total_price_plan = PricePlan::where('lang', $default_lang)->count();
        $total_services = Services::where('lang', $default_lang)->count();
        $total_key_features = KeyFeatures::where('lang', $default_lang)->count();
        $total_works = Works::where('lang', $default_lang)->count();
        $total_jobs = Jobs::where('lang', $default_lang)->count();
        $total_events = Events::where('lang', $default_lang)->count();
        $total_donations = Donation::where('lang', $default_lang)->count();
        $total_products = Products::where('lang', $default_lang)->count();
        $total_Faq = Faq::where('lang', $default_lang)->count();
        $total_brand = Brand::all()->count();
        $total_product_order = \App\ProductOrder::all()->count();
        $total_donated_log = DonationLogs::where('status','complete')->count();
        $total_event_attendance = EventAttendance::where('status','complete')->count();

        //recent 5 order of product order
        $product_recent_order = \App\ProductOrder::orderBy('id','desc')->take(5)->get();
        $package_recent_order = Order::orderBy('id','desc')->take(5)->get();
        $event_attendance_recent_order = EventAttendance::orderBy('id','desc')->take(5)->get();
        $donation_recent = DonationLogs::orderBy('id','desc')->take(5)->get();
// dd("test new controller");
        // $this->update_script_info();
// dd("test new controller");
        return view('ucp.backend.admin-home')->with([
            'blog_count' => $all_blogs,
            'total_admin' => $total_admin,
            'total_price_plan' => $total_price_plan,
            'total_works' => $total_works,
            'total_services' => $total_services,
            'total_jobs' => $total_jobs,
            'total_events' => $total_events,
            'total_donations' => $total_donations,
            'total_products' => $total_products,
            'total_donated_log' => $total_donated_log,
            'total_product_order' => $total_product_order,
            'total_event_attendance' => $total_event_attendance,
            'product_recent_order' => $product_recent_order,
            'package_recent_order' => $package_recent_order,
            'event_attendance_recent_order' => $event_attendance_recent_order,
            'donation_recent' => $donation_recent,
        ]);
    }

    public function adminLogout()
    {
        Auth::logout();
        return redirect()->route('ucp.admin.login')->with(['msg' => __('You Logged Out !!'), 'type' => 'danger']);
    }
    
    public function index()
    {
        dd('test');
        return view('frontend.frontend-home')->with($blade_data);

    }
    public function admin_set_static_option(Request $request)
    {
        $this->validate($request,[
           'static_option' => 'required|string',
           'static_option_value' => 'required|string',
        ]);
        set_static_option($request->static_option,$request->static_option_value);
        return 'ok';
    }
    public function admin_profile()
    {
        return view('ucp.auth.admin.edit-profile');
    }
    public function admin_profile_update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'email' => 'required|email|max:191',
            'username' => 'required|string|max:191',
            'image' => 'nullable|string|max:191'
        ]);
        UcpAdmin::find(Auth::user()->id)->update(['name' => $request->name, 'email' => $request->email, 'username' => str_replace(' ', '_', $request->username), 'image' => $request->image]);

        return redirect()->back()->with(['msg' => __('Profile Update Success'), 'type' => 'success']);
    }
}
