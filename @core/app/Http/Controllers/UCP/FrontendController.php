<?php

namespace App\Http\Controllers\UCP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Admin;
use App\Appointment;
use App\ContactInfoItem;
use App\Course;
use App\CoursesCategory;
use App\Donation;
use App\DonationLogs;
use App\EventAttendance;
use App\EventPaymentLogs;
use App\Events;
use App\EventsCategory;
use App\Faq;
use App\Feedback;
use App\Helpers\LanguageHelper;
use App\Helpers\NexelitHelpers;
use App\ImageGallery;
use App\ImageGalleryCategory;
use App\JobApplicant;
use App\Jobs;
use App\JobsCategory;
use App\Knowledgebase;
use App\KnowledgebaseTopic;
use App\Language;
use App\Mail\AdminResetEmail;
use App\Mail\CallBack;
use App\Mail\ContactMessage;
use App\Mail\PlaceOrder;
use App\Mail\RequestQuote;
use App\Menu;
use App\Newsletter;
use App\Order;
use App\Page;
use App\PaymentLogs;
use App\ProductCategory;
use App\ProductOrder;
use App\ProductRatings;
use App\Products;
use App\ProductShipping;
use App\Quote;
use App\ServiceCategory;
use App\Services;
use App\Blog;
use App\BlogCategory;
use App\Brand;
use App\HeaderSlider;
use App\KeyFeatures;
use App\PricePlan;
use App\StaticOption;
use App\TeamMember;
use App\User;
use App\Counterup;
use App\Testimonial;
use App\VideoGallery;
use App\Works;
use App\WorksCategory;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Svg\Tag\Image;
use Symfony\Component\Process\Process;
use App\Helpers\HomePageStaticSettings;
use App\PageBuilder;
use App\Announcements;
use App\AnnouncementsCategory;

use App\UceAnnouncements;
use App\UceAnnouncementsCategory;
use App\UcePage;

use App\UcpAnnouncements;
use App\UcpAnnouncementsCategory;
use App\UcpPage;
use App\UcpPageBuilder;
use App\UcpStaticOption;

class FrontendController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:uce_admin');
    }

    public function index()
    {
        // dd("trst");
        $home_page_variant = get_home_variant();
        // $home_page_variant = get_home_variant();
        $lang = LanguageHelper::user_lang_slug();
        //make a function to call all static option by home page
        $static_field_data = UcpStaticOption::whereIn('option_name',HomePageStaticSettings::get_home_field(get_static_option('home_page_variant')))->get()->mapWithKeys(function ($item) {
            return [$item->option_name => $item->option_value];
        })->toArray();
        // dd("test 7856");
    /*Code edited by prasanth*/
        $all_id_names = UcpPageBuilder::where(['addon_location' => 'homepage'])->orderBy('addon_order', 'ASC')->get();
        $temp = [];
        foreach($all_id_names as $id_name)
        {
            array_push($temp, unserialize($id_name->addon_settings));
        }
        $final = [];
        $i = 0;
        foreach($temp as $item){
            if(!empty($item['parent_id_name']))
            {
                $final[$i]['parent'] = $item['parent_id_name'];
                $final[$i]['child'] = $item['id_name'];
                $i++;
            }
        }
        /*End by Prasanth*/
        // dd(get_static_option('home_page_page_builder_status'));
        if (!empty(get_static_option('home_page_page_builder_status'))){
            // dd("testss");
            return view('ucp.frontend.frontend-home')->with([ 'static_field_data' => $static_field_data,'final' => $final]);
        }

        $all_header_slider = HeaderSlider::where('lang', $lang)->get();
        $all_counterup = Counterup::where('lang', $lang)->get();
        $all_key_features = KeyFeatures::where('lang', $lang)->get();
        $all_service = Services::where(['lang' => $lang, 'status' => 'publish'])->orderBy('sr_order', 'ASC')->take(get_static_option('home_page_01_service_area_items'))->get();
        $all_testimonial = Testimonial::where(['lang' => $lang, 'status' => 'publish'])->orderBy('id', 'desc')->get();
        $all_price_plan = PricePlan::where(['lang' => $lang, 'status' => 'publish'])->orderBy('id', 'desc')->take(get_static_option('home_page_01_price_plan_section_items'))->get();
        $all_team_members = TeamMember::where('lang', $lang)->orderBy('id', 'desc')->take(get_static_option('home_page_01_team_member_items'))->get();
        $all_brand_logo = Brand::all();
        $all_work = Works::where(['lang' => $lang, 'status' => 'publish'])->orderBy('id', 'desc')->take(get_static_option('home_page_01_case_study_items'))->get();
        $all_blog = Blog::where(['lang' => $lang, 'status' => 'publish'])->orderBy('id', 'desc')->take(6)->get();
        $all_contact_info = ContactInfoItem::where(['lang' => $lang])->orderBy('id', 'desc')->get();
        $all_service_category = ServiceCategory::where(['lang' => $lang, 'status' => 'publish'])->orderBy('id', 'desc')->take(get_static_option('home_page_01_service_area_items'))->get();
        $all_contain_cat = $all_work->map(function ($index) { return $index->categories_id; });
        $works_cat_ids = [];
        foreach($all_contain_cat as $k=>$v){
            foreach($v as $key=>$value){
                if(!in_array($value, $works_cat_ids)){
                    $works_cat_ids[]=$value;
                }
            }
        }

        $all_work_category = WorksCategory::find($works_cat_ids);


        $blade_data = [
            'static_field_data' => $static_field_data,
            'all_header_slider' => $all_header_slider,
            'all_counterup' => $all_counterup,
            'all_key_features' => $all_key_features,
            'all_service' => $all_service,
            'all_testimonial' => $all_testimonial,
            'all_blog' => $all_blog,
            'all_price_plan' => $all_price_plan,
            'all_team_members' => $all_team_members,
            'all_brand_logo' => $all_brand_logo,
            'all_work_category' => $all_work_category,
            'all_work' => $all_work,
            'all_service_category' => $all_service_category,
            'all_contact_info' => $all_contact_info,
        ];

        if (in_array($home_page_variant,['10','12','16']) ){
            //appointment module for home page 10,12,16
            $appointment_query = Appointment::query();
            $appointment_query->with('lang_front');
            $feature_product_list = get_static_option('home_page_' . get_static_option('home_page_variant') . '_appointment_section_category') ?? '';
            $feature_product_list = unserialize($feature_product_list, ['class' => false]);
            if (is_array($feature_product_list) && count($feature_product_list) > 0) {
                $appointment_query->whereIn('categories_id', $feature_product_list);
            }
            $appointments = $appointment_query->get();
            $blade_data['appointments'] = $appointments;

        }

        if ($home_page_variant == '13'){
            //popular donation cause
            $popular_cause_query = Donation::query();
            $popular_cause_list = get_static_option('home_page_13_' . $lang . '_popular_cause_popular_cause_list') ??  serialize([]);
            $popular_cause_list = unserialize($popular_cause_list, ['class' => false]);
            $popular_cause_items = get_static_option('home_page_13_popular_cause_popular_cause_items') ?? '';
            $popular_cause_order = get_static_option('home_page_13_popular_cause_popular_cause_order') ?? 'asc';
            $popular_cause_orderby = get_static_option('home_page_13_popular_cause_popular_cause_orderby') ?? 'id';


            if (count($popular_cause_list) > 0) {
                $popular_cause_query->whereIn('id', $popular_cause_list);
            }
            $popular_causes = $popular_cause_query->where('lang', $lang)->orderBy($popular_cause_orderby, $popular_cause_order)->take($popular_cause_items)->get();
            $blade_data['popular_causes'] = $popular_causes;
        }

        if (in_array($home_page_variant,['13','15','17','18'])){
            $all_events = Events::where(['lang' => $lang, 'status' => 'publish'])->orderBy('id', 'DESC')->take(get_static_option('home_page_01_event_area_items'))->get();
            $latest_products = Products::where(['lang' => $lang, 'status' => 'publish'])->orderBy('id', 'DESC')->take(get_static_option('home_page_products_area_items'))->get();
            $blade_data['all_events'] = $all_events;
            $blade_data['latest_products'] = $latest_products;
        }
        if (in_array($home_page_variant,['15','18'])){
            $product_query = Products::query();
            $feature_product_list = get_static_option('home_page_15_' . $lang . '_featured_product_area_items') ??  serialize([]);
            $feature_product_list = unserialize($feature_product_list, ['class' => false]);
            if (count($feature_product_list) > 0) {
                $product_query->whereIn('id', $feature_product_list);
            }
            $featured_products = $product_query->where('lang', $lang)->get();

            //best selling product
            $top_selling_products = Products::where(['lang' => $lang, 'status' => 'publish'])->orderBy('sales', 'desc')->take(get_static_option('home_page_15_top_selling_product_area_items'))->get();
            $blade_data['featured_products'] = $featured_products;
            $blade_data['top_selling_products'] = $top_selling_products;
        }


        if (in_array($home_page_variant,['17'])){
            //courses category
            $all_courses_category = CoursesCategory::where(['status' => 'publish'])->get();
            //
            $featured_courses_ids = unserialize(get_static_option('featured_courses_ids'), ['class' => false]); //fetch featured courses from db by admin selected ids;
            $featured_courses = Course::with('lang_front')->whereIn('id', $featured_courses_ids)->get(); //fetch featured courses from db by admin selected ids;
            //
            $latest_courses = Course::with('lang_front')->get()->take(get_static_option('course_home_page_all_course_area_items')); //get all latest course items, limit by admin given limit;

            $blade_data['latest_courses'] = $latest_courses;
            $blade_data['featured_courses'] = $featured_courses;
            $blade_data['all_courses_category'] = $all_courses_category;
        }

        if (in_array($home_page_variant,['18'])){
            //product categories
            $product_categories = ProductCategory::where(['lang' => $lang, 'status' => 'publish'])->get();
            $blade_data['product_categories'] = $product_categories;
        }

        return view('ucp.frontend.frontend-home')->with($blade_data);

    }
    
    public function testControlloer()
    {
        $home_page_variant = get_home_variant();
        $lang = LanguageHelper::user_lang_slug();

        //make a function to call all static option by home page
        $static_field_data = StaticOption::whereIn('option_name',HomePageStaticSettings::get_home_field(get_static_option('home_page_variant')))->get()->mapWithKeys(function ($item) {
            return [$item->option_name => $item->option_value];
        })->toArray();
        $all_id_names = PageBuilder::where(['addon_location' => 'homepage'])->orderBy('addon_order', 'ASC')->get();
        $temp = [];
        foreach($all_id_names as $id_name)
        {
            array_push($temp, unserialize($id_name->addon_settings));
        }
        $final = [];
        $i = 0;
        foreach($temp as $item){
            if(!empty($item['parent_id_name']))
            {
                $final[$i]['parent'] = $item['parent_id_name'];
                $final[$i]['child'] = $item['id_name'];
                $i++;
            }
        }

        if (!empty(get_static_option('home_page_page_builder_status'))){
            // dd($final);
            return view('ucp.frontend.frontend-home')->with([ 'static_field_data' => $static_field_data,'final' => $final]);
        }


        $all_header_slider = HeaderSlider::where('lang', $lang)->get();
        $all_counterup = Counterup::where('lang', $lang)->get();
        $all_key_features = KeyFeatures::where('lang', $lang)->get();
        $all_service = Services::where(['lang' => $lang, 'status' => 'publish'])->orderBy('sr_order', 'ASC')->take(get_static_option('home_page_01_service_area_items'))->get();
        $all_testimonial = Testimonial::where(['lang' => $lang, 'status' => 'publish'])->orderBy('id', 'desc')->get();
        $all_price_plan = PricePlan::where(['lang' => $lang, 'status' => 'publish'])->orderBy('id', 'desc')->take(get_static_option('home_page_01_price_plan_section_items'))->get();
        $all_team_members = TeamMember::where('lang', $lang)->orderBy('id', 'desc')->take(get_static_option('home_page_01_team_member_items'))->get();
        $all_brand_logo = Brand::all();
        $all_work = Works::where(['lang' => $lang, 'status' => 'publish'])->orderBy('id', 'desc')->take(get_static_option('home_page_01_case_study_items'))->get();
        $all_blog = Blog::where(['lang' => $lang, 'status' => 'publish'])->orderBy('id', 'desc')->take(6)->get();
        $all_contact_info = ContactInfoItem::where(['lang' => $lang])->orderBy('id', 'desc')->get();
        $all_service_category = ServiceCategory::where(['lang' => $lang, 'status' => 'publish'])->orderBy('id', 'desc')->take(get_static_option('home_page_01_service_area_items'))->get();
        $all_contain_cat = $all_work->map(function ($index) { return $index->categories_id; });
        $works_cat_ids = [];
        foreach($all_contain_cat as $k=>$v){
            foreach($v as $key=>$value){
                if(!in_array($value, $works_cat_ids)){
                    $works_cat_ids[]=$value;
                }
            }
        }
        $all_work_category = WorksCategory::find($works_cat_ids);


        $blade_data = [
            'static_field_data' => $static_field_data,
            'all_header_slider' => $all_header_slider,
            'all_counterup' => $all_counterup,
            'all_key_features' => $all_key_features,
            'all_service' => $all_service,
            'all_testimonial' => $all_testimonial,
            'all_blog' => $all_blog,
            'all_price_plan' => $all_price_plan,
            'all_team_members' => $all_team_members,
            'all_brand_logo' => $all_brand_logo,
            'all_work_category' => $all_work_category,
            'all_work' => $all_work,
            'all_service_category' => $all_service_category,
            'all_contact_info' => $all_contact_info,
        ];

        if (in_array($home_page_variant,['10','12','16']) ){
            //appointment module for home page 10,12,16
            $appointment_query = Appointment::query();
            $appointment_query->with('lang_front');
            $feature_product_list = get_static_option('home_page_' . get_static_option('home_page_variant') . '_appointment_section_category') ?? '';
            $feature_product_list = unserialize($feature_product_list, ['class' => false]);
            if (is_array($feature_product_list) && count($feature_product_list) > 0) {
                $appointment_query->whereIn('categories_id', $feature_product_list);
            }
            $appointments = $appointment_query->get();
            $blade_data['appointments'] = $appointments;

        }

        if ($home_page_variant == '13'){
            //popular donation cause
            $popular_cause_query = Donation::query();
            $popular_cause_list = get_static_option('home_page_13_' . $lang . '_popular_cause_popular_cause_list') ??  serialize([]);
            $popular_cause_list = unserialize($popular_cause_list, ['class' => false]);
            $popular_cause_items = get_static_option('home_page_13_popular_cause_popular_cause_items') ?? '';
            $popular_cause_order = get_static_option('home_page_13_popular_cause_popular_cause_order') ?? 'asc';
            $popular_cause_orderby = get_static_option('home_page_13_popular_cause_popular_cause_orderby') ?? 'id';


            if (count($popular_cause_list) > 0) {
                $popular_cause_query->whereIn('id', $popular_cause_list);
            }
            $popular_causes = $popular_cause_query->where('lang', $lang)->orderBy($popular_cause_orderby, $popular_cause_order)->take($popular_cause_items)->get();
            $blade_data['popular_causes'] = $popular_causes;
        }

        if (in_array($home_page_variant,['13','15','17','18'])){
            $all_events = Events::where(['lang' => $lang, 'status' => 'publish'])->orderBy('id', 'DESC')->take(get_static_option('home_page_01_event_area_items'))->get();
            $latest_products = Products::where(['lang' => $lang, 'status' => 'publish'])->orderBy('id', 'DESC')->take(get_static_option('home_page_products_area_items'))->get();
            $blade_data['all_events'] = $all_events;
            $blade_data['latest_products'] = $latest_products;
        }
        if (in_array($home_page_variant,['15','18'])){
            $product_query = Products::query();
            $feature_product_list = get_static_option('home_page_15_' . $lang . '_featured_product_area_items') ??  serialize([]);
            $feature_product_list = unserialize($feature_product_list, ['class' => false]);
            if (count($feature_product_list) > 0) {
                $product_query->whereIn('id', $feature_product_list);
            }
            $featured_products = $product_query->where('lang', $lang)->get();

            //best selling product
            $top_selling_products = Products::where(['lang' => $lang, 'status' => 'publish'])->orderBy('sales', 'desc')->take(get_static_option('home_page_15_top_selling_product_area_items'))->get();
            $blade_data['featured_products'] = $featured_products;
            $blade_data['top_selling_products'] = $top_selling_products;
        }


        if (in_array($home_page_variant,['17'])){
            //courses category
            $all_courses_category = CoursesCategory::where(['status' => 'publish'])->get();
            //
            $featured_courses_ids = unserialize(get_static_option('featured_courses_ids'), ['class' => false]); //fetch featured courses from db by admin selected ids;
            $featured_courses = Course::with('lang_front')->whereIn('id', $featured_courses_ids)->get(); //fetch featured courses from db by admin selected ids;
            //
            $latest_courses = Course::with('lang_front')->get()->take(get_static_option('course_home_page_all_course_area_items')); //get all latest course items, limit by admin given limit;

            $blade_data['latest_courses'] = $latest_courses;
            $blade_data['featured_courses'] = $featured_courses;
            $blade_data['all_courses_category'] = $all_courses_category;
        }

        if (in_array($home_page_variant,['18'])){
            //product categories
            $product_categories = ProductCategory::where(['lang' => $lang, 'status' => 'publish'])->get();
            $blade_data['product_categories'] = $product_categories;
        }

        return view('ucp.frontend.frontend-home')->with($blade_data);
    }

    public function dynamic_single_page($slug)
    {
        // dd("test");
        $page_post = UcpPage::where('slug', $slug)->first();

        /*Code start By Prasanth*/
        $all_id_names = UcpPageBuilder::where(['addon_location' => 'dynamic_page'])->where(['addon_page_id' => $page_post->id])->orderBy('addon_order', 'ASC')->get();
        // dd($all_id_names);
        $temp = [];
        foreach($all_id_names as $id_name)
        {
            array_push($temp, unserialize($id_name->addon_settings));
        }
        $final = [];
        $i = 0;
        foreach($temp as $item){
            if(!empty($item['parent_id_name']))
            {
                $final[$i]['parent'] = $item['parent_id_name'];
                $final[$i]['child'] = $item['id_name'];
                $i++;
            }
        }
        /*End by Prasanth*/
        if (empty($page_post)){
            abort(404);
        }
        // dd($slug);
        return view('ucp.frontend.pages.dynamic-single')->with([
            'page_post' => $page_post,
            'final' => $final,
            'slug' => $slug
        ]);
    }
    public function announcements_single($slug)
    {
        $default_lang = Language::where('default', 1)->first();
        $lang = !empty(session()->get('lang')) ? session()->get('lang') : $default_lang->slug;

        $announcement = UcpAnnouncements::where('slug', $slug)->first();
        if (empty($announcement)) {
            return redirect_404_page();
        }
        $all_announcements_category = UcpAnnouncementsCategory::where(['status' => 'publish', 'lang' => $lang])->get();
        $all_announcements = UcpAnnouncements::orderBy('id', 'desc')->get();
        // dd($all_announcements);
        return view('ucp.frontend.pages.announcements.announcement-single')->with([
            'announcement' => $announcement,
            'all_announcement_category' => $all_announcements_category,
            'all_announcements' => $all_announcements
        ]);
    }
}
