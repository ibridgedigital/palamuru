<?php

namespace App\Http\Controllers\PGCK;

use App\EventAttendance;
use App\EventPaymentLogs;
use App\Events;
use App\EventsCategory;
use App\Helpers\LanguageHelper;
use App\Helpers\NexelitHelpers;
use App\JobApplicant;
use App\Jobs;
use App\Language;
use App\Mail\BasicMail;
use App\Mail\OrderReply;
use App\Mail\PaymentSuccess;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\AnnouncementAttendance;
use App\AnnouncementPaymentLogs;
use App\Announcements;
use App\AnnouncementsCategory;
use App\PgckAnnouncements;
use App\PgckAnnouncementsCategory;
use App\Http\Controllers\Controller;

class AnnouncementsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:pgck_admin');
    }

    public function all_announcements(){
        $all_announcements = PgckAnnouncements::all()->groupBy('lang');
        return view('pgck.backend.announcements.all-announcements')->with(['all_announcements' => $all_announcements]);
    }

    public function new_announcement(){
        $all_languages = Language::all();
        $all_categories = PgckAnnouncementsCategory::where(['status' => 'publish','lang' => get_default_language()])->get();
        return view('pgck.backend.announcements.new-announcement')->with(['all_languages' => $all_languages,'all_categories' => $all_categories]);
    }

    public function store_announcement(Request $request){
        $this->validate($request,[
            'title' => 'required|string|max:191',
        ]);
        $slug = !empty($request->slug) ? $request->slug : Str::slug($request->title,$request->lang);
        $slug_check = PgckAnnouncements::where(['slug' => $slug,'lang' => $request->lang])->count();
        $slug = $slug_check > 0 ? $slug.'2' : $slug;

        PgckAnnouncements::create([
            'title' => $request->title,
            'slug' => $slug,
        	'pdf_url' => $request->pdf_url,
            'content' => $request->event_content,
            'category_id' => $request->category_id,
            'status' => $request->status,
            'lang' => $request->lang,
            'date' => $request->date,
            'time' => $request->time,
            'cost' => $request->cost,
            'available_tickets' => $request->available_tickets,
            'image' => $request->image,
            'organizer' => $request->organizer,
            'organizer_email' => $request->organizer_email,
            'organizer_website' => $request->organizer_website,
            'organizer_phone' => $request->organizer_phone,
            'venue' => $request->venue,
            'venue_location' => $request->venue_location,
            'venue_phone' => $request->venue_phone,
            'meta_tags' => $request->meta_tags,
            'meta_description' => $request->meta_description,
        ]);

        return redirect()->back()->with(['msg' => __('New Announcement Created Success...'),'type'=>'success']);
    }

    

    public function edit_announcement($id){
        $announcement = PgckAnnouncements::find($id);
        $all_languages = Language::all();
        $all_categories = PgckAnnouncementsCategory::where(['status' => 'publish','lang' => $announcement->lang])->get();
        return view('pgck.backend.announcements.edit-announcement')->with(['all_languages' => $all_languages,'all_categories' => $all_categories,'announcement' => $announcement]);
    }

    public function delete_announcement(Request $request,$id){
        PgckAnnouncements::find($id)->delete();
        return redirect()->back()->with(['msg' => __('Announcement Delete Success...'),'type'=>'danger']);
    }

    public function update_announcement(Request $request){
        $this->validate($request,[
            'title' => 'required|string|max:191',
            // 'lang' => 'required|string|max:191',
            'category_id' => 'required|string|max:191',
            // 'event_content' => 'required|string',
            // 'organizer' => 'nullable|string',
            // 'organizer_email' => 'nullable|string',
            // 'organizer_website' => 'nullable|string',
            // 'organizer_phone' => 'nullable|string',
            // 'venue' => 'nullable|string',
            // 'venue_location' => 'nullable|string',
            // 'venue_phone' => 'nullable|string',
            // 'time' => 'required|string',
            // 'image' => 'nullable|string',
            // 'date' => 'required|string',
            // 'cost' => 'required|string',
            // 'available_tickets' => 'required|string',
            'slug' => 'nullable|string'
        ]);

        $slug = !empty($request->slug) ? $request->slug : Str::slug($request->title,$request->lang);
        $slug_check = PgckAnnouncements::where(['slug' => $slug,'lang' => $request->lang])->count();
        $slug = $slug_check > 1 ? $slug.'2' : $slug;
        // dd($request->announcement_id);
        PgckAnnouncements::find($request->announcement_id)->update([
            'title' => $request->title,
            'slug' => $slug,
        	'pdf_url' => $request->pdf_url,
            'content' => $request->event_content,
            'category_id' => $request->category_id,
            'status' => $request->status,
            'lang' => $request->lang,
            'date' => $request->date,
            'time' => $request->time,
            'cost' => $request->cost,
            'available_tickets' => $request->available_tickets,
            'image' => $request->image,
            'organizer' => $request->organizer,
            'organizer_email' => $request->organizer_email,
            'organizer_website' => $request->organizer_website,
            'organizer_phone' => $request->organizer_phone,
            'venue' => $request->venue,
            'venue_location' => $request->venue_location,
            'venue_phone' => $request->venue_phone,
            'meta_tags' => $request->meta_tags,
            'meta_description' => $request->meta_description,
        ]);

        return redirect()->back()->with(['msg' => __('Announcement Update Success...'),'type'=>'success']);
    }

    public function single_page_settings(){
        $all_languages = Language::all();
        return view('backend.announcements.announcement-single-page-settings')->with(['all_languages' => $all_languages]);
    }

    public function update_single_page_settings(Request $request){
        $all_languages = Language::all();
        foreach ($all_languages as $lang){
            $this->validate($request,[
                'site_announcements_category_'.$lang->slug.'_title'  => 'nullable|string'
            ]);
            $all_fields = [
              'announcement_single_'.$lang->slug.'_announcement_info_title',
              'announcement_single_'.$lang->slug.'_date_title',
              'announcement_single_'.$lang->slug.'_time_title',
              'announcement_single_'.$lang->slug.'_cost_title',
              'announcement_single_'.$lang->slug.'_category_title',
              'announcement_single_'.$lang->slug.'_organizer_title',
              'announcement_single_'.$lang->slug.'_organizer_name_title',
              'announcement_single_'.$lang->slug.'_organizer_email_title',
              'announcement_single_'.$lang->slug.'_organizer_phone_title',
              'announcement_single_'.$lang->slug.'_organizer_website_title',
              'announcement_single_'.$lang->slug.'_venue_title',
              'announcement_single_'.$lang->slug.'_venue_name_title',
              'announcement_single_'.$lang->slug.'_venue_location_title',
              'announcement_single_'.$lang->slug.'_venue_phone_title',
              'announcement_single_'.$lang->slug.'_category_title',
              'announcement_single_'.$lang->slug.'_available_ticket_text',
              'announcement_single_'.$lang->slug.'_reserve_button_title',
              'announcement_single_'.$lang->slug.'_announcement_expire_text',
            ];
            foreach ($all_fields as $field){
                update_static_option($field,$request->$field);
            }
        }

        return redirect()->back()->with(['msg' => __('Announcements Single Page Settings Success...'),'type' => 'success']);
    }


    public function page_settings(){
        $all_languages = Language::all();
        return view('backend.announcements.announcement-page-settings')->with(['all_languages' => $all_languages]);
    }

    public function update_page_settings(Request $request){
        $this->validate($request,[
            'site_announcements_post_items' => 'required|string|max:191'
        ]);
        $all_languages = Language::all();
        foreach ($all_languages as $lang){
            $this->validate($request,[
                'site_announcements_category_'.$lang->slug.'_title'  => 'nullable|string'
            ]);
            $site_announcements_category_title = 'site_announcements_category_'.$lang->slug.'_title';
            update_static_option('site_announcements_category_'.$lang->slug.'_title',$request->$site_announcements_category_title);
        }
        update_static_option('site_announcements_post_items',$request->site_announcements_post_items);
        return redirect()->back()->with(['msg' => __('Announcements Page Settings Success...'),'type' => 'success']);
    }

    public function clone_announcement(Request $request){
        $announcement_details = PgckAnnouncements::find($request->item_id);
        PgckAnnouncements::create([
            'title' => $announcement_details->title,
            'slug' => $announcement_details->slug,
            'content' => $announcement_details->content,
            'category_id' => $announcement_details->category_id,
            'status' => 'draft',
            'lang' => $announcement_details->lang,
            'date' => $announcement_details->date,
            'time' => $announcement_details->time,
            'cost' => $announcement_details->cost,
            'available_tickets' => $announcement_details->available_tickets,
            'image' => $announcement_details->image,
            'organizer' => $announcement_details->organizer,
            'organizer_email' => $announcement_details->organizer_email,
            'organizer_website' => $announcement_details->organizer_website,
            'organizer_phone' => $announcement_details->organizer_phone,
            'venue' => $announcement_details->venue,
            'venue_location' => $announcement_details->venue_location,
            'venue_phone' => $announcement_details->venue_phone,
        ]);
        return redirect()->back()->with(['msg' => __('Announcements Clone Success...'),'type' => 'success']);
    }

    



    public function send_mail_announcement_attendance_logs(Request $request){
        $this->validate($request,[
            'email' => 'required|string|max:191',
            'name' => 'required|string|max:191',
            'subject' => 'required|string',
            'message' => 'required|string',
        ]);
        $subject = str_replace('{site}',get_static_option('site_'.get_default_language().'_title'),$request->subject);
        $data = [
            'name' => $request->name,
            'message' => $request->message,
        ];
        try {
            Mail::to($request->email)->send(new OrderReply($data,$subject));
        }catch (\Exception $e){
            return redirect()->back()->with(NexelitHelpers::item_delete($e->getMessage()));
        }
        return redirect()->back()->with(['msg' => __('Attendance Reply Mail Send Success...'),'type' => 'success']);
    }

    public function announcement_payment_logs(){
        $paymeng_logs = EventPaymentLogs::all();
        return view('backend.announcements.announcement-payment-logs-all')->with(['payment_logs' => $paymeng_logs]);
    }
    public function delete_announcement_payment_logs(Request $request,$id){
        EventPaymentLogs::find($id)->delete();
        return redirect()->back()->with(['msg' => __('Announcement Payment Log Delete Success...'),'type' => 'danger']);
    }

    public function approve_announcement_payment(Request $request,$id){

        $payment_logs = EventPaymentLogs::find($id);
        $payment_logs->status = 'complete';
        $payment_logs->save();

        $event_attendance = EventAttendance::find($payment_logs->attendance_id);
        $event_attendance->payment_status = 'complete';
        $event_attendance->status = 'complete';
        $event_attendance->save();

        $event_details = Events::find($event_attendance->event_id);
        $event_details->available_tickets = $event_details->available_tickets - $event_attendance->quantity;
        $event_details->save();

        //update database
        $event_payment_logs = EventPaymentLogs::find($id);
        $event_attendance = EventAttendance::find($event_payment_logs->attendance_id);

        $order_mail = get_static_option('announcement_attendance_receiver_mail') ? get_static_option('announcement_attendance_receiver_mail') : get_static_option('site_global_email');
        $event_details = Events::find($event_attendance->event_id);

        //send mail to admin
        $subject = __('you have an event booking order');
        $message = __('you have an event booking order. attendance Id').' #'.$event_attendance->id;
        $message .= ' '.__('at').' '.date_format($event_attendance->created_at,'d F Y H:m:s');
        $message .= ' '.__('via').' '.str_replace('_',' ',$event_payment_logs->package_gateway);
        $admin_mail = !empty(get_static_option('event_attendance_receiver_mail')) ? get_static_option('event_attendance_receiver_mail') : get_static_option('site_global_email');

        try {
            Mail::to($admin_mail)->send(new \App\Mail\EventAttendance([
                'subject' => $subject,
                'message' => $message,
                'event_attendance' => $event_attendance,
                'event_details' => $event_details,
                'event_payment_logs' => $event_payment_logs,
            ]));
        }catch (\Exception $e){
            return redirect()->back()->with(['msg' => __('Manual Payment Accept Success, mail send failed').' '.$e->getMessage(),'type' => 'success']);
        }

        //send mail to user
        $subject = __('your event booking order has been placed');
        $message = __('your event booking order has been placed. attendance Id').' #'.$event_attendance->id;
        $message .= ' '.__('at').' '.date_format($event_attendance->created_at,'d F Y H:m:s');
        $message .= ' '.__('via').' '.str_replace('_',' ',$event_payment_logs->package_gateway);
        try {
            Mail::to($order_mail)->send(new \App\Mail\EventAttendance([
                'subject' => $subject,
                'message' => $message,
                'event_attendance' => $event_attendance,
                'event_details' => $event_details,
                'event_payment_logs' => $event_payment_logs,
            ]));
        }catch (\Exception $e){
            return redirect()->back()->with(['msg' => __('Manual Payment Accept Success, mail send failed').' '.$e->getMessage(),'type' => 'success']);
        }

        return redirect()->back()->with(['msg' => __('Manual Payment Accept Success'),'type' => 'success']);
    }

    public function payment_success_page_settings(){
        $all_languages = Language::all();
        return view('backend.announcements.announcement-payment-success-page')->with(['all_languages' => $all_languages]);
    }

    public function update_payment_success_page_settings(Request $request){
        $all_languages = Language::all();
        foreach ($all_languages as $lang){
            $this->validate($request,[
                'event_success_page_'.$lang->slug.'_title' => 'nullable|string',
                'event_success_page_'.$lang->slug.'_description' => 'nullable|string',
            ]);
            $all_fields = [
                'event_success_page_'.$lang->slug.'_title',
                'event_success_page_'.$lang->slug.'_description',
            ];
            foreach ($all_fields as $field){
                update_static_option($field,$request->$field);
            }
        }
        return redirect()->back()->with(['msg' => __('Settings Update Success'),'type' => 'success']);
    }

    public function payment_cancel_page_settings(){
        $all_languages = Language::all();
        return view('backend.announcements.announcement-payment-cancel-page')->with(['all_languages' => $all_languages]);
    }

    public function update_payment_cancel_page_settings(Request $request){
        $all_languages = Language::all();
        foreach ($all_languages as $lang){
            $this->validate($request,[
                'event_cancel_page_'.$lang->slug.'_title' => 'nullable|string',
                'event_cancel_page_'.$lang->slug.'_subtitle' => 'nullable|string',
                'event_cancel_page_'.$lang->slug.'_description' => 'nullable|string',
            ]);
            $all_fields = [
                'event_cancel_page_'.$lang->slug.'_title',
                'event_cancel_page_'.$lang->slug.'_subtitle',
                'event_cancel_page_'.$lang->slug.'_description',
            ];
            foreach ($all_fields as $field){
                update_static_option($field,$request->$field);
            }
        }
        return redirect()->back()->with(['msg' => __('Settings Update Success'),'type' => 'success']);
    }

    public function bulk_action(Request $request){
        PgckAnnouncements::whereIn('id',$request->ids)->delete();
        return response()->json(['status' => 'ok']);
    }

    public function attendance_logs_bulk_action(Request $request){
        EventAttendance::whereIn('id',$request->ids)->delete();
        return response()->json(['status' => 'ok']);
    }

    public function payment_logs_bulk_action(Request $request){
        EventPaymentLogs::whereIn('id',$request->ids)->delete();
        return response()->json(['status' => 'ok']);
    }
    
    public function payment_report(Request  $request){
        $order_data = '';
        $query = EventPaymentLogs::query();
        if (!empty($request->start_date)){
            $query->whereDate('created_at','>=',$request->start_date);
        }
        if (!empty($request->end_date)){
            $query->whereDate('created_at','<=',$request->end_date);
        }
        if (!empty($request->payment_status)){
            $query->where(['status' => $request->payment_status ]);
        }
        $error_msg = __('select start & end date to generate event payment report');
        if (!empty($request->start_date) && !empty($request->end_date)){
            $query->orderBy('id','DESC');
            $order_data =  $query->paginate($request->items);
            $error_msg = '';
        }

        return view('backend.announcements.payment-report')->with([
            'order_data' => $order_data,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'items' => $request->items,
            'payment_status' => $request->payment_status,
            'error_msg' => $error_msg
        ]);
    }

    public function attendance_report(Request  $request){
        $order_data = '';
        $events = Events::where(['status' => 'publish','lang' => get_default_language()])->get();
        $query = EventAttendance::query();
        if (!empty($request->start_date)){
            $query->whereDate('created_at','>=',$request->start_date);
        }
        if (!empty($request->end_date)){
            $query->whereDate('created_at','<=',$request->end_date);
        }
        if (!empty($request->event_id)){
            $query->where(['event_id' => $request->event_id ]);
        }
        $error_msg = __('select start & end date to generate event attendance report');
        if (!empty($request->start_date) && !empty($request->end_date)){
            $query->orderBy('id','DESC');
            $order_data =  $query->paginate($request->items);
            $error_msg = '';
        }

        return view('backend.announcements.attendance-report')->with([
            'order_data' => $order_data,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'items' => $request->items,
            'event_id' => $request->event_id,
            'events' => $events,
            'error_msg' => $error_msg
        ]);
    }

    public function announcement_attedance_reminder(Request $request){
        //send order reminder mail
        $order_details = EventAttendance::find($request->id);
        $payment_log = EventPaymentLogs::where(['attendance_id' => $request->id])->first();
        $data['subject'] = __('your event booking is still in pending at').' '. get_static_option('site_'.get_default_language().'_title');
        $data['message'] = __('hello').' '.$payment_log->name .'<br>';
        $data['message'] .= __('your event booking').' #'.$order_details->id.' ';
        $data['message'] .= __('is still in pending, to complete your booking go to');
        $data['message'] .= ' <a href="'.route('user.home').'">'.__('your dashboard').'</a>';

        //send mail while order status change
        try {
            Mail::to($payment_log->email)->send(new BasicMail($data));
        }catch (\Exception $e){
            return back()->with(NexelitHelpers::item_delete($e->getMessage()));
        }

        return redirect()->back()->with(['msg' => __('Reminder Mail Send Success'),'type' => 'success']);
    }

    public function settings(){
        return view('backend.announcements.settings')->with(['all_languages' => LanguageHelper::all_languages()]);
    }
    public function update_settings(Request $request){
        $this->validate($request,[
            'disable_guest_mode_for_event_module' => 'nullable|string'
        ]);
        $all_fields = [
            'disable_guest_mode_for_event_module'
        ];
        foreach ($all_fields as $field){
            update_static_option($field,$request->$field);
        }
        return back()->with(NexelitHelpers::settings_update());
    }
}
