<?php

namespace App\Http\Controllers\PGCW;

use App\Announcements;
use App\AnnouncementsCategory;
use App\PgcwAnnouncements;
use App\PgcwAnnouncementsCategory;
use App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AnnouncementsCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pgcw_admin');
    }

    public function all_announcements_category(){
        $all_category = PgcwAnnouncementsCategory::all()->groupBy('lang');
        $all_languages = Language::all();
        return view('pgcw.backend.announcements.all-announcements-category')->with(['all_category' => $all_category,'all_languages' => $all_languages] );
    }

    public function store_announcements_category(Request $request){
        $this->validate($request,[
            'title' => 'required|string|max:191|unique:events_categories',
            'lang' => 'required|string|max:191',
            'status' => 'required|string|max:191'
        ]);
        PgcwAnnouncementsCategory::create($request->all());

        return redirect()->back()->with([
            'msg' => __('New Category Added...'),
            'type' => 'success'
        ]);
    }

    public function update_announcements_category(Request $request){
        $this->validate($request,[
            'title' => 'required|string|max:191',
            'lang' => 'required|string|max:191',
            'status' => 'required|string|max:191'
        ]);

        PgcwAnnouncementsCategory::find($request->id)->update([
            'title' => $request->title,
            'status' => $request->status,
            'lang' => $request->lang,
        ]);

        return redirect()->back()->with([
            'msg' => __( 'Category Update Success...'),
            'type' => 'success'
        ]);
    }

    public function delete_announcements_category(Request $request,$id){
        if (PgcwAnnouncements::where('category_id',$id)->first()){
            return redirect()->back()->with([
                'msg' => __('You Can Not Delete This Category, It Already Associated With An Announcement...'),
                'type' => 'danger'
            ]);
        }
        PgcwAnnouncementsCategory::find($id)->delete();
        return redirect()->back()->with([
            'msg' => __('Category Delete Success...'),
            'type' => 'danger'
        ]);
    }


    public function bulk_action(Request $request){
        if (PgcwAnnouncements::whereIn('category_id',$request->ids)->count() > 0){
            return response()->json(['status' => 'no']);
        }
        PgcwAnnouncementsCategory::whereIn('id',$request->ids)->delete();
        return response()->json(['status' => 'ok']);
    }
}
