<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function redirectTo(){
        return route('user.home');
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('logout');
        $this->middleware('guest:uce_admin')->except('logout');
    	$this->middleware('guest:pgcg_admin')->except('logout');
    	$this->middleware('guest:pgck_admin')->except('logout');
        $this->middleware('guest:pgcw_admin')->except('logout');
        $this->middleware('guest:upgc_admin')->except('logout');
        $this->middleware('guest:ucp_admin')->except('logout');
    }
    /**
     * Override username functions
     * @since 1.0.0
     * */
    public function username() {
        return 'username';
    }

    /**
     * show admin login page
     * @since 1.0.0
     * */
    public function showAdminLoginForm(){
        return view('auth.admin.login');
    }

    public function showUceAdminLoginForm(){
        return view('uce.auth.admin.login');
    }
	
	public function showPgcgAdminLoginForm(){
        return view('pgcg.auth.admin.login');
    }
	public function showPgckAdminLoginForm(){
        return view('pgck.auth.admin.login');
    }
    public function showPgcwAdminLoginForm(){
        return view('pgcw.auth.admin.login');
    }
    public function showUpgcAdminLoginForm(){
        return view('upgc.auth.admin.login');
    }
    public function showUcpAdminLoginForm(){
        return view('ucp.auth.admin.login');
    }

    public function uceAdminLogin(Request $request)
    {
        $this->validate($request, [
            'username'   => 'required|string',
            'password' => 'required|min:6'
        ],[
            'username.required'   => __('username required'),
            'password.required' => __('password required')
        ]);

        if (Auth::guard('uce_admin')->attempt(['username' => $request->username, 'password' => $request->password], $request->get('remember'))) {

            return response()->json([
               'msg' => __('Login Success Redirecting'),
               'type' => 'success',
               'status' => 'ok'
            ]);
        }
        return response()->json([
            'msg' => __('Your Username or Password Is Wrong !!'),
            'type' => 'danger',
            'status' => 'not_ok'
        ]);
    }
	public function pgcgAdminLogin(Request $request)
    {
        $this->validate($request, [
            'username'   => 'required|string',
            'password' => 'required|min:6'
        ],[
            'username.required'   => __('username required'),
            'password.required' => __('password required')
        ]);

        if (Auth::guard('pgcg_admin')->attempt(['username' => $request->username, 'password' => $request->password], $request->get('remember'))) {
            return response()->json([
               'msg' => __('Login Success Redirecting'),
               'type' => 'success',
               'status' => 'ok'
            ]);
        }
        return response()->json([
            'msg' => __('Your Username or Password Is Wrong !!'),
            'type' => 'danger',
            'status' => 'not_ok'
        ]);
    }
	
    public function pgckAdminLogin(Request $request)
    {
        $this->validate($request, [
            'username'   => 'required|string',
            'password' => 'required|min:6'
        ],[
            'username.required'   => __('username required'),
            'password.required' => __('password required')
        ]);

        if (Auth::guard('pgck_admin')->attempt(['username' => $request->username, 'password' => $request->password], $request->get('remember'))) {
            return response()->json([
               'msg' => __('Login Success Redirecting'),
               'type' => 'success',
               'status' => 'ok'
            ]);
        }
        return response()->json([
            'msg' => __('Your Username or Password Is Wrong !!'),
            'type' => 'danger',
            'status' => 'not_ok'
        ]);
    }
    public function pgcwAdminLogin(Request $request)
    {
        $this->validate($request, [
            'username'   => 'required|string',
            'password' => 'required|min:6'
        ],[
            'username.required'   => __('username required'),
            'password.required' => __('password required')
        ]);

        if (Auth::guard('pgcw_admin')->attempt(['username' => $request->username, 'password' => $request->password], $request->get('remember'))) {
            return response()->json([
               'msg' => __('Login Success Redirecting'),
               'type' => 'success',
               'status' => 'ok'
            ]);
        }
        return response()->json([
            'msg' => __('Your Username or Password Is Wrong !!'),
            'type' => 'danger',
            'status' => 'not_ok'
        ]);
    }
    public function upgcAdminLogin(Request $request)
    {
        $this->validate($request, [
            'username'   => 'required|string',
            'password' => 'required|min:6'
        ],[
            'username.required'   => __('username required'),
            'password.required' => __('password required')
        ]);

        if (Auth::guard('upgc_admin')->attempt(['username' => $request->username, 'password' => $request->password], $request->get('remember'))) {
            return response()->json([
               'msg' => __('Login Success Redirecting'),
               'type' => 'success',
               'status' => 'ok'
            ]);
        }
        return response()->json([
            'msg' => __('Your Username or Password Is Wrong !!'),
            'type' => 'danger',
            'status' => 'not_ok'
        ]);
    }
    public function ucpAdminLogin(Request $request)
    {
        $this->validate($request, [
            'username'   => 'required|string',
            'password' => 'required|min:6'
        ],[
            'username.required'   => __('username required'),
            'password.required' => __('password required')
        ]);

        if (Auth::guard('ucp_admin')->attempt(['username' => $request->username, 'password' => $request->password], $request->get('remember'))) {
            return response()->json([
               'msg' => __('Login Success Redirecting'),
               'type' => 'success',
               'status' => 'ok'
            ]);
        }
        return response()->json([
            'msg' => __('Your Username or Password Is Wrong !!'),
            'type' => 'danger',
            'status' => 'not_ok'
        ]);
    }
    /**
     * admin login system
     * */
    public function adminLogin(Request $request)
    {
        $this->validate($request, [
            'username'   => 'required|string',
            'password' => 'required|min:6'
        ],[
            'username.required'   => __('username required'),
            'password.required' => __('password required')
        ]);

        if (Auth::guard('admin')->attempt(['username' => $request->username, 'password' => $request->password], $request->get('remember'))) {

            return response()->json([
               'msg' => __('Login Success Redirecting'),
               'type' => 'success',
               'status' => 'ok'
            ]);
        }
        return response()->json([
            'msg' => __('Your Username or Password Is Wrong !!'),
            'type' => 'danger',
            'status' => 'not_ok'
        ]);
    }
    /**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('frontend.user.login');
    }


}
