<?php

namespace App\Http\Controllers\Frontend;

use App\Appointment;
use App\AppointmentBooking;
use App\AppointmentCategory;
use App\AppointmentLang;
use App\AppointmentReview;
use App\Course;
use App\CourseCoupon;
use App\User;
use App\UserCategory;
use App\UserProfile;
use App\ProfileReview;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class ProfileController extends Controller
{
    public $base_path = 'frontend.pages.appointment.';
    public function single($slug,$id){
        $item = UserProfile::where('id',$id)->first();
        return view('frontend.pages.profile.single')->with([
            'item' =>$item
        ]);
    }

    public function department($id){
        $dept_name = UserCategory::findOrFail($id)->title;
        $all_appointment = UserProfile::with(['department'])->where(['dept_id' => $id])->orderBy('id','desc')->paginate(9);
        return view('frontend.pages.profile.profile-department')->with(['dept_name'=>$dept_name,'all_appointment' => $all_appointment]);
    }
	public function category($id){
    	$cat_name = UserCategory::findOrFail($id)->title;
        $all_appointment = UserProfile::with(['department'])->where(['category_id' => $id])->orderBy('id','desc')->paginate(9);
        return view('frontend.pages.profile.appointment-category')->with(['cat_name'=>$cat_name,'all_appointment' => $all_appointment]);
    }

}
