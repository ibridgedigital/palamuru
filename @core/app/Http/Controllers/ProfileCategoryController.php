<?php

namespace App\Http\Controllers;

use App\AppointmentCategory;
use App\AppointmentCategoryLang;
use App\Helpers\NexelitHelpers;
use App\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\UserCategory;

class ProfileCategoryController extends Controller
{
    private $all_languages;
    public function __construct()
    {
        $this->all_languages = Language::all();
    }

    public function category_all(){
         // $all_categories = AppointmentCategory::all();
         $all_categories = UserCategory::all();
         return view('frontend3.appointment.appointment-category')->with(['all_category' => $all_categories,'all_languages' => $this->all_languages ]);
    }

    public function category_new(Request $request){
        // dd("test");
        $this->validate($request,[
            'title' => 'required',
        ]);
// dd($request);
        DB::beginTransaction();

        try {
            $cat_id = UserCategory::create(['title' => $request->title])->id;
            
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            throwException($e->getMessage());
        }

        return back()->with(NexelitHelpers::item_new());
    }

    public function category_delete(Request $request,$id){
        // dd($id);
        UserCategory::findOrFail($id)->delete();
        // AppointmentCategory::findOrFail($id)->delete();
        // AppointmentCategoryLang::where('cat_id',$id)->delete();
        return back()->with(NexelitHelpers::item_delete());
    }

    public function bulk_action(Request $request){
        UserCategory::whereIn('id',$request->ids)->delete();
        // AppointmentCategory::whereIn('id',$request->ids)->delete();
        // AppointmentCategoryLang::whereIn('cat_id',$request->ids)->delete();

        return response()->json(['status' => 'ok']);
    }

    public function category_update(Request $request){
        // dd($request);
        $this->validate($request,[
            'title' => 'required',
        ]);

        DB::beginTransaction();

        try {
            UserCategory::find($request->id)->update(['title' => $request->title]);
            
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            throwException($e->getMessage());
        }

        return back()->with(NexelitHelpers::item_update());
    }


}
