<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($guard == 'admin' && Auth::guard($guard)->check()){
            return redirect()->route('admin.home');
        }
        if ($guard == 'uce_admin' && Auth::guard($guard)->check()){
            return redirect()->route('uce.admin.home');
        }
    	if ($guard == 'pgcg_admin' && Auth::guard($guard)->check()){
            return redirect()->route('pgcg.admin.home');
        }
    	if ($guard == 'pgck_admin' && Auth::guard($guard)->check()){
            return redirect()->route('pgck.admin.home');
        }
        if ($guard == 'pgcw_admin' && Auth::guard($guard)->check()){
            return redirect()->route('pgcw.admin.home');
        }
        if ($guard == 'upgc_admin' && Auth::guard($guard)->check()){
            return redirect()->route('upgc.admin.home');
        }
        if ($guard == 'ucp_admin' && Auth::guard($guard)->check()){
            return redirect()->route('ucp.admin.home');
        }
        if ($guard == 'web' && Auth::guard($guard)->check()){
            return redirect()->route('user.home');
        }

        if (Auth::guard($guard)->check()) {
            return redirect('/');
        }

        return $next($request);
    }
}
