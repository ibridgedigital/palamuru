<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcgAdminRole extends Model
{
    protected $table = 'pgcg_admin_roles';
    protected $fillable = ['name' ,'permission'];

}
