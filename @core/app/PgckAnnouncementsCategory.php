<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgckAnnouncementsCategory extends Model
{
    protected $table = 'pgck_announcements_categories';
    protected $fillable = ['title','status','lang'];
}
