<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UcpAdminRole extends Model
{
    protected $table = 'ucp_admin_roles';
    protected $fillable = ['name' ,'permission'];

}
