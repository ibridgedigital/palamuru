<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UcpTestimonial extends Model
{
    protected $table ='ucp_testimonials';
    protected $fillable = ['name','image','lang','status','description','designation'];
}
