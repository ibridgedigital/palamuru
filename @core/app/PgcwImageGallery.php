<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgcwImageGallery extends Model
{
    protected $table = 'pgcw_image_galleries';
    protected $fillable = ['image','title','lang','cat_id'];
}
