<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UceMenu extends Model
{
    protected $table = 'uce_menus';
    protected $fillable = ['title','lang','content','status'];
}
