<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgckImageGalleryCategory extends Model
{
    protected $table = 'pgck_image_gallery_categories';
    protected $fillable = ['title','status','lang'];
}
