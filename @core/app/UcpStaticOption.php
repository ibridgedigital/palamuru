<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UcpStaticOption extends Model
{
    protected $table = 'ucp_static_options';
    protected $fillable = ['option_name','option_value'];
}
