<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgckMenu extends Model
{
    protected $table = 'pgck_menus';
    protected $fillable = ['title','lang','content','status'];
}
