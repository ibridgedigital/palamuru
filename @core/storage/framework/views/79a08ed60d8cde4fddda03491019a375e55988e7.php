<?php 
if($home_page_variant == '07' || $home_page_variant == '09' || Route::currentRouteName() == 'frontend.course.lesson'){ return;} ?>
<?php if(!empty(get_static_option('home_page_support_bar_section_status'))): ?>
    <div class="top-bar-area header-variant-<?php echo e(get_static_option('home_page_variant')); ?>">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="top-bar-inner">
                        <div class="left-content">
                            <ul>
                                <li><a href="./"><i class="fa fa-home"></i> Home</a></li>
                            </ul>
                        </div>
                    	<?php 
                        $base_url = url('/').'/p';
                        ?>
                        <div class="right-content">
                            <ul class="topbar-left">
                            	<li class="current-menu-item"><a href="https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ltmpl=default&hd=palamuruuniversity.ac.in&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin">UNIVERSITY E-MAIL</a></li>
                            	<li class="current-menu-item"><a href="<?php echo e($base_url); ?>/nirf-2022-report">NIRF-2022-REPORT</a></li>
                               	<li class="current-menu-item"><a href="<?php echo e($base_url); ?>/cbcs-syllabus">CBCS Syllabus</a></li>
                                <li class="current-menu-item"><a href="<?php echo e($base_url); ?>/ssr-links">SSR links</a></li>
                                <li class="current-menu-item"><a href="<?php echo e($base_url); ?>/self-study-report">Self Study Report</a></li>
                                <li class="current-menu-item"><a href="<?php echo e($base_url); ?>/allumni ">alumni</a></li>
                                <li class="current-menu-item"><a href="<?php echo e($base_url); ?>/almanac">Almanac</a></li>
                                <li class="current-menu-item"><a href="<?php echo e($base_url); ?>/rti-act">RTI Act</a></li>
                                <li class="current-menu-item"><a href="<?php echo e($base_url); ?>/contact-us">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?><?php /**PATH /home/ibridge/public_html/projects/palamuru/v3/@core/resources/views/frontend/partials/supportbar.blade.php ENDPATH**/ ?>