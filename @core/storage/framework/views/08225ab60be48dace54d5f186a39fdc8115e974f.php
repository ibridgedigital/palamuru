<?php
  $post_img = null;
  $blog_image = get_attachment_image_by_id($item->image,"full",false);
  $post_img = !empty($blog_image) ? $blog_image['img_url'] : '';
 ?>

<?php $__env->startSection('og-meta'); ?>
    <meta property="og:url"  content="<?php echo e(route('frontend.appointment.single',[$item->lang_front->slug ?? __('untitled'),$item->id])); ?>" />
    <meta property="og:type"  content="article" />
    <meta property="og:title"  content="<?php echo e($item->name); ?>" />
    <meta property="og:image" content="<?php echo e($item->image); ?>" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-meta-data'); ?>
    <meta name="description" content="<?php echo e($item->meta_description); ?>">
    <meta name="tags" content="<?php echo e($item->meta_tag); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('site-title'); ?>
    <?php echo e($item->name); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-title'); ?>
    <?php echo e($item->name); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <section class="blog-details-content-area padding-top-100 padding-bottom-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="appointment-details-item">
                       <div class="top-part">
                           <div class="thumb">
                               <?php echo render_image_markup_by_attachment_id($item->image,'full'); ?>

                           </div>
                           <div class="content">
                               <?php if($item->designation): ?>
                               <span class="designation"><?php echo e($item->designation); ?></span>
                               <?php endif; ?>
                               <h2 class="title"><?php echo e($item->name); ?></h2>
                               <div class="short-description"><?php echo $item->short_description; ?></div>
                               
                               <div class="social-share-wrap">
                                   <ul class="social-share">
                                        <?php if(!empty($item->facebook)): ?>
                                        <li>
                                            <a class="facebook" href="<?php echo e($item->facebook); ?>"><i class="fab fa-facebook-f"></i></a>
                                       </li>
                                       <?php endif; ?>
                                       <?php if(!empty($item->twitter)): ?>
                                       <li>
                                            <a class="twitter" href="<?php echo e($item->twitter); ?>"><i class="fab fa-twitter"></i></a>
                                        </li>
                                        <?php endif; ?>
                                        <?php if(!empty($item->linkedin)): ?>
                                        <li>
                                            <a class="linkedin" href="<?php echo e($item->linkedin); ?>"><i class="fab fa-linkedin-in"></i></a>
                                        </li>
                                        <?php endif; ?>
                                        <?php if(!empty($item->pinterest)): ?>
                                        <li>
                                            <a class="pinterest" href="<?php echo e($item->pinterest); ?>"><i class="fab fa-pinterest-p"></i></a>
                                        </li>
                                        <?php endif; ?>
                                   </ul>
                               </div>
                           </div>
                       </div>

                        <div class="bottom-part">
                             <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.error-msg','data' => []]); ?>
<?php $component->withName('error-msg'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
                             <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.flash-msg','data' => []]); ?>
<?php $component->withName('flash-msg'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
                            <nav>
                                <div class="nav nav-tabs" role="tablist">
                                    <a class="nav-link "  data-toggle="tab" href="#nav-information" role="tab"  aria-selected="false">Information</a>
                                    <a class="nav-link active"  data-toggle="tab" href="#nav-booking" role="tab"  aria-selected="true">Publications</a>
                                    <a class="nav-link"  data-toggle="tab" href="#nav-feedback" role="tab"  aria-selected="false">Achievements</a>
                                </div>
                            </nav>

                            <div class="tab-content" >
                                <div class="tab-pane fade" id="nav-information" role="tabpanel" >
                                    <div class="information-area-wrap">
                                        <div class="description-wrap">
                                            <h3 class="title"><?php echo e(get_static_option('appointment_single_'.$user_select_lang_slug.'_appointment_booking_about_me_title')); ?></h3>
                                            <?php echo $item->description; ?>

                                        </div>
                                        
                                        <?php
                                            $experience_info = explode('|', $item->experience_info);
                                            ?>
                                        <?php if(!empty($item->experience_info)): ?>
                                        <div class="education-info">
                                            <h3 class="title">Experience Info</h3>
                                            <ul class="circle-list">
                                                <?php $__currentLoopData = $experience_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li><?php echo e($info); ?></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                        <?php endif; ?>
                                        
                                        <?php
                                            $additional_info = explode('|', $item->additional_info);
                                            ?>
                                        <?php if(!empty($item->additional_info)): ?>
                                        <div class="additional-info">
                                            <h3 class="title">Additional Info</h3>
                                            <ul class="circle-list">
                                                <?php $__currentLoopData = $additional_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li><?php echo e($info); ?></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                        <?php endif; ?>
                                        
                                        <?php
                                        $specialized_info = explode('|', $item->specialized_info);
                                        ?>
                                        <?php if(!empty($item->specialized_info)): ?>
                                        <div class="specialised-info">
                                            <h3 class="title">Specialize Info</h3>
                                            <ul class="circle-list">
                                                <?php $__currentLoopData = $specialized_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li><?php echo e($info); ?></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                        <?php endif; ?>
                                        
                                    </div>
                                </div>
                                <div class="tab-pane fade show active" id="nav-booking" role="tabpanel" >
                                    <div class="information-area-wrap">
                                        
                                        <?php
                                        $publications = explode('|', $item->publications);
                                        ?>
                                        <?php if(!empty($item->publications)): ?>
                                        <div class="additional-info">
                                            <h3 class="title">Publications</h3>
                                            <ul class="circle-list">
                                                <?php $__currentLoopData = $publications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $publication): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li><?php echo e($publication); ?></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-feedback" role="tabpanel" >
                                    <div class="feedback-wrapper">
                                    	<?php
                                        $achievements = explode('|', $item->achievements);
                                        ?>
                                    	<?php if(!empty($item->achievements)): ?>
                                        <div class="feedback-form-wrapper">
                                            <h3 class="title"><?php echo e(__('Achievements')); ?></h3>
                                        	<ul class="circle-list">
                                                <?php $__currentLoopData = $achievements; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $achievement): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li><?php echo e($achievement); ?></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                            
                                        </div>
                                    	<?php endif; ?>
                                    </div>

                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript" src="//use.fontawesome.com/5ac93d4ca8.js"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/frontend/js/bootstrap4-rating-input.js')); ?>"></script>
    <?php echo $__env->make('frontend.partials.ajax-login-form-js', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <script>
        (function ($){
            "use strict";

            $(document).on('change','#guest_logout',function (e) {
                e.preventDefault();
                var infoTab = $('#nav-profile-tab');
                var nextBtn = $('.next-step-btn');
                if($(this).is(':checked')){
                    $('.booking-wrap .login-form').hide();
                    infoTab.attr('disabled',false).removeClass('disabled');
                    nextBtn.show();

                }else{
                    $('.login-form').show();
                    infoTab.attr('disabled',true).addClass('disabled');
                    nextBtn.hide();
                }
            });
            $(document).on('click','.next-step-btn',function(e){
                var infoTab = $('#nav-profile-tab');
                infoTab.attr('disabled',false).removeClass('disabled').addClass('active').siblings().removeClass('active');
                $('#nav-profile').addClass('show active').siblings().removeClass('show active');
            });

            $(document).on('click','.payment-gateway-wrapper > ul > li',function (e) {
                e.preventDefault();
                var gateway = $(this).data('gateway');
                var manual_gateway_tr = $('.manual_payment_transaction_field');
                $(this).addClass('selected').siblings().removeClass('selected');
                $('input[name="selected_payment_gateway"]').val(gateway);
                if(gateway === 'manual_payment'){
                    manual_gateway_tr.show();
                }else{
                    manual_gateway_tr.hide();
                }
            });

            $(document).on('click','.time-slot.date li',function (e){
                e.preventDefault();
                var date = $(this).data('date');
                date = date.split('-');
                var showDate = new Date(date[2]+'-'+ date[1]+'-'+date[0]);
                $('.time_slog_date').text(showDate.toDateString());
                $(this).toggleClass('selected').siblings().removeClass('selected');
                $('input[name="booking_date"]').val($(this).data('date'));
            });
            $(document).on('click','.time-slot.time li',function (e){
                e.preventDefault();
                $(this).toggleClass('selected').siblings().removeClass('selected');
                $('input[name="booking_time_id"]').val($(this).data('id'));
            });


            

            //appo_booking_btn
        })(jQuery);
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.frontend-page-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ibridge/public_html/projects/palamuru/v2/@core/resources/views/frontend/pages/profile/single.blade.php ENDPATH**/ ?>