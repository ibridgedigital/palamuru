<?php $__env->startSection('site-title'); ?>
    <?php echo e(__('Edit Profile')); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('assets/backend/css/summernote-bs4.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/backend/css/dropzone.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/backend/css/media-uploader.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/backend/css/bootstrap-tagsinput.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/backend/css/bootstrap-datepicker.min.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('section'); ?>
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                 <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.error-msg','data' => []]); ?>
<?php $component->withName('error-msg'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
                 <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.flash-msg','data' => []]); ?>
<?php $component->withName('flash-msg'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
            </div>
            <div class="col-lg-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <div class="header-wrap d-flex justify-content-between">
                            <h4 class="header-title"><?php echo e(__('Edit Profile')); ?></h4>
                            <a href="<?php echo e(route('user.profile.all')); ?>" class="btn btn-info"><?php echo e(__('All Profiles')); ?></a>
                        </div>
                        <form action="<?php echo e(route('update.user.profile')); ?>" method="post" enctype="multipart/form-data">

                            <input type="hidden" value="<?php echo e($user_profile->id); ?>" name="id">
                        	<input type="hidden" value="<?php echo e($user_profile->user_id); ?>" name="user_id">
                            <?php echo csrf_field(); ?>
                            <ul class="nav nav-tabs" role="tablist">
                                <?php $default_lang = get_default_language(); ?>
                                
                                    <li class="nav-item">
                                        <a class="nav-link active"  data-toggle="tab" href="#slider_tab" role="tab" aria-controls="home" aria-selected="true">English (USA)</a>
                                    </li>    
                            </ul>
                            <div class="tab-content margin-top-40" >
                                <!-- <?php $__currentLoopData = $item->lang_all; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $appointment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> -->
                                    <div class="tab-pane fade show active " id="slider_tab" role="tabpanel" >
                                        <div class="form-group">
                                            <label for="name"><?php echo e(__('Name')); ?></label>
                                            <input type="text" class="form-control" name="name" value="<?php echo e($user_profile->name); ?>">
                                        </div>
                                    	<div class="form-group">
                                            <label for="username"><?php echo e(__('User Name')); ?></label>
                                            <input type="text" class="form-control" name="username" value="<?php echo e($user->username); ?>">
                                        </div>
                                    	<div class="form-group">
                                            <label for="email"><?php echo e(__('Email')); ?></label>
                                            <input type="email" class="form-control" name="email" value="<?php echo e($user_profile->user->email); ?>" placeholder="<?php echo e(__('Email')); ?>">
                                        </div>
                                    	<div class="form-group">
                                            <label for="slug"><?php echo e(__('Slug')); ?></label>
                                            <input type="text" class="form-control" name="slug" value="<?php echo e($user_profile->slug); ?>">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="designation"><?php echo e(__('Designation')); ?></label>
                                            <input type="text" class="form-control"  name="designation" value="<?php echo e($user_profile->designation); ?>" >
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo e(__('Description')); ?></label>
                                            <input type="hidden" name="description" value="<?php echo e($user_profile->description); ?>">
                                            <div class="summernote" data-content='<?php echo e($user_profile->description); ?>'></div>
                                        </div>                                        
                                        <div class="form-group">
                                            <label for="location"><?php echo e(__('Location')); ?></label>
                                            <input type="text" name="location" class="form-control" value="<?php echo e($user_profile->location); ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="short_description"><?php echo e(__('Short Description')); ?></label>
                                            <textarea name="short_description" cols="30" rows="5" class="form-control" placeholder="<?php echo e(__('Short Description')); ?>"><?php echo e($user_profile->short_description); ?></textarea>
                                        </div>

                                        <div class="iconbox-repeater-wrapper dynamic-repeater">
                                            <label for="additional_info" class="d-block"><?php echo e(__('Additional Info')); ?></label>
                                            <?php
                                            $additional_info = explode('|', $user_profile->additional_info);
                                            ?>
                                            <?php $__empty_1 = true; $__currentLoopData = $additional_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $add_info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                            <div class="all-field-wrap">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="additional_info[]"  value="<?php echo e($add_info); ?>" placeholder="<?php echo e(__('additional info')); ?>">
                                                </div>
                                                <div class="action-wrap">
                                                    <span class="add"><i class="ti-plus"></i></span>
                                                    <span class="remove"><i class="ti-trash"></i></span>
                                                </div>
                                            </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                <div class="all-field-wrap">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="additional_info[]"  placeholder="<?php echo e(__('additional info')); ?>">
                                                    </div>
                                                    <div class="action-wrap">
                                                        <span class="add"><i class="ti-plus"></i></span>
                                                        <span class="remove"><i class="ti-trash"></i></span>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="iconbox-repeater-wrapper  dynamic-repeater">
                                            <label for="experience_info" class="d-block"><?php echo e(__('Experience Info')); ?></label>
                                            <?php
                                            $experience_info = explode('|', $user_profile->experience_info);
                                            ?>
                                            <?php $__empty_1 = true; $__currentLoopData = $experience_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $exp_info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                            <div class="all-field-wrap">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="experience_info[]" placeholder="<?php echo e(__('Experience Info')); ?>" value="<?php echo e($exp_info); ?>">
                                                </div>
                                                <div class="action-wrap">
                                                    <span class="add"><i class="ti-plus"></i></span>
                                                    <span class="remove"><i class="ti-trash"></i></span>
                                                </div>
                                            </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                <div class="all-field-wrap">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="experience_info[]" placeholder="<?php echo e(__('Experience Info')); ?>">
                                                    </div>
                                                    <div class="action-wrap">
                                                        <span class="add"><i class="ti-plus"></i></span>
                                                        <span class="remove"><i class="ti-trash"></i></span>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        
                                        <div class="iconbox-repeater-wrapper  dynamic-repeater">
                                            <label for="specialized_info" class="d-block"><?php echo e(__('Specialized Info')); ?></label>
                                            <?php
                                            $specialized_info = explode('|', $user_profile->specialized_info);
                                            ?>
                                            <?php $__empty_1 = true; $__currentLoopData = $specialized_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spe_info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                            <div class="all-field-wrap">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="specialized_info[]" placeholder="<?php echo e(__('Specialized Info')); ?>" value="<?php echo e($spe_info); ?>">
                                                </div>
                                                <div class="action-wrap">
                                                    <span class="add"><i class="ti-plus"></i></span>
                                                    <span class="remove"><i class="ti-trash"></i></span>
                                                </div>
                                            </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                <div class="all-field-wrap">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="specialized_info[]"  placeholder="<?php echo e(__('Specialized Info')); ?>">
                                                    </div>
                                                    <div class="action-wrap">
                                                        <span class="add"><i class="ti-plus"></i></span>
                                                        <span class="remove"><i class="ti-trash"></i></span>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="iconbox-repeater-wrapper  dynamic-repeater">
                                            <label for="publications" class="d-block"><?php echo e(__('Publications')); ?></label>
                                            <?php
                                            $publications = explode('|', $user_profile->publications);
                                            ?>
                                            <?php $__empty_1 = true; $__currentLoopData = $publications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $publication): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                            <div class="all-field-wrap">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="publications[]" placeholder="<?php echo e(__('Publications')); ?>" value="<?php echo e($publication); ?>">
                                                </div>
                                                <div class="action-wrap">
                                                    <span class="upload"><i class="ti-upload"></i></span>
                                                    <span class="add"><i class="ti-plus"></i></span>
                                                    <span class="remove"><i class="ti-trash"></i></span>
                                                </div>
                                            </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                <div class="all-field-wrap">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="publications[]"  placeholder="<?php echo e(__('Publications')); ?>">
                                                    </div>
                                                    <div class="action-wrap">
                                                        <span class="upload"><i class="ti-upload"></i></span>
                                                        <span class="add"><i class="ti-plus"></i></span>
                                                        <span class="remove"><i class="ti-trash"></i></span>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="form-group mb-2">
                                            <label for="facebook"><?php echo e(__('Social Links')); ?></label>
                                            <!-- <input type="text" class="form-control" name="facebook" placeholder="<?php echo e(__('Facebook')); ?>"> -->
                                            <div class="row">
                                                <div class="col">
                                                    <input type="text" class="form-control" placeholder="<?php echo e(__('Facebook')); ?>" name="facebook" value="<?php echo e($user_profile->facebook); ?>">
                                                </div>
                                                <div class="col">
                                                    <input type="text" class="form-control" placeholder="<?php echo e(__('Twitter')); ?>" value="<?php echo e($user_profile->twitter); ?>" name="twitter">
                                                </div>
                                                <div class="col">
                                                    <input type="text" class="form-control" placeholder="<?php echo e(__('LinkedIn')); ?>" name="linkedin" value="<?php echo e($user_profile->linkedin); ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="meta_title"><?php echo e(__('Meta title')); ?></label>
                                            <input type="text" class="form-control" name="meta_title" placeholder="<?php echo e(__('Meta title')); ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="meta_description"><?php echo e(__('Meta Description')); ?></label>
                                            <textarea  class="form-control max-height-120" name="meta_description"cols="30" rows="10" placeholder="<?php echo e(__('Meta Description')); ?>"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="meta_tags"><?php echo e(__('Meta Tags')); ?></label>

                                            

                                            <input type="text" name="meta_tags"  class="form-control" data-role="tagsinput" value="<?php echo e($user_profile->meta_tags); ?>">
                                        </div>
                                    </div>
                                <!-- <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> -->
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="category"><?php echo e(__('Category')); ?></label>
                                        <select name="category_id" class="form-control" id="category">
                                            <option value=""><?php echo e(__("Select Category")); ?></option>
                                            <?php $__currentLoopData = $all_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($category->id); ?>" <?php if($category->id == $user_profile->category_id): ?> selected <?php endif; ?>><?php echo e($category->title); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                	<div class="form-group">
                                        <label for="department"><?php echo e(__('Department')); ?></label>
                                        <select name="department_id" class="form-control" id="department">
                                            <option value=""><?php echo e(__("Select Department")); ?></option>
                                            <?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $department): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($department->id); ?>" <?php if($department->id == $user_profile->dept_id): ?> selected <?php endif; ?>><?php echo e($department->dept_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                    
                                     <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.media-upload','data' => ['name' => 'image','title' => __('Image'),'id' => $user_profile->image,'dimentions' => '350x500']]); ?>
<?php $component->withName('media-upload'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['name' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute('image'),'title' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('Image')),'id' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute($user_profile->image),'dimentions' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute('350x500')]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
                                    

                                    <button type="submit"
                                            class="btn btn-primary mt-4 pr-4 pl-4"><?php echo e(__('Submit')); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $__env->make('backend.partials.media-upload.media-upload-markup', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <?php echo $__env->make('backend.partials.repeater.script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <script>
        $(document).ready(function () {
            $(document).on('click','ul.time_slot li',function (e){
                e.preventDefault();
                //prent selector
                var parent = $(this).parent().parent();
                //append input field value by this id
                var ids = parent.find('input[name="booking_time_ids"]');
                var oldValue = ids.val()
                //assign new value =
                var id = $(this).data('id');
                if(oldValue != ''){
                    var oldValAr = oldValue.split(',');
                    if($(this).hasClass('selected')){
                        var oldValAr = oldValAr.filter(function (item){return item != id;});
                    }else{
                        oldValAr.push(id);
                    }
                    ids.val(oldValAr.toString());
                }else{
                    ids.val(id);
                }
                //add class for this li
                $(this).toggleClass('selected');
            });
            $(document).on('change', '#language', function (e) {
                e.preventDefault();
                var selectedLang = $(this).val();
                $.ajax({
                    url: "<?php echo e(route('admin.appointment.category.by.lang')); ?>",
                    type: "POST",
                    data: {
                        _token: "<?php echo e(csrf_token()); ?>",
                        lang: selectedLang
                    },
                    success: function (data) {
                        $('#category').html('<option value=""><?php echo e(__("Select Category")); ?></option>');
                        $.each(data, function (index, value) {
                            $('#category').append('<option value="' + value.id + '">' + value.title + '</option>')
                        });
                    }
                });
            });
            $('.summernote').summernote({
                height: 250,   //set editable area's height
                codemirror: { // codemirror options
                    theme: 'monokai'
                },
                callbacks: {
                    onChange: function (contents, $editable) {
                        $(this).prev('input').val(contents);
                    }
                }
            });
            if($('.summernote').length > 0){
                $('.summernote').each(function(index,value){
                    $(this).summernote('code', $(this).data('content'));
                });
            }
        });
    </script>
    <script src="<?php echo e(asset('assets/backend/js/summernote-bs4.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/dropzone.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/js/bootstrap-tagsinput.js')); ?>"></script>
    <?php echo $__env->make('backend.partials.media-upload.media-js', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend3.user.dashboard.user-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ibridge/public_html/projects/palamuru/v2/@core/resources/views/frontend3/user/dashboard/edit-profile.blade.php ENDPATH**/ ?>