<?php echo $__env->make('ucp.frontend.partials.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->yieldContent('content'); ?>
<?php echo $__env->make('ucp.frontend.partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php /**PATH /home/palamuruuniversity/public_html/@core/resources/views/ucp/frontend/frontend-master.blade.php ENDPATH**/ ?>