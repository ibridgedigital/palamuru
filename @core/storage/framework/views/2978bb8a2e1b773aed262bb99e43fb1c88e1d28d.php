<?php    
    $post_img = null;
    $blog_image = get_attachment_image_by_id($announcement->image,"full",false);
    $post_img = !empty($blog_image) ? $blog_image['img_url'] : '';
?>
<?php $__env->startSection('og-meta'); ?>
    <meta property="og:url"  content="<?php echo e(route('uce.frontend.announcements.single',$announcement->slug)); ?>" />
    <meta property="og:type"  content="article" />
    <meta property="og:title"  content="<?php echo e($announcement->title); ?>" />
    <meta property="og:image" content="<?php echo e($post_img); ?>" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('site-title'); ?>
    <?php echo e($announcement->title); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-title'); ?>
    <?php echo e(__('Announcements')); ?>

    <!-- <?php echo e($announcement->title); ?> -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-meta-data'); ?>
    <meta name="description" content="<?php echo e($announcement->meta_tags); ?>">
    <meta name="tags" content="<?php echo e($announcement->meta_description); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <section class="blog-content-area padding-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="single-event-details">
                        <div>
                            <h2><?php echo e($announcement->title); ?></h2>
                        </div>
                        <div class="thumb">
                            <?php echo render_image_markup_by_attachment_id($announcement->image,'','large'); ?>

                        </div>
                        <div class="content">
                            <div class="details-content-area">
                                <?php echo $announcement->content; ?>

                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="widget-area">
                        
                        <div class="widget event-info">
                            <h4 class="widget-title">Recent Announcements</h4>
                            <ul class="icon-with-title-description">
                                <?php $__currentLoopData = $all_announcements; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $announcement): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li>
                                    <div class="content">
                                        <h4 class="title"><a href="<?php echo e(route('frontend.announcements.single',$announcement->slug)); ?>"><?php echo e($announcement->title); ?></a></h4>
                                    </div>
                                </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                        <?php echo App\WidgetsBuilder\WidgetBuilderSetup::render_frontend_sidebar('announcement',['column' => false]); ?>

                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(asset('assets/common/js/countdown.jquery.js')); ?>"></script>
    <script>
        var ev_offerTime = "<?php echo e($announcement->date); ?>";
        var ev_year = ev_offerTime.substr(0, 4);
        var ev_month = ev_offerTime.substr(5, 2);
        var ev_day = ev_offerTime.substr(8, 2);

        if (ev_offerTime) {
            $('#event_countdown').countdown({
                year: ev_year,
                month: ev_month,
                day: ev_day,
                labels: true,
                labelText: {
                    'days': "<?php echo e(__('days')); ?>",
                    'hours': "<?php echo e(__('hours')); ?>",
                    'minutes': "<?php echo e(__('min')); ?>",
                    'seconds': "<?php echo e(__('sec')); ?>",
                }
            });
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('uce.frontend.frontend-page-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\v3\@core\resources\views/uce/frontend/pages/announcements/announcement-single.blade.php ENDPATH**/ ?>