
<?php $__env->startSection('site-title'); ?>
    <?php echo e(__('Dashboard')); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('section'); ?>
    <section class="blog-details-content-area padding-top-100 padding-bottom-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="appointment-details-item">
                       <div class="top-part">
                           <div class="thumb">
                               <?php echo render_image_markup_by_attachment_id($user_profile->image,'full'); ?>

                           </div>
                           <div class="content">
                               <span class="designation"><?php echo e($user_profile->category->title ?? ''); ?></span>
                               <h2 class="title"><?php echo e($user_profile->name ?? ''); ?></h2>
                               <div class="short-description"><?php echo $user_profile->short_description ?? ''; ?></div>
            
                               <div class="social-share-wrap">
                                   
                                    <ul class="social-share">
                                        <?php if(!empty($user_profile->facebook)): ?>
                                        <li>
                                            <a class="facebook" href="<?php echo e($user_profile->facebook); ?>"><i class="fab fa-facebook-f"></i></a>
                                       </li>
                                       <?php endif; ?>
                                       <?php if(!empty($user_profile->twitter)): ?>
                                       <li>
                                            <a class="twitter" href="<?php echo e($user_profile->twitter); ?>"><i class="fab fa-twitter"></i></a>
                                        </li>
                                        <?php endif; ?>
                                        <?php if(!empty($user_profile->linkedin)): ?>
                                        <li>
                                            <a class="linkedin" href="<?php echo e($user_profile->linkedin); ?>"><i class="fab fa-linkedin-in"></i></a>
                                        </li>
                                        <?php endif; ?>
                                        <?php if(!empty($user_profile->pinterest)): ?>
                                        <li>
                                            <a class="pinterest" href="<?php echo e($user_profile->pinterest); ?>"><i class="fab fa-pinterest-p"></i></a>
                                        </li>
                                        <?php endif; ?>
                                    </ul>
                               </div>
                           </div>
                       </div>
                        <div class="bottom-part">
                             <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.error-msg','data' => []]); ?>
<?php $component->withName('error-msg'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
                             <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.flash-msg','data' => []]); ?>
<?php $component->withName('flash-msg'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
                            <nav>
                                <div class="nav nav-tabs" role="tablist">
                                    <a class="nav-link "  data-toggle="tab" href="#nav-information" role="tab"  aria-selected="false">Information</a>
                                    <a class="nav-link active"  data-toggle="tab" href="#nav-booking" role="tab"  aria-selected="true">Publications</a>
                                    <a class="nav-link"  data-toggle="tab" href="#nav-feedback" role="tab"  aria-selected="false">Achievements</a>
                                </div>
                            </nav>
                            <div class="tab-content" >
                                <div class="tab-pane fade" id="nav-information" role="tabpanel" >
                                    <div class="information-area-wrap">
                                        <?php if(!empty($user_profile->description)): ?>
                                        <div class="description-wrap">
                                            <h3 class="title">About Me</h3>
                                            <?php echo $user_profile->description; ?>

                                        </div>
                                        <?php endif; ?>
                                        <?php
                                            $experience_info = explode('|', $user_profile->experience_info);
                                            ?>
                                        <?php if(!empty($experience_info)): ?>
                                        <div class="education-info">
                                            <h3 class="title">Experience Info</h3>
                                            <ul class="circle-list">
                                                <?php $__currentLoopData = $experience_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li><?php echo e($info); ?></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                        <?php endif; ?>
                                        <?php
                                            $additional_info = explode('|', $user_profile->additional_info);
                                            ?>
                                        <?php if(!empty($additional_info)): ?>
                                        <div class="additional-info">
                                            <h3 class="title">Additional Info</h3>
                                            <ul class="circle-list">
                                                <?php $__currentLoopData = $additional_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li><?php echo e($info); ?></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                        <?php endif; ?>
                                        <?php
                                        $specialized_info = explode('|', $user_profile->specialized_info);
                                        ?>
                                        <?php if(!empty($specialized_info)): ?>
                                        <div class="specialised-info">
                                            <h3 class="title">Specialize Info</h3>
                                            <ul class="circle-list">
                                                <?php $__currentLoopData = $specialized_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li><?php echo e($info); ?></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="tab-pane fade show active" id="nav-booking" role="tabpanel" >
                                    <div class="information-area-wrap">
                                        
                                        <?php
                                        $publications = explode('|', $user_profile->publications);
                                        ?>
                                        <?php if(!empty($publications)): ?>
                                        <div class="additional-info">
                                            <h3 class="title">Publications</h3>
                                            <ul class="circle-list">
                                                <?php $__currentLoopData = $publications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $publication): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li><?php echo e($publication); ?></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-feedback" role="tabpanel" >
                                    <div class="feedback-wrapper">
                                        <?php if(auth()->guard('web')->check()): ?>
                                        <div class="feedback-form-wrapper">
                                            <h3 class="title"><?php echo e(__('Leave your feedback')); ?></h3>
                                            <form action="<?php echo e(route('frontend.appointment.review')); ?>" method="post" class="appointment-booking-form" id="appointment_rating_form">
                                                <?php echo csrf_field(); ?>
                                                <div class="error-message"></div>
                                                <input type="hidden" name="appointment_id" value="">
                                                <div class="form-group">
                                                    <label for="rating-empty-clearable2"><?php echo e(__('Ratings')); ?></label>
                                                    <input type="number" name="ratings"
                                                           id="rating-empty-clearable2"
                                                           class="rating text-warning"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for=""><?php echo e(__('Message')); ?></label>
                                                    <textarea name="message" cols="30" class="form-control" rows="5"></textarea>
                                                </div>
                                                <button type="submit" class="btn-boxed appointment" id="appointment_ratings"><?php echo e(__('Submit')); ?>  <i class="fas fa-spinner fa-spin d-none"></i></button>
                                            </form>
                                        </div>
                                        <?php else: ?>
                                            <?php echo $__env->make('frontend.partials.ajax-login-form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                        <?php endif; ?>
                                        
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend3.user.dashboard.user-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ibridge/public_html/projects/palamuru/v2/@core/resources/views/frontend3/user/dashboard/user-home-single.blade.php ENDPATH**/ ?>