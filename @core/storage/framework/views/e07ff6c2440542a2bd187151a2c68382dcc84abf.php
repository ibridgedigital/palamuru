<?php 
if($home_page_variant == '07' || $home_page_variant == '09' || Route::currentRouteName() == 'frontend.course.lesson'){ return;} ?>
<?php if(!empty(get_static_option('home_page_support_bar_section_status'))): ?>
    <div class="top-bar-area header-variant-<?php echo e(get_static_option('home_page_variant')); ?>">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="top-bar-inner">
                        <div class="left-content">
                            <ul>
                                <li><a href="<?php echo e(url('/')); ?>/UCP"><i class="fa fa-home"></i> Home</a></li>
                            </ul>
                        </div>
                    	<?php 
                        $base_url = url('/UCP').'/p';
                        ?>
                        <div class="right-content">
                            <ul class="topbar-left">
                                <li class="current-menu-item"><a href="https://www.onlinesbi.com/prelogin/icollecthome.htm?corpID=907106">SB Collect-Online Payment for Hostel</a></li>
                                <li class="current-menu-item"><a href="<?php echo e($base_url); ?>/pci-professional-ethics">PCI Professional Ethics</a></li>
                                <li class="current-menu-item"><a href="<?php echo e(url('/')); ?>">Palamuru University</a></li>
                                <li class="current-menu-item"><a href="<?php echo e($base_url); ?>/almanac">Almanac</a></li>
                                <li class="current-menu-item"><a href="<?php echo e($base_url); ?>/allumni-association">Allumni Association</a></li>
                                <li class="current-menu-item"><a href="<?php echo e($base_url); ?>/gallery">Gallery</a></li>
                                <li class="current-menu-item"><a href="<?php echo e($base_url); ?>/contact-us">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?><?php /**PATH /home/palamuruuniversity/public_html/@core/resources/views/ucp/frontend/partials/supportbar.blade.php ENDPATH**/ ?>