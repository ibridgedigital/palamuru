<?php $home_page_variant = get_static_option('home_page_variant');?>
<div class="sidebar-menu">
    <div class="sidebar-header">
        <div class="logo" style="max-height: 50px;">
            <a href="<?php echo e(route('user.home')); ?>">
                <?php
                    $logo_type = 'site_logo';
                    if(!empty(get_static_option('site_admin_dark_mode'))){
                        $logo_type = 'site_white_logo';
                    }
                ?>
                <?php echo render_image_markup_by_attachment_id(get_static_option($logo_type)); ?>

            </a>
        </div>
    </div>
    <div class="main-menu">
        <div class="menu-inner">
            <nav id="main_menu_wrap">
                <ul class="metismenu" id="menu">
                    <li class="<?php echo e(active_menu('user-home')); ?>">
                        <a href="<?php echo e(route('user.home')); ?>"
                           aria-expanded="true">
                            <i class="ti-dashboard"></i>
                            <span><?php echo app('translator')->get('dashboard'); ?></span>
                        </a>
                    </li>
                    <!-- <?php if(Auth::user()->role <= 3): ?>
                    <li class="main_dropdown <?php if(request()->is('user-home/profile/*')): ?> active <?php endif; ?> ">
                        <a href="javascript:void(0)" aria-expanded="true">
                            <?php echo e(__('Profiles Manage')); ?> </a>
                        <ul class="collapse">
                            <li class="<?php echo e(active_menu('user-home/profile/all')); ?>">
                                <a href="<?php echo e(route('user.profile.all')); ?>"><?php echo e(__('All Profiles')); ?></a></li>
                            
                            <li class="<?php echo e(active_menu('user-home/profile/new')); ?>">
                                <a href="<?php echo e(route('user.profile.new')); ?>"><?php echo e(__('New Profile')); ?></a></li>
                            
                            <li class="<?php echo e(active_menu('user-home/profile/category')); ?>">
                                <a href="<?php echo e(route('user.profile.category.all')); ?>"><?php echo e(__('Category')); ?></a></li>
                            
                        </ul>
                    </li>
                    <?php endif; ?> -->
                    <?php if(Auth::user()->role == 4): ?>
                    <li class="<?php echo e(active_menu('user-home/profile/all')); ?>">
                            <a href="<?php echo e(route('user.home.edit.profile')); ?>"><?php echo e(__('Edit Profile')); ?></a></li>
                        
                    <li class="<?php echo e(active_menu('user-home/profile/new')); ?>">
                            <a href="<?php echo e(route('user.logout')); ?>"
                                onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <?php echo e(__('Logout')); ?>

                            </a></li>
                    <?php endif; ?>
                        

                    <?php if(Auth::user()->role <= 3): ?>
                    
                        <li class="<?php echo e(active_menu('user-home/profile/all')); ?>">
                            <a href="<?php echo e(route('user.profile.all')); ?>"><?php echo e(__('All Profiles')); ?></a></li>
                        
                        <li class="<?php echo e(active_menu('user-home/profile/new')); ?>">
                            <a href="<?php echo e(route('user.profile.new')); ?>"><?php echo e(__('New Profile')); ?></a></li>
                        
                        <li class="<?php echo e(active_menu('user-home/profile/category')); ?>">
                            <a href="<?php echo e(route('user.profile.category.all')); ?>"><?php echo e(__('Category')); ?></a></li>
            			<li class="<?php echo e(active_menu('user-home/profile/department')); ?>">
                            <a href="<?php echo e(route('user.profile.department.all')); ?>"><?php echo e(__('Departments')); ?></a></li> 
                       
                    <?php endif; ?>
                </ul>
            </nav>
        </div>
    </div>
</div>
<?php /**PATH /home/ibridge/public_html/projects/palamuru/v2/@core/resources/views/frontend3/partials/sidebar.blade.php ENDPATH**/ ?>