<?php $__env->startSection('content'); ?>
    <?php
    $page_partial = 'home-'.get_uce_static_option('home_page_variant');

    if (!empty(get_uce_static_option('home_page_page_builder_status'))){
        $page_partial = 'page-builder';
    }
    ?>
<?php echo $__env->make('pgcg.frontend.home-pages.'.$page_partial, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function (){
        var ids = <?php echo json_encode($final); ?>;
        for (let i = 0; i < ids.length; i++){
            $('#'+ids[i].child).appendTo('#'+ids[i].parent);
        }
    });
</script>

<?php echo $__env->make('pgcg.frontend.frontend-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ibridge/public_html/projects/palamuru/v3/@core/resources/views/pgcg/frontend/frontend-home.blade.php ENDPATH**/ ?>