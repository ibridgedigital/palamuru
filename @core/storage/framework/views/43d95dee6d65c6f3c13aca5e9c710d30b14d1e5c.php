<?php $__env->startSection('page-meta-data'); ?>
<meta name="description" content="<?php echo e($page_post->meta_description); ?>">
<meta name="tags" content="<?php echo e($page_post->meta_tags); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('site-title'); ?>
    <?php echo e($page_post->title); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-title'); ?>
    <?php echo e($page_post->title); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <section class="blog-content-area padding-120">
        <div class="container">
            <div class="row">
                <!--Main Navigation-->
                <div class="col-lg-3">
                    <div class="ib-sidebar">
                        <?php echo render_frontend_menu_sidebar($slug); ?>

                    </div>
                </div>
                
                <div class="col-lg-9">
                    <div class="single-event-details">
                        
                        <div class="content">
                            <div class="details-content-area">
                                <?php if(!empty($page_post->page_builder_status)): ?>
                                    <?php echo \App\PageBuilder\PageBuilderSetup::render_frontend_pagebuilder_content_for_dynamic_page('dynamic_page',$page_post->id); ?>


                                <?php else: ?>
                                    <?php echo $__env->make('ucp.frontend.partials.dynamic-page-content', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                <?php endif; ?>  

                            </div>                                          
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </section> 
<?php $__env->stopSection(); ?>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function (){
        var ids = <?php echo json_encode($final); ?>;
        for (let i = 0; i < ids.length; i++){
            $('#'+ids[i].child).appendTo('#'+ids[i].parent);
        }
    });
</script>
<!-- for custom slider -->


<?php echo $__env->make('ucp.frontend.frontend-page-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ibridge/public_html/projects/palamuru/v3/@core/resources/views/ucp/frontend/pages/dynamic-single.blade.php ENDPATH**/ ?>