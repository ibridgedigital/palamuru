@extends('frontend.frontend-page-master')
@php
  $post_img = null;
  $blog_image = get_attachment_image_by_id($item->image,"full",false);
  $post_img = !empty($blog_image) ? $blog_image['img_url'] : '';
 @endphp

@section('og-meta')
    <meta property="og:url"  content="{{route('frontend.appointment.single',[$item->lang_front->slug ?? __('untitled'),$item->id])}}" />
    <meta property="og:type"  content="article" />
    <meta property="og:title"  content="{{$item->name}}" />
    <meta property="og:image" content="{{$item->image}}" />
@endsection
@section('page-meta-data')
    <meta name="description" content="{{$item->meta_description}}">
    <meta name="tags" content="{{$item->meta_tag}}">
@endsection
@section('site-title')
    {{$item->name}}
@endsection
@section('page-title')
    {{$item->name}}
@endsection
@section('content')
    <section class="blog-details-content-area padding-top-100 padding-bottom-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="appointment-details-item">
                       <div class="top-part">
                           <div class="thumb">
                               {!! render_image_markup_by_attachment_id($item->image,'full') !!}
                           </div>
                           <div class="content">
                               @if($item->designation)
                               <span class="designation">{{$item->designation}}</span>
                               @endif
                               <h2 class="title">{{$item->name}}</h2>
                               <div class="short-description">{!! $item->short_description	 !!}</div>
                               
                               <div class="social-share-wrap">
                                   <ul class="social-share">
                                        @if(!empty($item->facebook))
                                        <li>
                                            <a class="facebook" href="{{$item->facebook}}"><i class="fab fa-facebook-f"></i></a>
                                       </li>
                                       @endif
                                       @if(!empty($item->twitter))
                                       <li>
                                            <a class="twitter" href="{{$item->twitter}}"><i class="fab fa-twitter"></i></a>
                                        </li>
                                        @endif
                                        @if(!empty($item->linkedin))
                                        <li>
                                            <a class="linkedin" href="{{$item->linkedin}}"><i class="fab fa-linkedin-in"></i></a>
                                        </li>
                                        @endif
                                        @if(!empty($item->pinterest))
                                        <li>
                                            <a class="pinterest" href="{{$item->pinterest}}"><i class="fab fa-pinterest-p"></i></a>
                                        </li>
                                        @endif
                                   </ul>
                               </div>
                           </div>
                       </div>

                        <div class="bottom-part">
                            <x-error-msg/>
                            <x-flash-msg/>
                            <nav>
                                <div class="nav nav-tabs" role="tablist">
                                    <a class="nav-link "  data-toggle="tab" href="#nav-information" role="tab"  aria-selected="false">Information</a>
                                    <a class="nav-link active"  data-toggle="tab" href="#nav-booking" role="tab"  aria-selected="true">Publications</a>
                                    <a class="nav-link"  data-toggle="tab" href="#nav-feedback" role="tab"  aria-selected="false">Achievements</a>
                                </div>
                            </nav>

                            <div class="tab-content" >
                                <div class="tab-pane fade" id="nav-information" role="tabpanel" >
                                    <div class="information-area-wrap">
                                        <div class="description-wrap">
                                            <h3 class="title">{{get_static_option('appointment_single_'.$user_select_lang_slug.'_appointment_booking_about_me_title')}}</h3>
                                            {!! $item->description !!}
                                        </div>
                                        
                                        @php
                                            $experience_info = explode('|', $item->experience_info);
                                            @endphp
                                        @if(!empty($item->experience_info))
                                        <div class="education-info">
                                            <h3 class="title">Experience Info</h3>
                                            <ul class="circle-list">
                                                @foreach($experience_info as $info)
                                                    <li>{{$info}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        
                                        @php
                                            $additional_info = explode('|', $item->additional_info);
                                            @endphp
                                        @if(!empty($item->additional_info))
                                        <div class="additional-info">
                                            <h3 class="title">Additional Info</h3>
                                            <ul class="circle-list">
                                                @foreach($additional_info as $info)
                                                    <li>{{$info}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        
                                        @php
                                        $specialized_info = explode('|', $item->specialized_info);
                                        @endphp
                                        @if(!empty($item->specialized_info))
                                        <div class="specialised-info">
                                            <h3 class="title">Specialize Info</h3>
                                            <ul class="circle-list">
                                                @foreach($specialized_info as $info)
                                                    <li>{{$info}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        
                                    </div>
                                </div>
                                <div class="tab-pane fade show active" id="nav-booking" role="tabpanel" >
                                    <div class="information-area-wrap">
                                        
                                        @php
                                        $publications = explode('|', $item->publications);
                                        @endphp
                                        @if(!empty($item->publications))
                                        <div class="additional-info">
                                            <h3 class="title">Publications</h3>
                                            <ul class="circle-list">
                                                @foreach($publications as $publication)
                                                    <li>{{$publication}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-feedback" role="tabpanel" >
                                    <div class="feedback-wrapper">
                                    	@php
                                        $achievements = explode('|', $item->achievements);
                                        @endphp
                                    	@if(!empty($item->achievements))
                                        <div class="feedback-form-wrapper">
                                            <h3 class="title">{{__('Achievements')}}</h3>
                                        	<ul class="circle-list">
                                                @foreach($achievements as $achievement)
                                                    <li>{{$achievement}}</li>
                                                @endforeach
                                            </ul>
                                            
                                        </div>
                                    	@endif
                                    </div>

                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript" src="//use.fontawesome.com/5ac93d4ca8.js"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/js/bootstrap4-rating-input.js')}}"></script>
    @include('frontend.partials.ajax-login-form-js')
    <script>
        (function ($){
            "use strict";

            $(document).on('change','#guest_logout',function (e) {
                e.preventDefault();
                var infoTab = $('#nav-profile-tab');
                var nextBtn = $('.next-step-btn');
                if($(this).is(':checked')){
                    $('.booking-wrap .login-form').hide();
                    infoTab.attr('disabled',false).removeClass('disabled');
                    nextBtn.show();

                }else{
                    $('.login-form').show();
                    infoTab.attr('disabled',true).addClass('disabled');
                    nextBtn.hide();
                }
            });
            $(document).on('click','.next-step-btn',function(e){
                var infoTab = $('#nav-profile-tab');
                infoTab.attr('disabled',false).removeClass('disabled').addClass('active').siblings().removeClass('active');
                $('#nav-profile').addClass('show active').siblings().removeClass('show active');
            });

            $(document).on('click','.payment-gateway-wrapper > ul > li',function (e) {
                e.preventDefault();
                var gateway = $(this).data('gateway');
                var manual_gateway_tr = $('.manual_payment_transaction_field');
                $(this).addClass('selected').siblings().removeClass('selected');
                $('input[name="selected_payment_gateway"]').val(gateway);
                if(gateway === 'manual_payment'){
                    manual_gateway_tr.show();
                }else{
                    manual_gateway_tr.hide();
                }
            });

            $(document).on('click','.time-slot.date li',function (e){
                e.preventDefault();
                var date = $(this).data('date');
                date = date.split('-');
                var showDate = new Date(date[2]+'-'+ date[1]+'-'+date[0]);
                $('.time_slog_date').text(showDate.toDateString());
                $(this).toggleClass('selected').siblings().removeClass('selected');
                $('input[name="booking_date"]').val($(this).data('date'));
            });
            $(document).on('click','.time-slot.time li',function (e){
                e.preventDefault();
                $(this).toggleClass('selected').siblings().removeClass('selected');
                $('input[name="booking_time_id"]').val($(this).data('id'));
            });


            

            //appo_booking_btn
        })(jQuery);
    </script>
@endsection
