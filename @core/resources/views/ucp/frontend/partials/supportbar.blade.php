@php 
if($home_page_variant == '07' || $home_page_variant == '09' || Route::currentRouteName() == 'frontend.course.lesson'){ return;} @endphp
@if(!empty(get_static_option('home_page_support_bar_section_status')))
    <div class="top-bar-area header-variant-{{get_static_option('home_page_variant')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="top-bar-inner">
                        <div class="left-content">
                            <ul>
                                <li><a href="{{url('/')}}/UCP"><i class="fa fa-home"></i> Home</a></li>
                            </ul>
                        </div>
                    	@php 
                        $base_url = url('/UCP').'/p';
                        @endphp
                        <div class="right-content">
                            <ul class="topbar-left">
                                <li class="current-menu-item"><a href="https://www.onlinesbi.com/prelogin/icollecthome.htm?corpID=907106">SB Collect-Online Payment for Hostel</a></li>
                                <li class="current-menu-item"><a href="{{$base_url}}/pci-professional-ethics">PCI Professional Ethics</a></li>
                                <li class="current-menu-item"><a href="{{url('/')}}">Palamuru University</a></li>
                                <li class="current-menu-item"><a href="{{$base_url}}/almanac">Almanac</a></li>
                                <li class="current-menu-item"><a href="{{$base_url}}/allumni-association">Allumni Association</a></li>
                                <li class="current-menu-item"><a href="{{$base_url}}/gallery">Gallery</a></li>
                                <li class="current-menu-item"><a href="{{$base_url}}/contact-us">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif