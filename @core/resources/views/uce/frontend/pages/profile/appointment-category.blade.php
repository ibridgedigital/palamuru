@extends('frontend.frontend-page-master')
@section('site-title')
    {{__('Profile')}} {{__('Category:')}} {{$cat_name}}
@endsection
@section('page-title')
    {{__('Profile')}} {{__('Category:')}} {{$cat_name}}
@endsection
@section('page-meta-data')
    <meta name="description" content="{{get_static_option('appointment_page_'.$user_select_lang_slug.'_meta_description')}}">
    <meta name="tags" content="{{get_static_option('appointment_page_'.$user_select_lang_slug.'_meta_tags')}}">
@endsection
@section('content')
    <section class="appointment-content-area padding-top-120 padding-bottom-90">
        <div class="container">
            <div class="row">
                        @forelse($all_appointment as $data)
                        <div class="col-lg-4">
                            <div class="appointment-single-item">
                                <div class="thumb"
                                {!! render_background_image_markup_by_attachment_id($data->image,'','grid') !!}
                                    >
                                    <div class="cat">
                                        <a href="{{route('frontend.profile.category',['id' => $data->category_id,'any' => Str::slug($data->category->title ?? __("Uncategorized"))])}}">{{$data->category->title ?? __("Uncategorized")}}</a>
                                    </div>
                                </div>
                                <div class="content">
                                    @if(!empty($data->designation))
                                        <span class="designation">{{$data->designation}}</span>
                                	
                                    @endif
                                	@if(!empty($data->dept_id))
                                		<span class="designation">{{$data->department->dept_name}}</span>
                                    @endif
                                    <a href="{{route('frontend.profile.single',[$data->slug ?? __('untitled'),$data->id])}}"><h4 class="title">{{$data->name}}</h4></a>
                                    
                                    @if(!empty($data->location))
                                        <span class="location"><i class="fas fa-map-marker-alt"></i>{{$data->location}}</span>
                                    @endif
                                    <p>{{Str::words(strip_tags($data->short_description),10)}}</p>
                                    
                                </div>
                            </div>
                        </div>
                        @empty
                        <div class="col-lg-12 text-center">
                           <div class="alert alert-warning">{{__('nothing found')}} <strong>{{$search_term}}</strong></div>
                        </div>
                        @endforelse
                <div class="col-lg-12 text-center">
                    <nav class="pagination-wrapper " aria-label="Page navigation ">
                        {{$all_appointment->links()}}
                    </nav>
                </div>
            </div>
        </div>
    </section>
@endsection
