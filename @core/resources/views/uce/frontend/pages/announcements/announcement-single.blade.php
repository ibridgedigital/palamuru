@extends('uce.frontend.frontend-page-master')
@php    
    $post_img = null;
    $blog_image = get_attachment_image_by_id($announcement->image,"full",false);
    $post_img = !empty($blog_image) ? $blog_image['img_url'] : '';
@endphp
@section('og-meta')
    <meta property="og:url"  content="{{route('uce.frontend.announcements.single',$announcement->slug)}}" />
    <meta property="og:type"  content="article" />
    <meta property="og:title"  content="{{$announcement->title}}" />
    <meta property="og:image" content="{{$post_img}}" />
@endsection
@section('site-title')
    {{$announcement->title}}
@endsection
@section('page-title')
    {{__('Announcements')}}
    <!-- {{$announcement->title}} -->
@endsection
@section('page-meta-data')
    <meta name="description" content="{{$announcement->meta_tags}}">
    <meta name="tags" content="{{$announcement->meta_description}}">
@endsection
@section('content')
    <section class="blog-content-area padding-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="single-event-details">
                        <div>
                            <h2>{{$announcement->title}}</h2>
                        </div>
                        <div class="thumb">
                            {!! render_image_markup_by_attachment_id($announcement->image,'','large') !!}
                        </div>
                        <div class="content">
                            <div class="details-content-area">
                                {!! $announcement->content !!}
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="widget-area">
                        
                        <div class="widget event-info">
                            <h4 class="widget-title">Recent Announcements</h4>
                            <ul class="icon-with-title-description">
                                @foreach($all_announcements as $announcement)
                                <li>
                                    <div class="content">
                                        <h4 class="title"><a href="{{route('frontend.announcements.single',$announcement->slug)}}">{{$announcement->title}}</a></h4>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        {!! App\WidgetsBuilder\WidgetBuilderSetup::render_frontend_sidebar('announcement',['column' => false]) !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{asset('assets/common/js/countdown.jquery.js')}}"></script>
    <script>
        var ev_offerTime = "{{$announcement->date}}";
        var ev_year = ev_offerTime.substr(0, 4);
        var ev_month = ev_offerTime.substr(5, 2);
        var ev_day = ev_offerTime.substr(8, 2);

        if (ev_offerTime) {
            $('#event_countdown').countdown({
                year: ev_year,
                month: ev_month,
                day: ev_day,
                labels: true,
                labelText: {
                    'days': "{{__('days')}}",
                    'hours': "{{__('hours')}}",
                    'minutes': "{{__('min')}}",
                    'seconds': "{{__('sec')}}",
                }
            });
        }
    </script>
@endsection