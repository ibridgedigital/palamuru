@extends('uce.frontend.frontend-master')

@section('content')
    @php
    $page_partial = 'home-'.get_uce_static_option('home_page_variant');

    if (!empty(get_uce_static_option('home_page_page_builder_status'))){
        $page_partial = 'page-builder';
    }
    @endphp
    
@include('uce.frontend.home-pages.'.$page_partial)

@endsection
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function (){
        var ids = <?php echo json_encode($final); ?>;
        for (let i = 0; i < ids.length; i++){
            $('#'+ids[i].child).appendTo('#'+ids[i].parent);
        }
    });
</script>
