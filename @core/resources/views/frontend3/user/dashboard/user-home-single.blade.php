@extends('frontend3.user.dashboard.user-master')
@section('site-title')
    {{__('Dashboard')}}
@endsection
@section('section')
    <section class="blog-details-content-area padding-top-100 padding-bottom-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="appointment-details-item">
                       <div class="top-part">
                           <div class="thumb">
                               {!! render_image_markup_by_attachment_id($user_profile->image,'full') !!}
                           </div>
                           <div class="content">
                               <span class="designation">{{$user_profile->category->title ?? ''}}</span>
                               <h2 class="title">{{$user_profile->name ?? ''}}</h2>
                               <div class="short-description">{!! $user_profile->short_description ?? ''     !!}</div>
            
                               <div class="social-share-wrap">
                                   
                                    <ul class="social-share">
                                        @if(!empty($user_profile->facebook))
                                        <li>
                                            <a class="facebook" href="{{$user_profile->facebook}}"><i class="fab fa-facebook-f"></i></a>
                                       </li>
                                       @endif
                                       @if(!empty($user_profile->twitter))
                                       <li>
                                            <a class="twitter" href="{{$user_profile->twitter}}"><i class="fab fa-twitter"></i></a>
                                        </li>
                                        @endif
                                        @if(!empty($user_profile->linkedin))
                                        <li>
                                            <a class="linkedin" href="{{$user_profile->linkedin}}"><i class="fab fa-linkedin-in"></i></a>
                                        </li>
                                        @endif
                                        @if(!empty($user_profile->pinterest))
                                        <li>
                                            <a class="pinterest" href="{{$user_profile->pinterest}}"><i class="fab fa-pinterest-p"></i></a>
                                        </li>
                                        @endif
                                    </ul>
                               </div>
                           </div>
                       </div>
                        <div class="bottom-part">
                            <x-error-msg/>
                            <x-flash-msg/>
                            <nav>
                                <div class="nav nav-tabs" role="tablist">
                                    <a class="nav-link "  data-toggle="tab" href="#nav-information" role="tab"  aria-selected="false">Information</a>
                                    <a class="nav-link active"  data-toggle="tab" href="#nav-booking" role="tab"  aria-selected="true">Publications</a>
                                    <a class="nav-link"  data-toggle="tab" href="#nav-feedback" role="tab"  aria-selected="false">Achievements</a>
                                </div>
                            </nav>
                            <div class="tab-content" >
                                <div class="tab-pane fade" id="nav-information" role="tabpanel" >
                                    <div class="information-area-wrap">
                                        @if(!empty($user_profile->description))
                                        <div class="description-wrap">
                                            <h3 class="title">About Me</h3>
                                            {!! $user_profile->description !!}
                                        </div>
                                        @endif
                                        @php
                                            $experience_info = explode('|', $user_profile->experience_info);
                                            @endphp
                                        @if(!empty($experience_info))
                                        <div class="education-info">
                                            <h3 class="title">Experience Info</h3>
                                            <ul class="circle-list">
                                                @foreach($experience_info as $info)
                                                    <li>{{$info}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        @php
                                            $additional_info = explode('|', $user_profile->additional_info);
                                            @endphp
                                        @if(!empty($additional_info))
                                        <div class="additional-info">
                                            <h3 class="title">Additional Info</h3>
                                            <ul class="circle-list">
                                                @foreach($additional_info as $info)
                                                    <li>{{$info}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        @php
                                        $specialized_info = explode('|', $user_profile->specialized_info);
                                        @endphp
                                        @if(!empty($specialized_info))
                                        <div class="specialised-info">
                                            <h3 class="title">Specialize Info</h3>
                                            <ul class="circle-list">
                                                @foreach($specialized_info as $info)
                                                    <li>{{$info}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane fade show active" id="nav-booking" role="tabpanel" >
                                    <div class="information-area-wrap">
                                        
                                        @php
                                        $publications = explode('|', $user_profile->publications);
                                        @endphp
                                        @if(!empty($publications))
                                        <div class="additional-info">
                                            <h3 class="title">Publications</h3>
                                            <ul class="circle-list">
                                                @foreach($publications as $publication)
                                                    <li>{{$publication}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-feedback" role="tabpanel" >
                                    <div class="feedback-wrapper">
                                        @if(auth()->guard('web')->check())
                                        <div class="feedback-form-wrapper">
                                            <h3 class="title">{{__('Leave your feedback')}}</h3>
                                            <form action="{{route('frontend.appointment.review')}}" method="post" class="appointment-booking-form" id="appointment_rating_form">
                                                @csrf
                                                <div class="error-message"></div>
                                                <input type="hidden" name="appointment_id" value="">
                                                <div class="form-group">
                                                    <label for="rating-empty-clearable2">{{__('Ratings')}}</label>
                                                    <input type="number" name="ratings"
                                                           id="rating-empty-clearable2"
                                                           class="rating text-warning"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">{{__('Message')}}</label>
                                                    <textarea name="message" cols="30" class="form-control" rows="5"></textarea>
                                                </div>
                                                <button type="submit" class="btn-boxed appointment" id="appointment_ratings">{{__('Submit')}}  <i class="fas fa-spinner fa-spin d-none"></i></button>
                                            </form>
                                        </div>
                                        @else
                                            @include('frontend.partials.ajax-login-form')
                                        @endif
                                        
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection