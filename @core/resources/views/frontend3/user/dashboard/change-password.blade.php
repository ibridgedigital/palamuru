@extends('frontend3.user.dashboard.user-master')
@section('section')
    
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                <x-error-msg/>
                <x-flash-msg/>
            </div>
            <div class="col-lg-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        
                        <form action="{{route('user.password.change')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="old_password">{{__('Old Password')}}</label>
                            <input type="password" class="form-control" id="old_password" name="old_password"
                                   placeholder="{{__('Old Password')}}">
                        </div>
                        <div class="form-group">
                            <label for="password">{{__('New Password')}}</label>
                            <input type="password" class="form-control" id="password" name="password"
                                   placeholder="{{__('New Password')}}">
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">{{__('Confirm Password')}}</label>
                            <input type="password" class="form-control" id="password_confirmation"
                                   name="password_confirmation" placeholder="{{__('Confirm Password')}}">
                        </div>
                        <button type="submit" class="btn btn-info">{{__('Save changes')}}</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection