@extends('frontend3.user.dashboard.user-master')
@section('site-title')
    {{__('Dashboard')}}
@endsection
@section('section')
    <div class="main-content-inner">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    @if($total_faculty > 0)
                    <div class="col-md-3 mt-5 mb-3">
                        <div class="card">
                            <div class="dsh-box-style">
                                <a href="{{route('user.profile.new')}}" class="add-new"><i class="ti-plus"></i></a>
                                <div class="icon">
                                
                                    <i class="ti-user"></i>
                                </div>
                                <div class="content">
                                    <span class="total">{{$total_faculty}}</span>
                                    <h4 class="title">{{__('Total Faculty')}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="col-md-3 mt-md-5 mb-3">
                        <div class="card">
                            <div class="dsh-box-style">
                                <a href="{{route('admin.blog.new')}}" class="add-new"><i class="ti-plus"></i></a>
                                <div class="icon">
                                    <i class="ti-layout-width-default"></i>
                                </div>
                                <div class="content">
                                    <span class="total">32</span>
                                    <h4 class="title">{{__('Total Blogs')}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 mt-md-5 mb-3">
                        <div class="card">
                            <div class="dsh-box-style">
                                <a href="{{route('admin.services.new')}}" class="add-new"><i class="ti-plus"></i></a>
                                <div class="icon">
                                    <i class="ti-blackboard"></i>
                                </div>
                                <div class="content">
                                    <span class="total">43</span>
                                    <h4 class="title">{{__('Total Services')}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection