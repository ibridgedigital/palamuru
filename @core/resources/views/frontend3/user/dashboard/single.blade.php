@extends('frontend3.frontend-page-master')
@php
  $post_img = null;
  $blog_image = get_attachment_image_by_id($user_profile->image,"full",false);
  $post_img = !empty($blog_image) ? $blog_image['img_url'] : '';
 @endphp
 
@section('og-meta')
    <meta property="og:url"  content="" />
    <meta property="og:type"  content="article" />
    <meta property="og:image" content="{{$post_img}}" />
@endsection
@section('page-meta-data')
    <meta name="description" content="">
    <meta name="tags" content="">
@endsection
@section('site-title')
    {{$user_profile->name}}
@endsection
@section('page-title')
    {{$user_profile->name}}
@endsection
@section('content')

    <section class="blog-details-content-area padding-top-100 padding-bottom-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="appointment-details-item">
                       <div class="top-part">
                           <div class="thumb">
                               {!! render_image_markup_by_attachment_id($user_profile->image,'full') !!}
                           </div>
                           <div class="content">
                               <span class="designation">{{$user_profile->category->title ?? ''}}</span>
                               <h2 class="title">{{$user_profile->name ?? ''}}</h2>
                               <div class="short-description">{!! $user_profile->short_description ?? ''	 !!}</div>
            
                               <div class="social-share-wrap">
                                   
                                    <ul class="social-share">
                                        @if(!empty($user_profile->facebook))
                                        <li>
                                            <a class="facebook" href="{{$user_profile->facebook}}"><i class="fab fa-facebook-f"></i></a>
                                       </li>
                                       @endif
                                       @if(!empty($user_profile->twitter))
                                       <li>
                                            <a class="twitter" href="{{$user_profile->twitter}}"><i class="fab fa-twitter"></i></a>
                                        </li>
                                        @endif
                                        @if(!empty($user_profile->linkedin))
                                        <li>
                                            <a class="linkedin" href="{{$user_profile->linkedin}}"><i class="fab fa-linkedin-in"></i></a>
                                        </li>
                                        @endif
                                        @if(!empty($user_profile->pinterest))
                                        <li>
                                            <a class="pinterest" href="{{$user_profile->pinterest}}"><i class="fab fa-pinterest-p"></i></a>
                                        </li>
                                        @endif
                                    </ul>
                               </div>
                           </div>
                       </div>
                        <div class="bottom-part">
                            <x-error-msg/>
                            <x-flash-msg/>
                            <nav>
                                <div class="nav nav-tabs" role="tablist">
                                    <a class="nav-link "  data-toggle="tab" href="#nav-information" role="tab"  aria-selected="false">Information</a>
                                    <a class="nav-link active"  data-toggle="tab" href="#nav-booking" role="tab"  aria-selected="true">Publications</a>
                                    <a class="nav-link"  data-toggle="tab" href="#nav-feedback" role="tab"  aria-selected="false">Achievements</a>
                                </div>
                            </nav>
                            <div class="tab-content" >
                                <div class="tab-pane fade" id="nav-information" role="tabpanel" >
                                    <div class="information-area-wrap">
                                        @if(!empty($user_profile->description))
                                        <div class="description-wrap">
                                            <h3 class="title">About Me</h3>
                                            {!! $user_profile->description !!}
                                        </div>
                                        @endif
                                        @php
                                            $experience_info = explode('|', $user_profile->experience_info);
                                            @endphp
                                        @if(!empty($user_profile->experience_info))
                                        <div class="education-info">
                                            <h3 class="title">Experience Info</h3>
                                            <ul class="circle-list">
                                                @foreach($experience_info as $info)
                                                    <li>{{$info}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        @php
                                            $additional_info = explode('|', $user_profile->additional_info);
                                            @endphp
                                        @if(!empty($user_profile->additional_info))
                                        <div class="additional-info">
                                            <h3 class="title">Additional Info</h3>
                                            <ul class="circle-list">
                                                @foreach($additional_info as $info)
                                                    <li>{{$info}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        @php
                                        $specialized_info = explode('|', $user_profile->specialized_info);
                                        @endphp
                                        @if(!empty($user_profile->specialized_info))
                                        <div class="specialised-info">
                                            <h3 class="title">Specialize Info</h3>
                                            <ul class="circle-list">
                                                @foreach($specialized_info as $info)
                                                    <li>{{$info}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane fade show active" id="nav-booking" role="tabpanel" >
                                    <div class="information-area-wrap">
                                        @php
                                        $publications = explode('|', $user_profile->publications);
                                        @endphp
                                        @if(!empty($user_profile->publications))
                                        <div class="additional-info">
                                            <h3 class="title">Publications</h3>
                                            <ul class="circle-list">
                                                @foreach($publications as $publication)
                                                    <li>{{$publication}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-feedback" role="tabpanel" >
                                    <div class="information-area-wrap">
                                    	@php
                                        $achievements = explode('|', $user_profile->achievements);
                                        @endphp
                                        @if(!empty($user_profile->achievements))
                                        <div class="additional-info">
                                            <h3 class="title">Achievements</h3>
                                            <ul class="circle-list">
                                                @foreach($achievements as $achievement)
                                                    <li>{{$achievement}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script type="text/javascript" src="//use.fontawesome.com/5ac93d4ca8.js"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/js/bootstrap4-rating-input.js')}}"></script>
    @include('frontend.partials.ajax-login-form-js')
    <script>
        (function ($){
            "use strict";

            $(document).on('change','#guest_logout',function (e) {
                e.preventDefault();
                var infoTab = $('#nav-profile-tab');
                var nextBtn = $('.next-step-btn');
                if($(this).is(':checked')){
                    $('.booking-wrap .login-form').hide();
                    infoTab.attr('disabled',false).removeClass('disabled');
                    nextBtn.show();

                }else{
                    $('.login-form').show();
                    infoTab.attr('disabled',true).addClass('disabled');
                    nextBtn.hide();
                }
            });
            $(document).on('click','.next-step-btn',function(e){
                var infoTab = $('#nav-profile-tab');
                infoTab.attr('disabled',false).removeClass('disabled').addClass('active').siblings().removeClass('active');
                $('#nav-profile').addClass('show active').siblings().removeClass('show active');
            });

            $(document).on('click','.payment-gateway-wrapper > ul > li',function (e) {
                e.preventDefault();
                var gateway = $(this).data('gateway');
                var manual_gateway_tr = $('.manual_payment_transaction_field');
                $(this).addClass('selected').siblings().removeClass('selected');
                $('input[name="selected_payment_gateway"]').val(gateway);
                if(gateway === 'manual_payment'){
                    manual_gateway_tr.show();
                }else{
                    manual_gateway_tr.hide();
                }
            });

            $(document).on('click','.time-slot.date li',function (e){
                e.preventDefault();
                var date = $(this).data('date');
                date = date.split('-');
                var showDate = new Date(date[2]+'-'+ date[1]+'-'+date[0]);
                $('.time_slog_date').text(showDate.toDateString());
                $(this).toggleClass('selected').siblings().removeClass('selected');
                $('input[name="booking_date"]').val($(this).data('date'));
            });
            $(document).on('click','.time-slot.time li',function (e){
                e.preventDefault();
                $(this).toggleClass('selected').siblings().removeClass('selected');
                $('input[name="booking_time_id"]').val($(this).data('id'));
            });


            $(document).on('click', '#appointment_ratings', function (e) {
                e.preventDefault();
                var myForm = document.getElementById('appointment_rating_form');
                var formData = new FormData(myForm);

                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.appointment.review')}}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend: function(){
                        $('#appointment_ratings').children('i').removeClass('d-none');
                    },
                    success: function (data) {
                        var errMsgContainer = $('#appointment_rating_form').find('.error-message');
                        $('#appointment_ratings').children('i').addClass('d-none');
                        errMsgContainer.html('');
                        errMsgContainer.append('<div class="alert alert-'+data.type+'">'+data.msg+'</div>');

                    },
                    error: function (data) {
                        var error = data.responseJSON;
                        var errMsgContainer = $('#appointment_rating_form').find('.error-message');
                        errMsgContainer.html('');
                        $.each(error.errors,function (index,value) {
                            errMsgContainer.append('<span class="text-danger">'+value+'</span>');
                        });
                        $('#appointment_ratings').children('i').addClass('d-none');
                    }
                });
            });

            //appo_booking_btn
        })(jQuery);
    </script>
@endsection
