@php $home_page_variant = get_static_option('home_page_variant');@endphp
<div class="sidebar-menu">
    <div class="sidebar-header">
        <div class="logo" style="max-height: 50px;">
            <a href="{{route('user.home')}}">
                @php
                    $logo_type = 'site_logo';
                    if(!empty(get_static_option('site_admin_dark_mode'))){
                        $logo_type = 'site_white_logo';
                    }
                @endphp
                {!! render_image_markup_by_attachment_id(get_static_option($logo_type)) !!}
            </a>
        </div>
    </div>
    <div class="main-menu">
        <div class="menu-inner">
            <nav id="main_menu_wrap">
                <ul class="metismenu" id="menu">
                    <li class="{{active_menu('user-home')}}">
                        <a href="{{route('user.home')}}"
                           aria-expanded="true">
                            <i class="ti-dashboard"></i>
                            <span>@lang('Profile')</span>
                        </a>
                    </li>
                </ul>                
                <ul class="metismenu" id="menu">
                    
                    <li class="{{active_menu('user-home')}}">
                        <a href="{{route('user.home.edit.profile')}}"
                           aria-expanded="true">
                            <span>Edit</span>
                        </a>
                    </li>
                    <li class="{{active_menu('user-home')}}">
                        <a href="{{route('user.home.view.profile')}}"
                           aria-expanded="true">
                            <span>View</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
