@php $home_page_variant = get_static_option('home_page_variant');@endphp
<div class="sidebar-menu">
    <div class="sidebar-header">
        <div class="logo" style="max-height: 50px;">
            <a href="{{route('user.home')}}">
                @php
                    $logo_type = 'site_logo';
                    if(!empty(get_static_option('site_admin_dark_mode'))){
                        $logo_type = 'site_white_logo';
                    }
                @endphp
                {!! render_image_markup_by_attachment_id(get_static_option($logo_type)) !!}
            </a>
        </div>
    </div>
    <div class="main-menu">
        <div class="menu-inner">
            <nav id="main_menu_wrap">
                <ul class="metismenu" id="menu">
                    <li class="{{active_menu('user-home')}}">
                        <a href="{{route('user.home')}}"
                           aria-expanded="true">
                            <i class="ti-dashboard"></i>
                            <span>@lang('dashboard')</span>
                        </a>
                    </li>
                    <!-- @if(Auth::user()->role <= 3)
                    <li class="main_dropdown @if(request()->is('user-home/profile/*')) active @endif ">
                        <a href="javascript:void(0)" aria-expanded="true">
                            {{__('Profiles Manage')}} </a>
                        <ul class="collapse">
                            <li class="{{active_menu('user-home/profile/all')}}">
                                <a href="{{route('user.profile.all')}}">{{__('All Profiles')}}</a></li>
                            
                            <li class="{{active_menu('user-home/profile/new')}}">
                                <a href="{{route('user.profile.new')}}">{{__('New Profile')}}</a></li>
                            
                            <li class="{{active_menu('user-home/profile/category')}}">
                                <a href="{{route('user.profile.category.all')}}">{{__('Category')}}</a></li>
                            
                        </ul>
                    </li>
                    @endif -->
                    @if(Auth::user()->role == 4)
                    <li class="{{active_menu('user-home/profile/all')}}">
                            <a href="{{route('user.home.edit.profile')}}">{{__('Edit Profile')}}</a></li>
                        
                    <li class="{{active_menu('user-home/profile/new')}}">
                            <a href="{{route('user.logout')}}"
                                onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a></li>
                    @endif
                        

                    @if(Auth::user()->role <= 3)
                    
                        <li class="{{active_menu('user-home/profile/all')}}">
                            <a href="{{route('user.profile.all')}}">{{__('All Profiles')}}</a></li>
                        
                        <li class="{{active_menu('user-home/profile/new')}}">
                            <a href="{{route('user.profile.new')}}">{{__('New Profile')}}</a></li>
                        
                        <li class="{{active_menu('user-home/profile/category')}}">
                            <a href="{{route('user.profile.category.all')}}">{{__('Category')}}</a></li>
            			<li class="{{active_menu('user-home/profile/department')}}">
                            <a href="{{route('user.profile.department.all')}}">{{__('Departments')}}</a></li> 
                       
                    @endif
                </ul>
            </nav>
        </div>
    </div>
</div>
