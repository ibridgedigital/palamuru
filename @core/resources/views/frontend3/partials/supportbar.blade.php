@php 
if($home_page_variant == '07' || $home_page_variant == '09' || Route::currentRouteName() == 'frontend.course.lesson'){ return;} @endphp
@if(!empty(get_static_option('home_page_support_bar_section_status')))
    <div class="top-bar-area header-variant-{{get_static_option('home_page_variant')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="top-bar-inner">
                        <div class="left-content">
                            <ul>
                                <li><a href="./"><i class="fa fa-home"></i> Home</a></li>
                            </ul>
                        </div>
                        <div class="right-content">
                            <ul class="topbar-left">
                                <li class="current-menu-item"><a href="./cbcs-syllabus">CBCS Syllabus</a></li>
                                <li class="current-menu-item"><a href="./ssr-links">SSR links</a></li>
                                <li class="current-menu-item"><a href="./self-study-report">Self Study Report</a></li>
                                <li class="current-menu-item"><a href="./allumni ">Allumni</a></li>
                                <li class="current-menu-item"><a href="./almanac">Almanac</a></li>
                                <li class="current-menu-item"><a href="./rti-act">RTI Act</a></li>
                                <li class="current-menu-item"><a href="./contact-us">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif