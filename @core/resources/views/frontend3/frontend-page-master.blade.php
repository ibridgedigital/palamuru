@include('frontend3.partials.header')
@include('frontend3.partials.navbar-variant.navbar-'.get_static_option('navbar_variant'))
@yield('content')
@include('frontend3.partials.footer')
