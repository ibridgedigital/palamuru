@extends('frontend3.user.dashboard.user-master')
@section('site-title')
    {{__('Edit Profile')}}
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('assets/backend/css/summernote-bs4.css')}}">
    <link rel="stylesheet" href="{{asset('assets/backend/css/dropzone.css')}}">
    <link rel="stylesheet" href="{{asset('assets/backend/css/media-uploader.css')}}">
    <link rel="stylesheet" href="{{asset('assets/backend/css/bootstrap-tagsinput.css')}}">
    <link rel="stylesheet" href="{{asset('assets/backend/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('section')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                <x-error-msg/>
                <x-flash-msg/>
            </div>
            <div class="col-lg-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <div class="header-wrap d-flex justify-content-between">
                            <h4 class="header-title">{{__('Edit Profile')}}</h4>
                            <a href="{{route('user.profile.all')}}" class="btn btn-info">{{__('All Profiles')}}</a>
                        </div>
                        <form action="{{route('user.profile.update')}}" method="post" enctype="multipart/form-data">
                            <input type="hidden" value="{{$user_profile->id}}" name="id">
                            <input type="hidden" value="{{$user_profile->user_id}}" name="user_id">
                            @csrf
                            <ul class="nav nav-tabs" role="tablist">
                                @php $default_lang = get_default_language(); @endphp
                                <li class="nav-item">
                                    <a class="nav-link active "  data-toggle="tab" href="#slider_tab" role="tab" aria-controls="home" aria-selected="true">English (USA)</a>
                                </li>
                            </ul>
                            <div class="tab-content margin-top-40" >
                                
                                    <div class="tab-pane fade show active" id="slider_tab" role="tabpanel" >
                                        <div class="form-group">
                                            <label for="name">{{__('Name')}}</label>
                                            <input type="text" class="form-control" name="name" value="{{$user_profile->user->name}}">
                                        </div>
                                    	<div class="form-group">
                                            <label for="username">{{__('User Name')}}</label>
                                            <input type="text" class="form-control" name="username" value="{{$user_profile->user->username}}">
                                        </div>
                                    	 <div class="form-group">
                                            <label for="email">{{__('Email')}}</label>
                                            <input type="email" class="form-control" name="email" value="{{$user_profile->user->email}}" placeholder="{{__('Email')}}">
                                        </div>
                                    	
                                        <div class="form-group">
                                            <label for="slug">{{__('Slug')}}</label>
                                            <input type="text" class="form-control" name="slug" value="{{$user_profile->slug}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="designation">{{__('Designation')}}</label>
                                            <input type="text" class="form-control"  name="designation" value="{{$user_profile->designation}}" >
                                        </div>
                                        <div class="form-group">
                                            <label>{{__('Description')}}</label>
                                            <input type="hidden" name="description" value="{{$user_profile->description}}">
                                            <div class="summernote" data-content='{{$user_profile->description}}'></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="location">{{__('Location')}}</label>
                                            <input type="text" name="location" class="form-control" value="{{$user_profile->location}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="short_description">{{__('Short Description')}}</label>
                                            <textarea name="short_description" cols="30" rows="5" class="form-control" placeholder="{{__('Short Description')}}">{{$user_profile->short_description}}</textarea>
                                        </div>

                                        <div class="iconbox-repeater-wrapper dynamic-repeater">
                                            <label for="additional_info" class="d-block">{{__('Additional Info')}}</label>
                                            @php
                                            $additional_info = explode('|', $user_profile->additional_info);
                                            @endphp

                                            @forelse($additional_info as $add_info)
                                            <div class="all-field-wrap">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="additional_info[]"  value="{{$add_info}}" placeholder="{{__('additional info')}}">
                                                </div>
                                                <div class="action-wrap">
                                                    <span class="add"><i class="ti-plus"></i></span>
                                                    <span class="remove"><i class="ti-trash"></i></span>
                                                </div>
                                            </div>
                                            @empty
                                                <div class="all-field-wrap">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="additional_info[]"  placeholder="{{__('additional info')}}">
                                                    </div>
                                                    <div class="action-wrap">
                                                        <span class="add"><i class="ti-plus"></i></span>
                                                        <span class="remove"><i class="ti-trash"></i></span>
                                                    </div>
                                                </div>
                                            @endforelse
                                        </div>

                                        <div class="iconbox-repeater-wrapper  dynamic-repeater">
                                            <label for="experience_info" class="d-block">{{__('Experience Info')}}</label>
                                            @php
                                            $experience_info = explode('|', $user_profile->experience_info);
                                            @endphp
                                            @forelse($experience_info as $add_info)
                                            <div class="all-field-wrap">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="experience_info[]" placeholder="{{__('Experience Info')}}" value="{{$add_info}}">
                                                </div>
                                                <div class="action-wrap">
                                                    <span class="add"><i class="ti-plus"></i></span>
                                                    <span class="remove"><i class="ti-trash"></i></span>
                                                </div>
                                            </div>
                                            @empty
                                                <div class="all-field-wrap">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="experience_info[]" placeholder="{{__('Experience Info')}}">
                                                    </div>
                                                    <div class="action-wrap">
                                                        <span class="add"><i class="ti-plus"></i></span>
                                                        <span class="remove"><i class="ti-trash"></i></span>
                                                    </div>
                                                </div>
                                            @endforelse
                                        </div>
                                        <div class="iconbox-repeater-wrapper  dynamic-repeater">
                                            <label for="specialized_info" class="d-block">{{__('Specialized Info')}}</label>
                                            @php
                                            $specialized_info = explode('|', $user_profile->specialized_info);
                                            @endphp
                                            @forelse($specialized_info as $add_info)
                                            <div class="all-field-wrap">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="specialized_info[]" placeholder="{{__('Specialized Info')}}" value="{{$add_info}}">
                                                </div>
                                                <div class="action-wrap">
                                                    <span class="add"><i class="ti-plus"></i></span>
                                                    <span class="remove"><i class="ti-trash"></i></span>
                                                </div>
                                            </div>
                                            @empty
                                                <div class="all-field-wrap">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="specialized_info[]"  placeholder="{{__('Specialized Info')}}">
                                                    </div>
                                                    <div class="action-wrap">
                                                        <span class="add"><i class="ti-plus"></i></span>
                                                        <span class="remove"><i class="ti-trash"></i></span>
                                                    </div>
                                                </div>
                                            @endforelse
                                        </div>
                                        <div class="iconbox-repeater-wrapper  dynamic-repeater">
                                            <label for="specialized_info" class="d-block">
                                            {{__('Publications')}}</label>
                                            @php
                                            $publications = explode('|', $user_profile->publications);
                                            @endphp
                                            @forelse($publications as $publication)
                                            <div class="all-field-wrap">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="publications[]" placeholder="{{__('Publications')}}" value="{{$publication}}">
                                                </div>
                                                <div class="action-wrap">
                                                    <span class="add"><i class="ti-plus"></i></span>
                                                    <span class="remove"><i class="ti-trash"></i></span>
                                                </div>
                                            </div>
                                            @empty
                                                <div class="all-field-wrap">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="publications[]"  placeholder="{{__('Publications')}}">
                                                    </div>
                                                    <div class="action-wrap">
                                                        <span class="add"><i class="ti-plus"></i></span>
                                                        <span class="remove"><i class="ti-trash"></i></span>
                                                    </div>
                                                </div>
                                            @endforelse
                                        </div>
                                        <div class="form-group mb-2">
                                            <label for="facebook">{{__('Social Links')}}</label>
                                            <div class="row">
                                                <div class="col">
                                                    <input type="text" class="form-control" placeholder="{{__('Facebook')}}" name="facebook" value="{{$user_profile->facebook}}">
                                                </div>
                                                <div class="col">
                                                    <input type="text" class="form-control" placeholder="{{__('Twitter')}}" value="{{$user_profile->twitter}}" name="twitter">
                                                </div>
                                                <div class="col">
                                                    <input type="text" class="form-control" placeholder="{{__('LinkedIn')}}" name="linkedin" value="{{$user_profile->linkedin}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="meta_title">{{__('Meta title')}}</label>
                                            <input type="text" class="form-control" name="meta_title" placeholder="{{__('Meta title')}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="meta_description">{{__('Meta Description')}}</label>
                                            <textarea  class="form-control max-height-120" name="meta_description"cols="30" rows="10" placeholder="{{__('Meta Description')}}"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="meta_tags">{{__('Meta Tags')}}</label>
                                            <input type="text" name="meta_tags"  class="form-control" data-role="tagsinput">
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="category">{{__('Category')}}</label>
                                        <select name="category_id" class="form-control" id="category">
                                            <option value="">{{__("Select Category")}}</option>
                                            @foreach($all_category as $category)
                                                <option value="{{$category->id}}" @if($category->id == $user_profile->category_id) selected @endif>{{$category->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                	<div class="form-group">
                                        <label for="department">{{__('Department')}}</label>
                                        <select name="department_id" class="form-control" id="department">
                                            <option value="">{{__("Select Department")}}</option>
                                            @foreach($departments as $department)
                                                <option value="{{$department->id}}" @if($department->id == $user_profile->dept_id) selected @endif>{{$department->dept_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <x-media-upload :name="'image'" :title="__('Image')" :id="$user_profile->image" :dimentions="'350x500'"/>
                                    

                                    <x-backend.status-field :name="'status'" :value="$user_profile->status" :title="__('Status')"/>
                                    <button type="submit"
                                            class="btn btn-primary mt-4 pr-4 pl-4">{{__('Submit')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.partials.media-upload.media-upload-markup')
@endsection
@section('script')
    @include('backend.partials.repeater.script')
    <script>
        $(document).ready(function () {
            $(document).on('click','ul.time_slot li',function (e){
                e.preventDefault();
                //prent selector
                var parent = $(this).parent().parent();
                //append input field value by this id
                var ids = parent.find('input[name="booking_time_ids"]');
                var oldValue = ids.val()
                //assign new value =
                var id = $(this).data('id');
                if(oldValue != ''){
                    var oldValAr = oldValue.split(',');
                    if($(this).hasClass('selected')){
                        var oldValAr = oldValAr.filter(function (item){return item != id;});
                    }else{
                        oldValAr.push(id);
                    }
                    ids.val(oldValAr.toString());
                }else{
                    ids.val(id);
                }
                //add class for this li
                $(this).toggleClass('selected');
            });
            $(document).on('change', '#language', function (e) {
                e.preventDefault();
                var selectedLang = $(this).val();
                $.ajax({
                    url: "{{route('user.profile.category.by.lang')}}",
                    type: "POST",
                    data: {
                        _token: "{{csrf_token()}}",
                        lang: selectedLang
                    },
                    success: function (data) {
                        $('#category').html('<option value="">{{__("Select Category")}}</option>');
                        $.each(data, function (index, value) {
                            $('#category').append('<option value="' + value.id + '">' + value.title + '</option>')
                        });
                    }
                });
            });
            $('.summernote').summernote({
                height: 250,   //set editable area's height
                codemirror: { // codemirror options
                    theme: 'monokai'
                },
                callbacks: {
                    onChange: function (contents, $editable) {
                        $(this).prev('input').val(contents);
                    }
                }
            });
            if($('.summernote').length > 0){
                $('.summernote').each(function(index,value){
                    $(this).summernote('code', $(this).data('content'));
                });
            }
        });
    </script>
    <script src="{{asset('assets/backend/js/summernote-bs4.js')}}"></script>
    <script src="{{asset('assets/backend/js/dropzone.js')}}"></script>
    <script src="{{asset('assets/backend/js/bootstrap-tagsinput.js')}}"></script>
    @include('backend.partials.media-upload.media-js')
@endsection