@extends('frontend3.user.dashboard.user-master')

@section('style')
    @include('backend.partials.datatable.style-enqueue')
    <link rel="stylesheet" href="{{asset('assets/backend/css/media-uploader.css')}}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style type="text/css">
        .toggle {
            border: 1px solid grey;
        }
        input:checked + .slider:before {
            content: "Yes";
        }
        .slider:before {
            content: "No";
        }
    </style>
@endsection
@section('site-title')
    {{__('All Appointments')}}
@endsection
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('section')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                <x-error-msg/>
                <x-flash-msg/>
            </div>
            <div class="col-lg-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('All Appointments')}}</h4>
                        <div class="bulk-delete-wrapper">
                            <div class="select-box-wrap">
                                <select name="bulk_option" id="bulk_option">
                                    <option value="">{{{__('Bulk Action')}}}</option>
                                    <option value="delete">{{{__('Delete')}}}</option>
                                </select>
                                <button class="btn btn-primary btn-sm" id="bulk_delete_btn">{{__('Apply')}}</button>
                            </div>
                        </div>
                         <div class="table-wrap table-responsive">
                            <table class="table table-default" >
                                <thead>
                                <th class="no-sort">
                                    <div class="mark-all-checkbox">
                                        <input type="checkbox" class="all-checkbox">
                                    </div>
                                </th>
                                <th>{{__('ID')}}</th>
                                <th>{{__('Name')}}</th>
                                <th>{{__('Image')}}</th>
                                <th>{{__('Category')}}</th>
                                <th>{{__('Department')}}</th>
                                <th>{{__('Active')}}</th>
                                <th>{{__('Action')}}</th>
                                </thead>
                                <tbody>
                                @foreach($all_appointment as $data)
                                    <tr>
                                        <td>
                                            <div class="bulk-checkbox-wrapper">
                                                <input type="checkbox" class="bulk-checkbox"
                                                       name="bulk_delete[]" value="{{$data->id}}">
                                            </div>
                                        </td>
                                        <td>{{$data->id}}</td>
                                        <td>{{strtoupper($data->user->name) ?? __('untitled')}}</td>
                                        <td>
                                            <div class="img-wrap">
                                                @php
                                                    $event_img = get_attachment_image_by_id($data->image,'thumbnail',true);
                                                @endphp
                                                @if (!empty($event_img))
                                                    <div class="attachment-preview">
                                                        <div class="thumbnail">
                                                            <div class="centered">
                                                                <img class="avatar user-thumb"
                                                                     src="{{$event_img['img_url']}}" alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </td>
                                        @if(!empty($data->category->title))
                                        <td>{{$data->category->title}}</td>
                                        @else
                                        <td></td>
                                        @endif
                                        @if(!empty($data->department->dept_name))
                                        <td>
                                            {{$data->department->dept_name}}
                                        </td>
                                        @else
                                        <td></td>
                                        @endif
                                        <td>
                                            <!-- <input type="checkbox" data-toggle="toggle" data-onstyle="success" data-offstyle="default" onclick="isApproveUser()"> -->
                                            <label class="switch"><input type="checkbox" onclick="updateText({{$data->user_id}},{{$data->user->is_approved}})" @if($data->user->is_approved == 1) checked @endif><span class="slider"></span></label>

                                        </td>
                                        <td>
                                            <x-delete-popover :url="route('user.profile.delete',$data->id)"/>
                                            <x-edit-icon :url="route('user.profile.edit',$data->id)"/>
                                            <x-view-icon :url="route('user.profile.single',[$data->slug ?? 'untitled',$data->id])"/>
<!--                                             <x-backend.clone-icon :url="route('user.profile.clone')" :id="$data->id"/> -->
                                        </td>                                        
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    @include('backend.partials.datatable.script-enqueue')
    @include('backend.partials.bulk-action',['action' =>route('user.profile.bulk.action') ])
    <script type="text/javascript">
        function updateText(user_id,is_approved)
        {              
            token = $('#token').val();
            $.ajax({              
              url: "{{ route('profile.approve.store') }}",
              data: {"_token": token, "user_id": user_id, "is_approved": is_approved},
              method: "POST",
              success: function (data) {
                // alert("success");
                location.reload();
              }
          });
        }
    </script>
@endsection