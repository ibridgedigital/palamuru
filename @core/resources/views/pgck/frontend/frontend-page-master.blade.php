@include('pgcg.frontend.partials.header')
@include('pgcg.frontend.partials.navbar-variant.navbar-'.get_static_option('navbar_variant'))
@include('pgcg.frontend.partials.breadcrumb')
@yield('content')
@include('pgcg.frontend.partials.footer')
