@extends('pgck.frontend.frontend-page-master')
@section('page-meta-data')
<meta name="description" content="{{$page_post->meta_description}}">
<meta name="tags" content="{{$page_post->meta_tags}}">
@endsection
@section('site-title')
    {{$page_post->title}}
@endsection
@section('page-title')
    {{$page_post->title}}
@endsection
@section('content')
    <section class="blog-content-area padding-120">
        <div class="container">
            <div class="row">
                <!--Main Navigation-->
                <div class="col-lg-3">
                    <div class="ib-sidebar">
                        {!! render_frontend_menu_sidebar($slug) !!}
                    </div>
                </div>
                                
                <div class="col-lg-9">
                    <div class="single-event-details">
                        
                        <div class="content">
                            <div class="details-content-area">
                                @if(!empty($page_post->page_builder_status))
                                    {!! \App\PageBuilder\PageBuilderSetup::render_frontend_pagebuilder_content_for_dynamic_page('dynamic_page',$page_post->id) !!}

                                @else
                                    @include('pgck.frontend.partials.dynamic-page-content')
                                @endif  

                            </div>                                          
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </section> 
@endsection
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function (){
        var ids = <?php echo json_encode($final); ?>;
        for (let i = 0; i < ids.length; i++){
            $('#'+ids[i].child).appendTo('#'+ids[i].parent);
        }
    });
</script>
<!-- for custom slider -->

